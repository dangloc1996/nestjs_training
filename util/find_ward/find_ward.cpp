#include <string>

#include <gdal/ogr_geometry.h>
#include <gdal/ogrsf_frmts.h>

using std::vector;

struct Wards {
    vector<std::string> cities;
    vector<std::string> districts;
    vector<std::string> wards;
};

Wards wards;

static inline void PrintWard(int i) {
    printf("%s,", wards.cities[i].c_str());
    printf("%s,", wards.districts[i].c_str());
    printf("%s", wards.wards[i].c_str());
}

static OGRLayer *poLayer;

static void ReadData() {
    GDALAllRegister();
    const auto poDS = (GDALDataset *) GDALOpenEx("gadm36_VNM.gpkg", GDAL_OF_VECTOR, 0, 0, 0);
    if(!poDS) {
        printf("Open Dataset failed\n");
        exit(1);
    }

    poLayer = poDS->GetLayerByName("gadm36_VNM_3");

    for(auto& poFeature: poLayer) {
        wards.cities.push_back(poFeature->GetFieldAsString("NAME_1"));
        wards.districts.push_back(poFeature->GetFieldAsString("NAME_2"));
        wards.wards.push_back(poFeature->GetFieldAsString("NAME_3"));
    }

}

static int FindWard(double lon, double lat) {
    int FeatureCount = 0;
    int Result = -1;

    const auto P = new OGRPoint(lon, lat);

    for( auto& poFeature: poLayer ) {
        auto poGeometry = poFeature->GetGeomFieldRef(0);
        auto contain = poGeometry->Contains(P);
        if (contain) {
            Result = FeatureCount;
        }
        ++FeatureCount;
    }
    return Result;
}

int main(int argc, char *argv[]) {
    if (argc <= 2) {
        return 1;
    }
    ReadData();
    int Result = FindWard(atof(argv[1]), atof(argv[2]));
    PrintWard(Result);
    return 0;
}
