#!/usr/bin/bash

TARGET=$1
HOST=$2
if [ -z "$TARGET" ]; then
    TARGET=dev
    HOST="root@115.165.166.228"
fi

command="cd /app/be-1022-19009095 && \
         git pull && \
         sh misc/update-app.sh $TARGET"
ssh $HOST $command
