#!/usr/bin/bash

TARGET=$1
[ -z "$TARGET" ] && TARGET=dev

git stash
git pull

mkdir -p public/import-agent-log &&  \
    mkdir -p public/import-agent-template && \
    mkdir -p public/import-agent-validate

rm -rf .config
cp -r .config.$TARGET .config

echo $target
docker-compose up -d --build BackendApp
