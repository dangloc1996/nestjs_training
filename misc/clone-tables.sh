#!/usr/bin/bash

TABLES="ott_ticket_ba_setting 
	ott_ticket_field_consultation 
	ott_ticket_staff 
	ott_ticket_import_agent 
	ott_ticket_active_location"

ORIGIN_HOST="119.82.130.154"
ORIGIN_DATABASE="mylocal"
ORIGIN_USERNAME="localuser"
ORIGIN_PASSWORD="qwerty@123456"

DESTINATION_HOST="localhost"
DESTINATION_DATABASE="ott_ticket"
DESTINATION_USERNAME="ott_ticket_user"
DESTINATION_PASSWORD="ott_ticket_user_password_asrtneio"

EXPORT_COMMAND="mysqldump --host=${ORIGIN_HOST} --databases ${ORIGIN_DATABASE} --tables ${TABLES} 
			  --user=${ORIGIN_USERNAME} --password=${ORIGIN_PASSWORD}"
IMPORT_COMMAND="mysql -h ${DESTINATION_HOST} -u ${DESTINATION_USERNAME}
		       --password=${DESTINATION_PASSWORD} ${DESTINATION_DATABASE}"

docker-compose exec BackendMariadb ${EXPORT_COMMAND} > sql/tmp-backup.sql
docker-compose exec BackendMariadb ${IMPORT_COMMAND} < sql/tmp-backup.sql 
rm sql/tmp-backup.sql
