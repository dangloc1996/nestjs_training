#!/usr/bin/bash

source ./.env

command="mysqldump --host=${TYPEORM_HOST} --databases ${TYPEORM_DATABASE}
		   --user=${TYPEORM_USERNAME} --password=${TYPEORM_PASSWORD} 
		   --tables ott_ticket_ba_setting ott_ticket_field_consultation ott_ticket_staff 
		   ott_ticket_import_agent ott_ticket_active_location"

docker-compose exec BackendMariadb $command
