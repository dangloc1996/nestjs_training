-- ott_ticket.ott_ticket_ticket definition

CREATE TABLE `ott_ticket_ticket` (
  `id` varchar(40) CHARACTER SET ascii NOT NULL DEFAULT 'uuid()',
  `title` text CHARACTER SET utf8mb4 NOT NULL,
  `field` int(11) DEFAULT NULL,
  `status` text NOT NULL,
  `number_id` text NOT NULL,
  `assignee` int(11) NOT NULL,
  `creator` int(11) NOT NULL,
  `assigned_at` timestamp(6) NULL DEFAULT NULL,
  `started_at` timestamp(6) NULL DEFAULT NULL,
  `completed_at` timestamp(6) NULL DEFAULT NULL,
  `extra` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '{}',
  `city` varchar(10) NOT NULL,
  `district` varchar(10) NOT NULL,
  `ward` varchar(10) NOT NULL,
  `assignee_phone` varchar(20) DEFAULT NULL,
  `user_phone` varchar(20) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
