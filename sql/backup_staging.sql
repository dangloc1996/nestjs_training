-- MariaDB dump 10.19  Distrib 10.6.4-MariaDB, for Linux (x86_64)
--
-- Host: 119.82.135.17    Database: mylocal
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ott_ticket_ba_setting`
--

DROP TABLE IF EXISTS `ott_ticket_ba_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ott_ticket_ba_setting` (
  `id` int NOT NULL AUTO_INCREMENT,
  `number` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `number_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `queue_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `routing_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ott_ticket_ba_setting_UN` (`number_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ott_ticket_ba_setting`
--

LOCK TABLES `ott_ticket_ba_setting` WRITE;
/*!40000 ALTER TABLE `ott_ticket_ba_setting` DISABLE KEYS */;
INSERT INTO `ott_ticket_ba_setting` VALUES (24,'+842871008879','NUUO9CGB','QUC9W189','GRRSRRRZ','Tổng đài 1022','ROF1KG8W'),(25,'+yte_19009095','NU2HBBFB','QUV4OURM','GRN4JKRC','Bộ y tế 19009095','ROMNHI7T');
/*!40000 ALTER TABLE `ott_ticket_ba_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ott_ticket_field_consultation`
--

DROP TABLE IF EXISTS `ott_ticket_field_consultation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ott_ticket_field_consultation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `special` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ott_ticket_field_consultation`
--

LOCK TABLES `ott_ticket_field_consultation` WRITE;
/*!40000 ALTER TABLE `ott_ticket_field_consultation` DISABLE KEYS */;
INSERT INTO `ott_ticket_field_consultation` VALUES (65,'Hỗ trợ Covid-19','GRRSRRRZ','COVID_CONSULTANT'),(66,'Giao thông','GRRSRRRZ',NULL),(67,'Cấp nước','GRRSRRRZ',NULL),(68,'Thoát Nước','GRRSRRRZ',NULL),(69,'Viễn thông','GRRSRRRZ',NULL),(70,'Cây xanh','GRRSRRRZ',NULL),(71,'Chiếu sáng','GRRSRRRZ',NULL),(72,'Trật tự đô thị','GRRSRRRZ',NULL),(73,'Tài nguyên môi trường','GRRSRRRZ',NULL),(74,'Điện nước','GRRSRRRZ',NULL),(75,'Đăng ký tiêm vaccine Covid-19','GRRSRRRZ','VACCINE'),(76,'Tư vấn covid','GRN4JKRC',NULL),(77,'Hô hấp','GRN4JKRC',NULL),(78,'Da liễu','GRN4JKRC',NULL),(79,'Răng hàm mặt','GRN4JKRC',NULL),(80,'Nam khoa','GRN4JKRC',NULL),(81,'Nhi khoa','GRN4JKRC',NULL),(82,'Tiêu hóa','GRN4JKRC',NULL),(83,'Tim mạch','GRN4JKRC',NULL),(84,'Mắt','GRN4JKRC',NULL),(85,'Y học cổ truyền','GRN4JKRC',NULL),(86,'SOS','GRRSRRRZ','SOS');
/*!40000 ALTER TABLE `ott_ticket_field_consultation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ott_ticket_staff`
--

DROP TABLE IF EXISTS `ott_ticket_staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ott_ticket_staff` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `group_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `agent_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `role` int NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `district` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ward` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `field` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `phone_number` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=206 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ott_ticket_staff`
--

LOCK TABLES `ott_ticket_staff` WRITE;
/*!40000 ALTER TABLE `ott_ticket_staff` DISABLE KEYS */;
INSERT INTO `ott_ticket_staff` VALUES (187,'vũ an hải','GRN4JKRC','AG8E1SYF',1,'example@example.com','HCM','010','0012','77',6,'2021-10-03 10:06:01.340620','84981234567'),(190,'Au Duong Dat','GRRSRRRZ','AGZ6GVPD',2,'datau@asimtelecom.vn','HCM','010','0012','69',13,'2021-10-03 06:42:19.936709','84908495911'),(192,'Văn Hậu','GRRSRRRZ','AGOICO6R',2,'vanhau@asimtelecom.vn','HCM','010','0012','70',15,'2021-10-03 07:04:40.335113','84899774122'),(198,'Bach','GRRSRRRZ','AGZTYL1F',2,'a@a.com','LCH','TUE','TTHU','75',17,'2021-10-11 10:43:27.414933','84913794656'),(199,'Mẫn Nhi','GRRSRRRZ','AGAUWTX1',2,'longan1@gmail.com','LAN','TAN','TKHA','69',22,'2021-10-03 15:07:24.844959','84968683379'),(200,'Trọng Hoàng','GRRSRRRZ','AGTYGOK0',2,'longan2@gmail.com','LAN','TAN','TKHA','70',23,'2021-10-03 15:09:33.377620','84907751727'),(201,'Văn Toàn','GRRSRRRZ','AGS7CE2A',2,'longan3@gmail.cm','LAN','TAN','TKHA','71',24,'2021-10-03 15:11:19.941399','84908592085'),(202,'Ngô Như Trang','GRRSRRRZ','AGKO983M',2,'longan4@gmail.com','LAN','TAN','TKHA','70',26,'2021-10-05 15:35:18.209446','84905911149'),(203,'Nguyễn Minh Hiếu','GRRSRRRZ','AGLRR8DY',2,'longan5@gmail.com','LAN','TAN','TKHA','70',29,'2021-10-06 03:40:15.198099','84936959141'),(204,'Trần Minh Hiếu','GRRSRRRZ','AGXGUA4J',2,'longan6@gmail.com','LAN','TAN','TKHA','70',31,'2021-10-06 05:22:49.193192','84902000999'),(205,'Âu Dương Thành','GRRSRRRZ','AGTFOXB3',2,'thanhau@asimtelecom.vn','HCM','010','0012','75',25,'2021-10-11 17:24:58.108747','84903344211');
/*!40000 ALTER TABLE `ott_ticket_staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ott_ticket_import_agent`
--

DROP TABLE IF EXISTS `ott_ticket_import_agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ott_ticket_import_agent` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ott_ticket_import_agent`
--

LOCK TABLES `ott_ticket_import_agent` WRITE;
/*!40000 ALTER TABLE `ott_ticket_import_agent` DISABLE KEYS */;
/*!40000 ALTER TABLE `ott_ticket_import_agent` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-19 17:33:46
