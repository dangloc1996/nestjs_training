-- mylocal.ott_ticket_import_agent definition

CREATE TABLE `ott_ticket_import_agent`
(
    `id`       int(255) NOT NULL AUTO_INCREMENT,
    `name`     varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `path`     varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `created_at`  timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE CURRENT_TIMESTAMP(6),
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 126
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;
