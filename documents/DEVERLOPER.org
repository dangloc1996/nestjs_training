#+TITLE: DEVELOPER GUIDES

* Tổng quan kiến trúc

Project sử dụng ~nestjs framwork~ để phát triển. Vì vậy, kiến trúc của
hệ thống được kế thừa từ ~nestjs framework~, có thể tra cứu kĩ hơn tại:

[[https://docs.nestjs.com/guards]]
  
Kiến trúc của hệ thống có thể minh họa đơn giản như hình sau:

#+CAPTION: Ảnh minh họa kiến trúc hệ thống
#+NAME: fig:architecture
[[./img/layer-architecture.png]]

Trong đó:

- /Feedback-citizen's controllers/: bao gồm các class để định nghĩa
  endpoint và nhận các request tương ứng từ phía client.
  
- /Auth guards/: chứa các class với mục đích kiểm tra ~role~
  và ~permission~ của user

- /Feedback-citizen's service/ và /Util service/: bao gồm các class service
  của hệ thống, với mục đích xử lý logic, truy cập đến dữ liệu từ
  database và các hệ thống ngoài.

- /Proxies/: gồm các class với mục đích kết nối với các hệ
  thống ngoài.

* Cài đặt môi trường phát triển

** Requirment
  
Với developer, cần cài đặt:
- ~git~
- ~nodejs~ version 16
- ~yarn~

Với database, nên deploy trước server dev và sau đấy connect đến
database. Cách thức deploy nằm trong ~INSTALL.org~.

** Install

Sau đó fetch repository và install node_modules

#+BEGIN_SRC bash
  git clone git@git.mylocal.vn:development/mylocal-ott-be.git
  yarn install
#+END_SRC

Trước khi khởi động server, cần trước tiên cần config và tạo một số
directory.

#+BEGIN_SRC bash
  cp .env.example .env
  mkdir -p public/import-agent-log &&  \
      mkdir -p public/import-agent-template && \
      mkdir -p public/import-agent-validate
  cp -r .config.dev .config
#+END_SRC

Sau đó, update nội dung file .env với các giá trị tương ứng.

** Start server

#+BEGIN_SRC bash
  yarn start:dev
#+END_SRC

Sau khi chạy command, server sẽ tự động chạy và update khi có sự thay
đổi của code trong ~src/~.

** Develoment Guidelines

- Khi cần một API mới, developer tạo controller và service tương ứng.
- Nếu API mới cần table trong db mới, đầu tiên viết script để tạo
  tables, sau đó, viết code cho entity tương ứng, đồng thời update
  script ở trong ~src/console/services/console.service.ts~.
- Trước khi commit code và push, cần chạy ~yarn build~ và ~yarn lint~
  để kiểm tra lỗi và format code.
  
* Deployment guide

Sử dụng docker và xem chi tiết ở ~INSTALL.org~.
