# MyLocal API

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ yarn
$ cp .config.dev .config
$ cp .env.example .env
```

## Running the app

# development
```bash
$ yarn start:dev
```

## Generate new service

```bash
$ yarn add typeorm @nestjs/typeorm mysql2 @nestjs/microservices

```

## Style Guid

https://git.mylocal.vn/haivn1.as/mylocal-api/-/wikis/Styleguide

## Flow chart

https://app.diagrams.net/#G1uLURuQNiO_DTVeNAO4-eYnOnUWm95fzU

## Git flow:

https://git.mylocal.vn/haivn1.as/mylocal-api/-/wikis/Git-Flow
