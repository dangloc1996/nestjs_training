import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { NotFound, BadRequest, Forbidden } from '@common/errors';
import { Role, LOCK_NUMBER } from '@auth/enum';
import { AuthProxy } from '@proxy/auth';
import { ChatProxy } from '@proxy/chat';

import { Ticket, Staff } from '../entities';
import { getManager, In, Repository } from 'typeorm';
import {
  GetTokenDto,
  UpdateAgentDto,
  ListAgentQueryDto,
  CreateAgentDto,
  DeleteAgentQueryDTO,
} from '../dto';
import { BASetting } from '@feedback-citizen/entities';
import { FieldConsultation } from '@feedback-citizen/entities/field-consultation.entity';
import { BaSettingService } from '@feedback-citizen/services/ba-setting.service';
import { ActiveLocationService } from './active-location.service';

import { UtilService } from '@util/services';

@Injectable()
export class AgentService {
  constructor(
    private readonly authProxy: AuthProxy,
    private readonly chatProxy: ChatProxy,
    private readonly baService: BaSettingService,
    private readonly utilService: UtilService,
    private readonly activeLocationService: ActiveLocationService,
    @InjectRepository(Staff)
    private staffRepository: Repository<Staff>,
    @InjectRepository(BASetting)
    private baSettingRepository: Repository<BASetting>,
    @InjectRepository(FieldConsultation)
    private fieldConsultationRepository: Repository<FieldConsultation>,
    @InjectRepository(Ticket)
    private ticketRepository: Repository<Ticket>,
  ) {}
  async getToken(getTokenDto: GetTokenDto) {
    return await this.authProxy.getToken(
      getTokenDto.phone_number,
      getTokenDto.password,
    );
  }

  _validatePhoneNumber(value: string) {
    if (typeof value !== 'string') return false;

    if (value && value.length > 0) {
      if (!value.match(/^[0-9]+$/)) {
        return false;
      }
      return (
        (value.startsWith('84') && value.length === 11) ||
        (value.startsWith('0') && value.length === 10)
      );
    }
    return false;
  }

  _validateLocation(info) {
    for (const i of info) {
      if (!i.city || !i.district || !i.ward || !i.group_id || !i.field) {
        throw new BadRequest(
          'city | district | ward | group | field is required',
        );
      }
    }
  }

  async _validateTicketStatus(assignee: string) {
    const remainTicket = await this.ticketRepository.findOne({
      where: [
        {
          assignee: +assignee,
          status: 'INPROGRESS',
        },
        {
          assignee: +assignee,
          status: 'ASSIGNED',
        },
      ],
      select: ['id'],
    });

    if (remainTicket) {
      throw new BadRequest(
        'Tư vấn viên này đang xử lý yêu cầu hỗ trợ. Vui lòng đóng yêu cầu hỗ trợ trước khi thay đổi.',
      );
    }
  }

  async _validateTicketStatusWhere(
    assignee: string,
    field: string,
    city: string,
    district: string,
    ward: string,
  ) {
    const query = [
      {
        status: 'ASSIGNED',
        assignee: +assignee,
        city,
        district: district,
        ward: ward,
        field: +field,
      },
      {
        status: 'INPROGRESS',
        assignee: +assignee,
        city,
        district: district,
        ward: ward,
        field: +field,
      },
    ];
    const remainTicket = await this.ticketRepository.findOne({
      where: query,
      select: ['id'],
    });

    if (remainTicket) {
      throw new BadRequest(
        'Tư vấn viên này đang xử lý yêu cầu hỗ trợ. Vui lòng đóng yêu cầu hỗ trợ trước khi thay đổi.',
      );
    }
  }

  async createAgent(data: CreateAgentDto, user?) {
    data = await this.utilService.autoFillInfo(data);
    const [info, agentId] = await this.validateCreateOrUpdateUser(data, user);

    return await this.createOrUpdateAgent(data, info, agentId, true);
  }

  async validateCreateOrUpdateUser(data: CreateAgentDto, user) {
    if (!data.hasOwnProperty('name')) throw new BadRequest('name is required');

    if (!data.hasOwnProperty('user_id'))
      throw new BadRequest('user_id is required');

    if (!data.hasOwnProperty('phone_number'))
      throw new BadRequest('phone_number is required');

    if (!data.hasOwnProperty('email'))
      throw new BadRequest('email is required');

    if (!data.hasOwnProperty('info')) throw new BadRequest('info is required');

    this._validateLocation(data.info);

    let checkingFrameList = [];

    for (const i of data.info) {
      checkingFrameList.push(
        this.utilService.valueCheckingFieldWithNumberId(i.field, i.group_id),
      );
      checkingFrameList.push(
        this.utilService.valueCheckingLocation(i.city, i.district, i.ward),
      );
    }
    await Promise.all(checkingFrameList);
    checkingFrameList = [];

    for (const i of data.info) {
      checkingFrameList.push(
        this.activeLocationService.checkActiveCity(i.city),
      );
    }
    const citiesIsValid = await Promise.all(checkingFrameList);

    for (const v of citiesIsValid) {
      if (!v) {
        throw new BadRequest(
          'Tỉnh/ Thành phố chưa sử dụng dịch vụ của myLocal. Xin vui lòng chọn Tỉnh, Thành phố khác.',
        );
      }
    }

    if (user) {
      const isAdmin = !!user.info.find((info) => info.role == Role.ADMIN);

      if (!isAdmin) {
        for (const i of data.info) {
          const authorized = this.utilService.permissionPowerChecking(user, i);
          if (!authorized) {
            throw new Forbidden('You do not have permission to do this.');
          }
        }
      }
    }

    if (!this._validatePhoneNumber(data.phone_number))
      throw new BadRequest('phone_number invalid');

    let staff = undefined;
    try {
      if (user) {
        staff = await this.utilService.mergedStaffDetailWithPermission(
          user,
          +data.user_id,
        );
      } else {
        staff = await this.utilService.mergedStaffDetail(+data.user_id);
      }
    } catch (error) {
      // console.log(error);
    }

    if (staff && staff.info.length > 0) {
      for (const u of staff.info) {
        for (const v of data.info) {
          const uRole = u.role > LOCK_NUMBER ? u.role - LOCK_NUMBER : u.role;
          const vRole = v.role > LOCK_NUMBER ? v.role - LOCK_NUMBER : v.role;
          if (uRole != vRole) {
            throw new BadRequest(
              'Vai trò của người dùng được gán không hợp lệ.',
            );
          }
        }
      }

      const updateAll = data.email != staff.email || data.name != staff.name;

      const updateInfo = [];
      const updateIdSet = new Set();
      const existIdSet = new Set();
      const deleteStaffs = [];
      let agentId = staff.info[0].agentId;

      if (!updateAll) {
        for (const u of data.info) {
          let exist = false;

          for (const v of staff.info) {
            if (
              +u.role == +v.role &&
              u.city == v.city &&
              u.district == v.district &&
              u.ward == v.ward &&
              u.group_id == v.group_id &&
              +u.field == +v.field
            ) {
              exist = true;
              existIdSet.add(v.id);
              break;
            }
          }

          if (!exist) {
            updateInfo.push(u);
            if (u['id']) {
              updateIdSet.add(u['id']);
            }
          }
        }
      } else {
        for (const u of staff.info) {
          updateInfo.push(u);
        }

        for (const u of data.info) {
          let exist = false;

          for (const v of staff.info) {
            if (
              +u.role == +v.role &&
              u.city == v.city &&
              u.district == v.district &&
              u.ward == v.ward &&
              u.group_id == v.group_id &&
              +u.field == +v.field
            ) {
              exist = true;
              existIdSet.add(v.id);
              break;
            }
          }

          if (!exist) {
            updateInfo.push(u);
          }
        }
      }

      for (const u of staff.info) {
        const uId = u.id;
        if (updateIdSet.has(uId) && !existIdSet.has(uId)) {
          deleteStaffs.push(u);
        }
      }

      return [updateInfo, agentId, deleteStaffs];
    }

    return [data.info, undefined, []];
  }

  async createOrUpdateAgent(
    data: CreateAgentDto,
    info,
    agentId: string,
    isCreate = false,
  ) {
    const mappingGroupID = await this.baService.mappingNumberIdToGroupId();
    const agentsData: Array<Staff> = [];

    if (!agentId) {
      const agent = await this.chatProxy.registerAgent(data);
      agentId = agent.data.agent_id;
    }

    for (const i of info) {
      const agent = {
        name: data.name,
        user_id: data.user_id,
        phone_number: data.phone_number,
        email: data.email,
        city: i.city,
        district: i.district,
        ward: i.ward,
        field: i.field,
        group_id: mappingGroupID[i.group_id],
        role: +i.role,
        agent_id: agentId,
      } as Staff;
      if (i.id) {
        agent.id = +i.id;
      }
      agentsData.push(agent);
      await this.addGroup(agent.agent_id, info);
      // NOTE: Send message after create
      if (isCreate) {
        const messages = [
          {
            content: await this.utilService.createAgentMessage(
              i.city,
              i.district,
              i.ward,
              +i.role,
            ),
          },
          {
            link: `${process.env.WEB_URL}`,
          },
        ];
        this.chatProxy.sendMessage(data.user_id, i.group_id, messages);
      }
    }

    const result = await this.staffRepository.save(agentsData);
    return !!result;
  }

  async addGroup(agent_id, groupIDs) {
    const mappingGroupID = await this.baService.mappingNumberIdToGroupId();
    const listPromiseGroup: Array<any> = [];
    for (const i of groupIDs) {
      listPromiseGroup.push(
        this.chatProxy.addGroup(agent_id, mappingGroupID[i.group_id]),
      );
    }
    await Promise.all(listPromiseGroup);
  }

  async findUserByPhone(phone: string, accessToken: string) {
    const user = await this.authProxy.getUserByPhone(phone, accessToken);
    return {
      user_id: user.user_id,
      name: user.name,
      email: user.email,
      phone_number: user.phone_number,
      avatar_url: user.avatar_url,
    };
  }

  async getLocation() {
    return await this.authProxy.getLocation();
  }

  async deleteAgent(id: number, user) {
    const staff = await this.staffRepository.findOne({
      where: { id },
      select: ['city', 'district', 'ward', 'group_id', 'agent_id'],
    });

    await this._validateTicketStatusWhere(
      String(staff.id),
      staff.field,
      staff.city,
      staff.district,
      String(staff.field),
    );

    this.utilService.locationPermissionChecking(
      user,
      {
        city: staff.city,
        district: staff.district,
        ward: staff.ward,
      },
      true,
    );

    const count = await this.staffRepository.count({
      where: { group_id: staff.group_id, agent_id: staff.agent_id },
    });

    if (count == 1) {
      await this.chatProxy.deleteGroupAgent(staff.group_id, staff.agent_id);
    }

    return await this.staffRepository.delete(id);
  }

  async deleteAgentWithUserId(
    userId: number,
    deleteQuery: DeleteAgentQueryDTO,
    user?: any,
  ) {
    await this._validateTicketStatus(String(userId));

    const numberId = deleteQuery.numberId;

    const query = {
      user_id: userId,
    };

    if (numberId) {
      const group_id = await this.utilService.getGroupIdFromNumberId(numberId);
      query['group_id'] = group_id;
    }

    const staffList = await this.staffRepository.find({
      where: query,
      select: ['id', 'city', 'district', 'ward', 'agent_id', 'group_id'],
    });

    const deleteStaffList = [];
    const deleteAgentIdMap = {};

    for (const staff of staffList) {
      const authorized = user
        ? this.utilService.locationPermissionChecking(user, {
            city: staff.city,
            district: staff.district,
            ward: staff.ward,
          })
        : true;
      if (authorized) {
        deleteStaffList.push(staff);
        if (!deleteAgentIdMap[staff.agent_id]) {
          deleteAgentIdMap[staff.agent_id] = staff.group_id;
        }
      }
    }

    const deleteFrames = [];
    for (const agentId in deleteAgentIdMap) {
      const groupId = deleteAgentIdMap[agentId];
      deleteFrames.push(this.chatProxy.deleteGroupAgent(groupId, agentId));
    }
    await Promise.all(deleteFrames);
    return await this.staffRepository.delete(
      deleteStaffList.map((staff) => staff.id),
    );
  }

  async fieldList(id: string) {
    const groupID = await this.convertFromNumberIdToGroupId(id);
    const listField = await this.fieldConsultationRepository.find({
      group_id: groupID,
    });
    if (listField && listField.length > 0) {
      return listField.map((field) => {
        return { id: field.id, name: field.name };
      });
    }
    return [];
  }

  async getAssigneeTicketStatus(users: number[]) {
    const aCountQuery = this.ticketRepository
      .createQueryBuilder()
      .select(['count(id)', 'assignee'])
      .groupBy('assignee')
      .where(
        users.map((user) => {
          return { assignee: user, status: 'ASSIGNED' };
        }),
      );
    const iCountQuery = this.ticketRepository
      .createQueryBuilder()
      .select(['count(id)', 'assignee'])
      .groupBy('assignee')
      .where(
        users.map((user) => {
          return { assignee: user, status: 'INPROGRESS' };
        }),
      );
    const dCountQuery = this.ticketRepository
      .createQueryBuilder()
      .select(['count(id)', 'assignee'])
      .groupBy('assignee')
      .where(
        users.map((user) => {
          return { assignee: user, status: 'DONE' };
        }),
      );
    const [aCountRaw, iCountRaw, dCountRaw] = await Promise.all([
      aCountQuery.getRawMany(),
      iCountQuery.getRawMany(),
      dCountQuery.getRawMany(),
    ]);
    const result = {};

    for (const row of aCountRaw) {
      if (!result[row.assignee]) result[row.assignee] = {};
      result[row.assignee]['ASSIGNEE'] = row['count(id)'];
    }

    for (const row of iCountRaw) {
      if (!result[row.assignee]) result[row.assignee] = {};
      result[row.assignee]['INPROGRESS'] = row['count(id)'];
    }

    for (const row of dCountRaw) {
      if (!result[row.assignee]) result[row.assignee] = {};
      result[row.assignee]['DONE'] = row['count(id)'];
    }

    for (const user in result) {
      if (!result[user]['ASSIGNEE']) result[user]['ASSIGNEE'] = 0;
      if (!result[user]['INPROGRESS']) result[user]['INPROGRESS'] = 0;
      if (!result[user]['DONE']) result[user]['DONE'] = 0;
    }

    return result;
  }

  async listAgent(param: ListAgentQueryDto, user) {
    let { city, district, ward, type, biz, page_size, page } = param;
    if (!page_size) {
      page_size = 10000;
    }
    if (!page) {
      page = 0;
    }
    const isAdmin = !!user.info.find((info) => info.role == Role.ADMIN);

    let query = [];
    let group_id = undefined;
    if (biz) {
      group_id = await this.convertFromNumberIdToGroupId(biz);
    }

    if (!isAdmin) {
      const cloneInfo = [...user.info];

      for (const staff of cloneInfo) {
        staff.role = +staff.role;
        if (staff.role != Role.AGENT) {
          if (city && staff.city != city) {
            continue;
          }

          if (
            district &&
            staff.district != district &&
            staff.role != Role.ADMIN_CITY
          ) {
            continue;
          } else if (district || staff.role == Role.ADMIN_CITY) {
            staff.district = district;
          }
          if (ward && staff.ward !== ward && staff.role == Role.ADMIN_WARD) {
            continue;
          } else if (ward || staff.role != Role.ADMIN_WARD) {
            staff.ward = ward;
          }

          staff.group_id = group_id;
          staff.field = type;

          query.push({
            role: this.utilService.roleLessPowerResovle(staff.role),
            city: staff.city,
            district: staff.district,
            ward: staff.ward,
            field: staff.field,
            group_id: staff.group_id,
          });
        }
      }
    } else {
      query.push({
        role: this.utilService.roleLessPowerResovle(Role.ADMIN),
        city,
        ward,
        district,
        group_id,
        field: type,
      });
    }

    if (query.length == 0) {
      throw new Forbidden('You cannot see those agents!');
    }

    const staffListData = await this.utilService.mergedStaffList(
      query,
      +page_size,
      +page * +page_size,
    );
    const staffList = staffListData.data;
    const total = staffListData.total;

    if (staffList.length == 0) {
      return {
        listTotal: total,
        list: [],
      };
    }

    const userIdList = [];
    const groupIdList = [];
    const fieldSet = new Set<string>();

    for (const staff of staffList) {
      userIdList.push(+staff.user_id);
      for (const info of staff.info) {
        fieldSet.add(info.field);
        groupIdList.push(info.group_id);
      }
    }

    const [fieldMap, groupMap] = await Promise.all([
      this.utilService.getTypes(fieldSet),
      this.utilService.getGroupsByNumberIds(groupIdList),
    ]);
    const result = staffList.map((staff) => {
      for (const info of staff.info) {
        info.fieldName = fieldMap[info.field];
        info.groupName = groupMap[info.group_id];
      }
      return staff;
    });

    if (Math.min(page_size, total) <= 100000) {
      const ticketStatus = await this.getAssigneeTicketStatus(userIdList);

      for (const staff of staffList) {
        const stringId = String(staff.user_id);
        const status = ticketStatus[stringId];
        staff['require_done'] = 0;
        staff['require_processing'] = 0;
        if (status) {
          const dStatus = ticketStatus[stringId]['DONE'];
          const pStatus = ticketStatus[stringId]['INPROGRESS'];
          if (dStatus) {
            staff['require_done'] = dStatus;
          }
          if (pStatus) {
            staff['require_processing'] = pStatus;
          }
        }
      }
    }

    for (const staff of result) {
      for (const a of staff.info) {
        if (a.role > LOCK_NUMBER) {
          a.role = String(a.role - LOCK_NUMBER);
          a['isLocked'] = true;
        } else {
          a['isLocked'] = false;
        }
      }
    }

    return {
      listTotal: total,
      list: result,
    };
  }

  async updateAgent(user_id: number, data: UpdateAgentDto, user?) {
    data = await this.utilService.autoFillInfo(data);
    const [info, agentId, deleteStaffs] = await this.validateCreateOrUpdateUser(
      data,
      user,
    );

    const validateFrames = deleteStaffs.map((staff) =>
      this._validateTicketStatusWhere(
        String(staff.id),
        staff.field,
        staff.city,
        staff.distrcit,
        String(staff.field),
      ),
    );

    await Promise.all(validateFrames);
    const deleteIds = deleteStaffs.map((staff) => staff.id);
    await this.staffRepository.delete([deleteIds]);
    return this.createOrUpdateAgent(data, info, agentId);
  }

  async getListGroup(user: any) {
    let listGroupId;
    const listBa = await this.baSettingRepository.find();
    const listMapping = await this.listMappingFieldInBA();

    const isAdmin = !!user.info.find(
      (info) =>
        info.role == Role.ADMIN ||
        info.role == Role.ADMIN_CITY ||
        info.role == Role.ADMIN_DISTRICT ||
        info.role == Role.ADMIN_WARD,
    );

    if (!isAdmin) {
      const listStaff = await this.staffRepository.find({
        where: {
          user_id: String(user.user_id),
        },
        select: ['group_id'],
      });
      listGroupId = listStaff.map((st) => listMapping[st.group_id]);
    } else {
      listGroupId = listBa.map((ba) => ba.number_id);
    }
    const baST = await this.baSettingRepository.find({
      where: { number_id: In(listGroupId) },
      order: {
        id: 'ASC',
      },
    });
    return baST.map((ba) => {
      return {
        group_id: ba.number_id,
        group_name: ba.group_name,
        number: ba.number,
      };
    });
  }

  async getRole(user) {
    // const isLock = !!user.info.find((info) => info.role > LOCK_NUMBER);
    const isAdmin = !!user.info.find(
      (info) =>
        info.role == Role.ADMIN ||
        info.role == Role.ADMIN_CITY ||
        info.role == Role.ADMIN_DISTRICT ||
        info.role == Role.ADMIN_WARD,
    );
    const isAgent = !!user.info.find((info) => info.role == Role.AGENT);
    let role = 0;
    if (isAdmin) {
      role = 1;
    }
    if (isAgent) {
      role = 2;
    }
    // if (isLock) {
    //   role = -1;
    // }
    return { role };
  }

  async detailAgent(user, userId: number) {
    const staff = await this.utilService.mergedStaffDetailWithPermission(
      user,
      userId,
    );
    if (staff && staff.info.length == 0) {
      throw new Forbidden('You cannot see this.');
    }
    for (const a of staff.info) {
      if (a.role > LOCK_NUMBER) {
        a.role = String(a.role - LOCK_NUMBER);
        a['isLocked'] = true;
      } else {
        a['isLocked'] = false;
      }
    }

    return staff;
  }

  async listMappingFieldInBA() {
    const ba = await this.baSettingRepository.find();
    if (ba && ba.length == 0) {
      throw new BadRequest('Biz account not exist');
    }
    const listMapping: Record<string, string> = {};
    for (const b of ba) {
      listMapping[b.group_id] = b.number_id;
    }
    return listMapping;
  }

  async convertFromNumberIdToGroupId(number_id: string) {
    const ba = await this.baSettingRepository.findOne({
      number_id: number_id,
    });
    if (!ba) {
      throw new BadRequest('biz account not exist');
    }
    return ba.group_id;
  }

  async listPhoneNumber() {
    const list = await this.staffRepository.find();
    return list.map(
      (a) =>
        `${a.phone_number}${a.group_id}${a.field}${a.city}${a.district}${a.ward}`,
    );
  }

  async getListFilter(biz_id: string) {
    let query: any = null;
    if (biz_id) {
      const groupID = await this.convertFromNumberIdToGroupId(biz_id);
      query = `select DISTINCT user_id, name from ott_ticket_staff where group_id = '${groupID}'`;
    } else {
      query = `select DISTINCT user_id, name from ott_ticket_staff`;
    }
    return await getManager().query(query);
  }

  async sendMailNoticeSuccessForAdmin(
    email: string,
    message: string,
    token: string,
  ) {
    await this.authProxy.sendMailNoticeSuccessForAdmin(email, message, token);
  }

  async updateRole(role: number, staffId: number, user: any) {
    if (staffId == user.id)
      throw new BadRequest('Không được cập nhật vai trò cho chính mình');
    const staff = await this.staffRepository.findOne({
      where: { id: staffId },
    });

    await this._validateTicketStatusWhere(
      String(staff.id),
      staff.field,
      staff.city,
      staff.district,
      String(staff.field),
    );

    this.utilService.locationPermissionChecking(
      user,
      {
        city: staff.city,
        district: staff.district,
        ward: staff.ward,
      },
      true,
    );

    this.utilService.permissionPowerChecking(user, {
      role: staff.role,
      city: staff.city,
      district: staff.district,
      ward: staff.ward,
    });

    staff.role = role;
    return !!(await this.staffRepository.save(staff));
  }

  async lockStaff(staffId: number, user: any) {
    const staff = await this.staffRepository.findOne({
      where: {
        id: staffId,
      },
    });

    if (!staff) {
      throw new NotFound('Staff not exist!');
    }

    const authorized = this.utilService.permissionPowerChecking(user, {
      role: staff.role,
      city: staff.city,
      district: staff.district,
      ward: staff.ward,
    });

    if (!authorized) {
      throw new Forbidden(
        'User do not have the permission to unlock this staff!',
      );
    }

    await this._validateTicketStatusWhere(
      String(staff.id),
      staff.field,
      staff.city,
      staff.district,
      String(staff.field),
    );
    if (staff.role < LOCK_NUMBER) {
      staff.role = staff.role + LOCK_NUMBER;
    }

    await this.staffRepository.save(staff);

    return { success: true };
  }

  async unlockStaff(staffId: number, user: any) {
    const staff = await this.staffRepository.findOne({
      where: {
        id: staffId,
      },
    });

    if (!staff) {
      throw new NotFound('Staff not exist!');
    }

    const authorized = this.utilService.permissionPowerChecking(user, {
      role: staff.role,
      city: staff.city,
      district: staff.district,
      ward: staff.ward,
    });

    if (!authorized) {
      throw new Forbidden(
        'User do not have the permission to unlock this staff!',
      );
    }

    if (staff.role > LOCK_NUMBER) {
      staff.role = staff.role - LOCK_NUMBER;
    }
    await this.staffRepository.save(staff);

    return { success: true };
  }
}
