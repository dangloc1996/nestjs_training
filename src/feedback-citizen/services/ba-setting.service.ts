import { Injectable } from '@nestjs/common';
import { ChatProxy } from '@proxy/chat';
import { InjectRepository } from '@nestjs/typeorm';
import { BASetting } from '@feedback-citizen/entities';
import { IsNull, Repository } from 'typeorm';

import { CreateSettingBaDto } from '@feedback-citizen/dto';
import { Forbidden } from '@common/errors';

@Injectable()
export class BaSettingService {
  covidNumberId = undefined;

  constructor(
    private readonly chatProxy: ChatProxy,
    @InjectRepository(BASetting)
    private baSettingRepository: Repository<BASetting>,
  ) {
    this.covidNumberId = process.env.COVID_NUMBER_ID;
  }

  async initSettingBa() {
    const baSetting = await this.baSettingRepository.find({
      where: {
        group_id: IsNull(),
      },
    });
    const promiseSetting: Array<any> = [];
    if (baSetting && baSetting.length > 0) {
      for (const ba of baSetting) {
        promiseSetting.push(await this.saveInfoSetting(ba));
      }
    }
    Promise.all(promiseSetting)
      .then((success) => {
        console.log(success);
        console.log('init ba setting success');
      })
      .catch((err) => {
        console.log(err.toString());
      });
    return true;
  }

  async saveInfoSetting(ba: any) {
    const result = await this.chatProxy.createSettingBA(ba);
    ba.queue_id = result.data.queue_id;
    ba.group_id = result.data.group_id;
    ba.routing_id = result.data.routing_id;
    await ba.save();
  }

  async createSettingBa(ba: CreateSettingBaDto) {
    const dbBA = new BASetting();
    dbBA.number = ba.number;
    dbBA.number_id = ba.number_id;
    dbBA.group_name = ba.group_name;
    return await this.baSettingRepository.save(dbBA);
  }

  async listBiz() {
    const listNameCode: Record<string, any> = {};
    const listBA = await this.baSettingRepository.find();
    for (const ba of listBA) {
      listNameCode[ba.group_name] = {
        number_id: ba.number_id,
        group_id: ba.group_id,
      };
    }
    return listNameCode;
  }

  async mappingNumberIdToGroupId() {
    const listBA = await this.baSettingRepository.find();
    const result: Record<any, any> = {};
    for (const ba of listBA) {
      result[ba.number_id] = ba.group_id;
    }
    return result;
  }

  async getGroupIdFromNumberId(numberId: string) {
    const result = await this.baSettingRepository.findOne({
      where: { number_id: numberId },
      select: ['group_id'],
    });
    if (!result) {
      throw new Forbidden('Number ID not exist!');
    }
    return result.group_id;
  }
}
