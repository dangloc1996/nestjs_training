import { Injectable } from '@nestjs/common';

import { AuthProxy } from '@proxy/auth';
import { ChangePasswordDto } from '../dto';

@Injectable()
export class AccountService {
  constructor(private readonly authProxy: AuthProxy) {}

  async changePassword(user, data: ChangePasswordDto, jwt: string) {
    await this.authProxy.getToken(user.phone_number, data.oldPassword);
    await this.authProxy.changePassword(data.newPassword, jwt);
    return {
      sucsess: true,
    };
  }
}
