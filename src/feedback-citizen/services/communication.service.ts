import { Injectable } from '@nestjs/common';
import { ChatProxy } from '@proxy/chat';
import { CallQueryDTO } from '../dto';

@Injectable()
export class CommunicationService {
  constructor(private readonly chatProxy: ChatProxy) {}

  getCallHistory(numberId: string) {
    return this.chatProxy.getStringeeEvents(numberId);
  }

  getCallHistoryV2(numberId: string, query: CallQueryDTO) {
    return this.chatProxy.getStringeeCalls(numberId, query);
  }
}
