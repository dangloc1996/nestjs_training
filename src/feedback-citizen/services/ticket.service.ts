import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, LessThan, MoreThan, Between, Like } from 'typeorm';

import { Role } from '@auth/enum';
import { ChatProxy } from '@proxy/chat';
import { S3Service } from '@proxy/aws';
import { UtilService } from '@util/services';

import { BadRequest, Forbidden } from '@common/errors';
import * as utils from '../utils';
import { Ticket, Staff } from '../entities';
import {
  ClientTicketDto,
  TicketQueryDto,
  AnswerTicketDto,
  VaccineTicketDto,
  VaccineReplyTicketDto,
  CloseTicketDto,
  MultipleAssignDto,
  MyTicketQueryDto,
} from '../dto';
import * as serializer from '../serializers';
import { ActiveLocationService } from './active-location.service';
import { BaSettingService } from './ba-setting.service';
import { FieldConsultationService } from './field-consultation.service';
import { TicketQuery } from '../type';

@Injectable()
export class TicketService {
  constructor(
    private readonly utilService: UtilService,
    private readonly chatProxy: ChatProxy,
    private readonly s3Service: S3Service,
    private readonly activeLocationService: ActiveLocationService,
    @InjectRepository(Ticket)
    private ticketRepository: Repository<Ticket>,
    @InjectRepository(Staff)
    private staffRepository: Repository<Staff>,
    private baSettingService: BaSettingService,
    private fieldService: FieldConsultationService,
  ) {}

  async uploadImages(
    user: any,
    images: Array<Express.Multer.File>,
    imagesName?: Array<string>,
  ): Promise<string[]> {
    let path = undefined;
    if (!utils.AllIsImage(images)) {
      throw new Error('Files must be images!');
    }
    if (imagesName && imagesName.length > 0) {
      this.deleteImages(imagesName);
      path = utils.GetImagePath(imagesName[0]);
    } else {
      path = utils.GenImagePath(user.user_id);
    }
    const result = [];
    await Promise.all(
      images.map((image, index) => {
        const extension = utils.GetExtension(image.originalname);
        const key = `${path}/${index}.${extension}`;
        result.push(key);
        return this.s3Service.publicUpload(key, image.buffer);
      }),
    );

    return result;
  }

  async deleteImages(images: Array<string>) {
    images.forEach((image) => {
      this.s3Service.deleteObject(image);
    });
  }

  async downloadImage(key: string) {
    if (!utils.CheckValidKey(key)) throw new BadRequest('key not valid');
    return this.s3Service.getObject(key);
  }

  async getTicket(id: string) {
    try {
      // const response = (await this.chatProxy.getGeneralTicket(id)).data;
      const ticket = await this.ticketRepository.findOne(id);
      const result = serializer.DbTicketSerialize(ticket);
      return {
        id: id,
        ...result,
      };
    } catch (error) {
      throw new BadRequest(error.message);
    }
  }

  async createTicket(
    images: Array<Express.Multer.File>,
    data: ClientTicketDto,
    user: any,
    jwt: string,
  ) {
    if (data.hasImages == 'true') {
      data.images = await this.uploadImages(user, images);
    }

    const invalidField = await this.fieldService.getSpecial(
      process.env.SPECIAL_FIELD_VACCINE,
    );

    if (+invalidField.id == +data.type) {
      throw new BadRequest('Invalid field');
    }

    data.assignee = undefined;
    data.creator = user.user_id;
    data['numberId'] = data.biz;

    delete data.biz;

    await Promise.all([
      this.utilService.valueCheckingLocation(
        data.city,
        data.district,
        data.ward,
      ),
      this.utilService.valueCheckingFieldWithNumberId(
        +data.type,
        data['numberId'],
      ),
    ]);

    const locationIsActive = await this.activeLocationService.checkActiveCity(
      data.city,
    );

    if (!locationIsActive) {
      throw new BadRequest(
        'Tỉnh/ Thành phố chưa sử dụng dịch vụ của myLocal. Xin vui lòng chọn Tỉnh, Thành phố khác.',
      );
    }

    // NOTE(): sync with chat service
    let id = undefined;
    {
      const requestData = serializer.ApiTicketSerialize(data);
      requestData['extra']['userPhone'] = user.phone_number;
      id = (await this.chatProxy.createGeneralTicket(requestData))?.data?.id;
    }
    if (id) {
      const insertData = serializer.InsertDbTicketSerialize(data);
      insertData.id = id;
      insertData.user_phone = user.phone_number;

      const response = await this.ticketRepository.save(insertData);

      const ticket = await this.getTicketWithUserInfo(response.id, user, jwt);

      {
        const messages = [
          {
            content: utils.CreateTicketMessage(ticket['bizName']),
          },
          {
            link: `${process.env.WEB_URL}/ticket-public/${ticket['id']}`,
          },
        ];
        this.chatProxy.sendMessage(
          String(ticket.creator),
          ticket.numberId,
          messages,
        );
      }
      return ticket;
    }
  }

  async vaccineRegistrationField() {
    const result = await this.fieldService.getSpecial(
      process.env.SPECIAL_FIELD_VACCINE,
    );
    return result;
  }

  async syncUpdateTicketChatSerivce(id: string, ticket: Ticket) {
    try {
      const vaccineField = await this.vaccineRegistrationField();

      if (vaccineField.id != ticket.field) {
        const clientData = serializer.DbTicketSerialize(ticket);

        const serializeData = serializer.ApiTicketSerialize(clientData);

        await this.chatProxy.updateGeneralTicket(id, serializeData);
      } else {
        const clientData = serializer.DbVaccineTicketSerialize(ticket);
        clientData.type = String(vaccineField.id);
        const serializeData = serializer.ApiVaccineTicketSerialize(clientData);

        await this.chatProxy.updateGeneralTicket(id, serializeData);
      }
    } catch (error) {
      console.log('SYNC ERROR:', JSON.stringify(error, null, 4));
    }
  }

  async updateTicket(
    images: Array<Express.Multer.File>,
    id: string,
    data: ClientTicketDto,
    user: any,
  ) {
    const oldTicket = await this.ticketRepository.findOne(id);
    const oldData = serializer.DbTicketSerialize(oldTicket);
    if (
      user.role != Role.ADMIN &&
      user.user_id != data.creator &&
      oldTicket.status != 'DONE'
    ) {
      const msg = 'You can not update this ticket!';
      throw new Forbidden(msg);
    }

    if (data.hasImages == 'true') {
      if (images && images.length > 0) {
        data.images = await this.uploadImages(
          user,
          images,
          oldData.images as Array<string>,
        );
      } else {
        this.deleteImages(oldData.images as Array<string>);
      }
    }

    data.creator = undefined;
    data.assignee = undefined;
    data = { ...oldData, ...data };

    await Promise.all([
      this.utilService.valueCheckingLocation(
        data.city,
        data.district,
        data.ward,
      ),
      this.utilService.valueCheckingFieldWithNumberId(+data.type, data.biz),
    ]);

    const insertData = serializer.InsertDbTicketSerialize(data);
    const response = await this.ticketRepository.save(insertData);

    // NOTE(): sync with chat service
    {
      this.syncUpdateTicketChatSerivce(id, insertData);
    }

    return response;
  }

  async closeTicket(id: string, user, data: CloseTicketDto) {
    const ticket = await this.ticketRepository.findOne(id);

    const isAdmin = !!user.info.find((info) => {
      return (
        info.role == Role.ADMIN ||
        info.role == Role.ADMIN_CITY ||
        info.role == Role.ADMIN_DISTRICT ||
        info.role == Role.ADMIN_WARD
      );
    });

    if (!isAdmin) {
      if (ticket.assignee != user.user_id) {
        const msg = 'You can not close this ticket!';
        throw new Forbidden(msg);
      }
    } else {
      this.utilService.locationPermissionChecking(
        user,
        {
          city: ticket.city,
          district: ticket.district,
          ward: ticket.ward,
        },
        true,
      );
    }

    if (data.reason) {
      ticket['extra']['reason'] = data.reason;
    }

    ticket.status = 'DONE';
    ticket.completed_at = new Date(Date.now());
    const result = await this.ticketRepository.save(ticket);

    // NOTE(): sync with chat service
    {
      this.syncUpdateTicketChatSerivce(id, ticket);
    }

    return result;
  }

  async assignChecking(user_id: number, ticket: Ticket) {
    const groupId = await this.baSettingService.getGroupIdFromNumberId(
      ticket.number_id,
    );

    const query = {
      user_id: user_id,
      city: ticket.city,
      district: ticket.district,
      group_id: groupId,
      ward: ticket.ward,
      field: ticket.field,
    };

    // NOTE(): For covid consultant
    {
      const covidFieldId = (
        await this.fieldService.getSpecial('COVID_CONSULTANT')
      )?.id;
      if (
        ticket.number_id == this.baSettingService.covidNumberId &&
        ticket.field != covidFieldId
      ) {
        delete query.field;
      }
    }

    const staff = await this.staffRepository.findOne({
      where: query,
      select: ['phone_number'],
    });

    if (!staff) {
      throw new Forbidden('You can not assign to this user this ticket!');
    }

    return staff;
  }

  async selfAssignChecking(user: any, ticket: Ticket) {
    await this.assignChecking(user.user_id, ticket);
    const isAdmin = !!user.info.find((info) => info.role == Role.ADMIN);
    if (ticket.assignee != -1 && !isAdmin) {
      throw new Forbidden('You can not self assign this ticket!');
    }
  }

  async assignTicket(ticketId: string, agentId: string, user: any) {
    const ticket = await this.ticketRepository.findOne(ticketId);

    this.utilService.locationPermissionChecking(
      user,
      {
        city: ticket.city,
        district: ticket.district,
        ward: ticket.ward,
      },
      true,
    );

    const staff = await this.assignChecking(+agentId, ticket);
    ticket.status = 'ASSIGNED';
    ticket.assignee = +agentId;
    ticket.assigned_at = new Date(Date.now());
    ticket.assignee_phone = staff.phone_number;
    const result = await this.ticketRepository.save(ticket);

    // NOTE(): sync with chat service
    {
      this.syncUpdateTicketChatSerivce(ticketId, ticket);
    }

    return result;
  }

  async assignMultiTicketAssignableList(data: MultipleAssignDto, user: any) {
    if (data.ticketIds && data.ticketIds.length > 0) {
      const ticketsQuery = [];
      for (const ticketId of data.ticketIds) {
        ticketsQuery.push({
          id: ticketId,
        });
      }

      const tickets = await this.ticketRepository.find({
        where: ticketsQuery,
      });

      for (const ticket of tickets) {
        this.utilService.locationPermissionChecking(
          user,
          {
            city: ticket.city,
            district: ticket.district,
            ward: ticket.ward,
          },
          true,
        );
      }

      const staffsQuery = tickets.map((ticket) => {
        return {
          role: Role.AGENT,
          city: ticket.city,
          district: ticket.district,
          ward: ticket.ward,
          field: ticket.field,
        };
      });

      const staffs = await this.utilService.mergedStaffList(
        staffsQuery,
        1000,
        0,
      );

      if (tickets.length < 30) {
        const result = [];
        for (const staff of staffs.data) {
          let nextStaff = true;
          for (const ticket of tickets) {
            nextStaff = true;
            for (const info of staff.info) {
              if (
                ticket.field == info.field &&
                ticket.city == info.city &&
                ticket.district == info.district &&
                ticket.ward == info.ward
              ) {
                nextStaff = false;
                break;
              }
            }
            if (nextStaff) {
              break;
            }
          }
          if (!nextStaff) {
            result.push(staff);
          }
        }
        return {
          total: result.length,
          data: result,
        };
      } else {
        return staffs;
      }
    }
    return [];
  }

  async assignMultiTicket(data: MultipleAssignDto, user: any) {
    if (data.ticketIds && data.ticketIds.length > 0 && data.userId) {
      const ticketsQuery = [];
      for (const ticketId of data.ticketIds) {
        ticketsQuery.push({
          id: ticketId,
        });
      }

      const tickets = await this.ticketRepository.find({
        where: ticketsQuery,
      });

      for (const ticket of tickets) {
        this.utilService.locationPermissionChecking(
          user,
          {
            city: ticket['extra']['city'],
            district: ticket['extra']['district'],
            ward: ticket['extra']['ward'],
          },
          true,
        );
      }

      const query = tickets.map((ticket) => {
        return {
          role: Role.AGENT,
          city: ticket.city,
          district: ticket.district,
          ward: ticket.ward,
          field: ticket.field,
        };
      });

      const staff = await this.utilService.mergedStaffDetailWithPermission(
        user,
        data.userId,
        query,
      );

      const assignTickets = [];

      if (staff && staff.info && staff.info.length > 0) {
        for (const ticket of tickets) {
          let check = false;
          for (const info of staff.info) {
            if (
              ticket.field == info.field &&
              ticket.city == info.city &&
              ticket.district == info.district &&
              ticket.ward == info.ward
            ) {
              check = true;
              break;
            }
          }
          if (!check) {
            throw new BadRequest(
              'You can not assign those tickets to that user',
            );
          }
        }

        for (const ticket of tickets) {
          ticket.status = 'ASSIGNED';
          ticket.assignee = +staff.user_id;
          ticket.assignee_phone = staff.phone_number;
          ticket.assigned_at = new Date(Date.now());
          assignTickets.push(ticket);

          // NOTE(): sync with chat service
          {
            this.syncUpdateTicketChatSerivce(ticket.id, ticket);
          }
        }
      } else {
        throw new BadRequest('You can not assign those tickets to that user');
      }
      const result = await this.ticketRepository.save(assignTickets);
      return result;
    }
  }

  async selfAssignTicket(ticketId: string, user: any, jwt) {
    const ticket = await this.ticketRepository.findOne(ticketId);

    {
      const isAgent = !!user.info.find((info) => info.role == Role.AGENT);
      if (
        isAgent &&
        ticket.status == 'ASSIGNED' &&
        ticket.assignee == user.user_id
      ) {
        return this.processTicket(ticketId, user, jwt);
      }
    }

    await this.selfAssignChecking(user, ticket);

    const processingTicket = await this.ticketRepository.findOne({
      assignee: +user.user_id,
      creator: +ticket.creator,
      status: 'INPROGRESS',
    });

    if (processingTicket) {
      const msg =
        'Không thể nhận xử lý yêu cầu. Bạn đang xử lý 1 yêu cầu của người dùng này rồi';
      throw new Forbidden(msg);
    }

    ticket.assignee = +user.user_id;
    ticket.assignee_phone = user.phone_number;
    ticket.assigned_at = new Date(Date.now());
    ticket.started_at = new Date(Date.now());
    ticket.status = 'INPROGRESS';
    const response = await this.ticketRepository.save(ticket);

    // NOTE(): sync with chat service
    {
      this.syncUpdateTicketChatSerivce(ticketId, ticket);
    }

    return response;
  }

  makeStaffQueryList(query: TicketQuery, user: any) {
    const queryList = [];

    const extra = user.info;

    let {
      city,
      district,
      ward,
      field,
      number_id,
      status,
      created_at,
      started_at,
      completed_at,
      assignee,
      creator,
      title,
    } = query;

    function appendQuery(tquery: TicketQuery) {
      if (created_at) tquery.created_at = created_at;
      if (started_at) tquery.started_at = started_at;
      if (completed_at) tquery.completed_at = completed_at;
      if (status) tquery.status = status;
      if (title) tquery.title = title;

      if (creator) tquery.creator = creator;
      if (assignee) tquery.assignee = assignee;

      if (number_id) tquery.number_id = number_id;
      if (field) tquery.field = field;
    }

    for (let i = 0; i < extra.length; ++i) {
      const info = extra[i];
      if (info.field) info.field = +info.field;

      switch (+info.role) {
        case Role.ADMIN_CITY:
          {
            if (!city || city == info.city) {
              const q = new TicketQuery();
              appendQuery(q);
              q.city = info.city;
              if (district) q.district = district;
              if (ward) q.ward = ward;
              queryList.push(q);
            }
          }
          break;
        case Role.ADMIN_DISTRICT:
          {
            if (
              (!city || city == info.city) &&
              (!district || district == info.district)
            ) {
              const q = new TicketQuery();
              appendQuery(q);
              q.city = info.city;
              q.district = info.district;
              if (ward) q.ward = ward;
              queryList.push(q);
            }
          }
          break;
        case Role.ADMIN_WARD:
          {
            if (
              (!city || city == info.city) &&
              (!district || district == info.district) &&
              (!ward || ward == info.ward)
            ) {
              const q = new TicketQuery();
              appendQuery(q);
              q.city = info.city;
              q.district = info.district;
              q.ward = info.ward;
              queryList.push(q);
            }
          }
          break;
        case Role.AGENT:
          {
            if (
              (!city || city == info.city) &&
              (!district || district == info.district) &&
              (!ward || ward == info.ward) &&
              (!number_id || info.group_id == number_id) &&
              (!field || info.field == field)
            ) {
              const q = new TicketQuery();
              appendQuery(q);
              q.city = info.city;
              q.district = info.district;
              q.ward = info.ward;
              q.field = info.field;
              q.number_id = info.group_id;
              if (!query.assignee) {
                const tq = { ...q };
                appendQuery(tq);
                tq.assignee = -1;
                queryList.push(tq);
                q.assignee = user.user_id;
              }
              queryList.push(q);
            }
          }
          break;
      }
    }

    for (const u of queryList) {
      for (const v of queryList) {
        if (u != v && !v['ignore'] && !u['ignore']) {
          const ignoreCity = v.city == u.city;
          const ignoreDistrict = !u.district || v.district == u.district;
          const ignoreWard = !u.ward || v.ward == u.ward;
          const ignoreNumber = !u.number_id || v.number_id == u.number_id;
          const ignoreField = !u.field || v.field == u.field;
          const ignoreAssigne = !u.assignee || v.assignee == u.assignee;
          const ignoreCreator = !u.creator || v.creator == u.creator;
          const ignoreStatus = !u.status || v.status == u.status;

          if (
            ignoreCity &&
            ignoreDistrict &&
            ignoreWard &&
            ignoreNumber &&
            ignoreField &&
            ignoreAssigne &&
            ignoreCreator &&
            ignoreStatus
          ) {
            v['ignore'] = true;
          }
        }
      }
    }

    const result = [];

    for (const u of queryList) {
      if (!u['ignore']) {
        for (const key in query) {
          u[key] = query[key];
        }

        for (const key in u) {
          if (u[key] == undefined) {
            delete u[key];
          }
        }

        result.push(u);
      }
    }

    return result;
  }

  // NOTE(): dd/mm/yyyy
  makeTicketListQueryDateFormat(date: string, endOfDay = false) {
    if (date) {
      const parts = date.split('-');

      let dateObj = undefined;
      if (endOfDay) {
        dateObj = new Date(+parts[2], +parts[1] - 1, +parts[0], 23, 59, 59);
      } else {
        dateObj = new Date(+parts[2], +parts[1] - 1, +parts[0]);
      }
      return dateObj;
    }
  }

  makeTicketQuery(query: TicketQueryDto) {
    const ticketQuery = new TicketQuery();

    ticketQuery.city = query.city;
    ticketQuery.district = query.district;
    ticketQuery.ward = query.ward;
    ticketQuery.number_id = query.biz;
    ticketQuery.status = query.status;
    if (query.search) ticketQuery.title = Like(`%${query.search}%`);

    if (query.type) ticketQuery.field = +query.type;
    if (query.assignee) ticketQuery.assignee = +query.assignee;
    if (query.creator) ticketQuery.creator = +query.creator;

    if (query.createdFrom && query.createdTo) {
      ticketQuery.created_at = Between(
        this.makeTicketListQueryDateFormat(query.createdFrom),
        this.makeTicketListQueryDateFormat(query.createdTo, true),
      );
    } else if (query.createdFrom) {
      ticketQuery.created_at = MoreThan(
        this.makeTicketListQueryDateFormat(query.createdFrom),
      );
    } else if (query.createdTo) {
      ticketQuery.created_at = LessThan(
        this.makeTicketListQueryDateFormat(query.createdTo, true),
      );
    }
    if (query.startedFrom && query.startedTo) {
      ticketQuery.started_at = Between(
        this.makeTicketListQueryDateFormat(query.startedFrom),
        this.makeTicketListQueryDateFormat(query.startedTo, true),
      );
    } else if (query.startedFrom) {
      ticketQuery.started_at = MoreThan(
        this.makeTicketListQueryDateFormat(query.startedFrom),
      );
    } else if (query.startedTo) {
      ticketQuery.started_at = LessThan(
        this.makeTicketListQueryDateFormat(query.startedTo, true),
      );
    }
    if (query.completedFrom && query.completedTo) {
      ticketQuery.completed_at = Between(
        this.makeTicketListQueryDateFormat(query.completedFrom),
        this.makeTicketListQueryDateFormat(query.completedTo, true),
      );
    } else if (query.completedFrom) {
      ticketQuery.completed_at = MoreThan(
        this.makeTicketListQueryDateFormat(query.completedFrom),
      );
    } else if (query.completedTo) {
      ticketQuery.completed_at = LessThan(
        this.makeTicketListQueryDateFormat(query.completedTo, true),
      );
    }

    for (const key in ticketQuery) {
      if (ticketQuery[key] == undefined) {
        try {
          delete ticketQuery[key];
        } catch (error) {}
      }
    }

    return ticketQuery;
  }

  async makeTicketListQuery(query: TicketQueryDto, user: any) {
    const ticketQuery = this.makeTicketQuery(query);

    const isAdmin = !!user.info.find((info) => info.role == Role.ADMIN);

    if (isAdmin) {
      for (const key in ticketQuery) {
        if (ticketQuery[key] == undefined) {
          try {
            delete ticketQuery[key];
          } catch (error) {}
        }
      }
      return [ticketQuery];
    } else {
      return this.makeStaffQueryList(ticketQuery, user);
    }
  }

  async serializeTicketList(records: Ticket[], user, jwt) {
    let data = [];

    if (records && records.length > 0) {
      data = new Array(records.length);

      const bizSet = new Set<string>();
      const typeSet = new Set<string>();
      const userPhoneNumberSet = new Set<string>();
      const assigneeSet = new Set<number>();

      records.forEach((element) => {
        bizSet.add(element.number_id);
        typeSet.add(String(element.field));
        userPhoneNumberSet.add(element.user_phone);
        const assignnee = element.assignee;
        if (assignnee && assignnee >= 0) {
          assigneeSet.add(assignnee);
        }
      });
      const groupMap = await this.utilService.getGroupsByNumberIds([...bizSet]);
      const typeMap = await this.utilService.getTypes(typeSet);

      const userInfoMap = {};
      {
        const userPhoneNumberArray = [...userPhoneNumberSet];
        const userInfoArray = await Promise.all(
          userPhoneNumberArray.map((phone) => {
            return this.utilService.getUserInfoByPhone(user, phone, jwt);
          }),
        );
        userInfoArray.forEach((userInfo) => {
          try {
            userInfoMap[userInfo.user_id] = userInfo;
          } catch (error) {
            console.log('CATCHED ERROR: ', error);
          }
        });
      }
      const assigneeMap = {};
      {
        const assignees = [...assigneeSet];
        const staffsName = await Promise.all(
          assignees.map((user_id) => {
            return this.utilService.getStaffNameByUserId(user_id);
          }),
        );
        staffsName.forEach((staff) => {
          if (staff) assigneeMap[staff.user_id] = staff.name;
        });
      }

      records.forEach(async (element, index) => {
        data[index] = {};

        data[index]['id'] = element.id;
        data[index]['title'] = element.title;
        data[index]['description'] = element.title;
        data[index]['status'] = element.status;

        data[index]['creator'] = String(element.creator);
        data[index]['userInfo'] = userInfoMap[element.creator];
        data[index]['assignee'] = String(element.assignee);
        data[index]['assigneeName'] = assigneeMap[element.assignee];

        data[index]['createdAt'] = element.created_at;
        data[index]['assignedAt'] = element.assigned_at;
        data[index]['startedAt'] = element.started_at;
        data[index]['completedAt'] = element.completed_at;

        data[index]['city'] = element.city;
        data[index]['district'] = element.district;
        data[index]['ward'] = element.ward;
        data[index]['biz'] = element.number_id;
        data[index]['bizName'] = groupMap[element.number_id];
        data[index]['type'] = String(element.field);
        data[index]['typeName'] = typeMap[+element.field];

        try {
          delete data[index]['userInfo']['user_id'];
          delete data[index]['userInfo']['email'];
        } catch (error) {}
      });
    }
    return data;
  }

  async getTicketList(query: TicketQueryDto, user: any, jwt: string) {
    const queryList = await this.makeTicketListQuery(query, user);

    if (queryList && queryList.length > 0) {
      let take = query['limit'] || 10;
      if (take > 100) take = 100;
      const page = query.page || 0;
      const skip = page * take;

      const [records, total] = await this.ticketRepository.findAndCount({
        where: queryList,
        order: {
          created_at: 'DESC',
        },
        take,
        skip,
      });

      const data = await this.serializeTicketList(records, user, jwt);
      return {
        listTotal: total,
        list: data,
      };
    }
    return {
      listTotal: 0,
      list: [],
    };
  }

  async getMyTicketList(query: MyTicketQueryDto, user: any, jwt: string) {
    let take = query['limit'] || 10;
    if (take > 100) take = 100;
    const page = query.page || 0;
    const skip = page * take;

    const ticketQuery = this.makeTicketQuery({
      ...query,
      creator: user.user_id,
    });

    const [records, total] = await this.ticketRepository.findAndCount({
      where: ticketQuery,
      order: {
        created_at: 'DESC',
      },
      take,
      skip,
    });

    const data = await this.serializeTicketList(records, user, jwt);
    return {
      listTotal: total,
      list: data,
    };
  }

  async deleteTicket(id: string, user) {
    try {
      const oldData = await this.getTicket(id);
      this.utilService.locationPermissionChecking(
        user,
        {
          city: oldData.city,
          district: oldData.district,
          ward: oldData.ward,
        },
        true,
      );
      const result = await this.ticketRepository.delete(id);
      // NOTE(): sync with chat service
      {
        this.chatProxy.deleteGeneralTicket(id);
      }
      this.deleteImages(oldData.images as Array<string>);
      return result;
    } catch (error) {
      throw new BadRequest(error);
    }
  }

  async processTicket(ticketId: string, user: any, jwt) {
    const ticket = await this.ticketRepository.findOne(ticketId);

    if (ticket.assignee != user.user_id || ticket.status != 'ASSIGNED') {
      const msg = 'You can not process this ticket!';
      throw new Forbidden(msg);
    }

    const processingTicket = await this.ticketRepository.findOne({
      assignee: +user.user_id,
      creator: +ticket.creator,
      status: 'INPROGRESS',
    });

    if (processingTicket) {
      const msg =
        'Không thể nhận xử lý yêu cầu. Bạn đang xử lý 1 yêu cầu của người dùng này rồi';
      throw new Forbidden(msg);
    }

    ticket.started_at = new Date(Date.now());
    ticket.status = 'INPROGRESS';
    const result = await this.ticketRepository.save(ticket);

    // NOTE(): sync with chat service
    {
      this.syncUpdateTicketChatSerivce(ticketId, ticket);
    }

    return this.getTicketWithUserInfo(result.id, user, jwt);
  }

  async replyTicket(
    images: Array<Express.Multer.File>,
    ticketId: string,
    user: any,
    data: AnswerTicketDto,
    jwt: string,
  ) {
    const ticket = await this.ticketRepository.findOne(ticketId);

    if (ticket.assignee != user.user_id || ticket.status != 'INPROGRESS') {
      const msg = 'You can not answer this ticket!';
      throw new Forbidden(msg);
    }

    let imageUrls = [];
    if (data.hasImages == 'true') {
      imageUrls = await this.uploadImages(user, images);
      ticket['extra'][`answer_images`] = imageUrls.toString();
    }
    ticket['extra']['answer'] = data.answer;
    await this.ticketRepository.save(ticket);
    // NOTE(): sync with chat service
    {
      this.syncUpdateTicketChatSerivce(ticketId, ticket);
    }
    const imagesMessages = imageUrls.map((url) => {
      return {
        photoUrl: `${process.env.API_URL}/${url}`,
      };
    });
    const messages = [
      {
        content: data.answer,
      },
      {
        link: `${process.env.WEB_URL}/ticket-public/${ticket.id}`,
      },
      ...imagesMessages,
    ];
    this.chatProxy.sendMessage(
      String(ticket.creator),
      ticket.number_id,
      messages,
    );

    return this.getTicketWithUserInfo(ticket.id, user, jwt);
  }

  async replyVaccineTicket(
    ticketId: string,
    user: any,
    data: VaccineReplyTicketDto,
    jwt: string,
  ) {
    const ticket = await this.ticketRepository.findOne(ticketId);

    if (ticket.assignee != user.user_id || ticket.status != 'INPROGRESS') {
      const msg = 'You can not answer this ticket!';
      throw new Forbidden(msg);
    }

    ticket['extra']['answer'] = data.answer;
    ticket['extra']['accepted'] = data.accept ? '1' : '0';
    await this.ticketRepository.save(ticket);

    // NOTE(): sync with chat service
    {
      this.syncUpdateTicketChatSerivce(ticketId, ticket);
    }

    const messages = [
      {
        content: data.answer,
      },
      {
        link: `${process.env.WEB_URL}/ticket-public/${ticket.id}`,
      },
    ];

    this.chatProxy.sendMessage(
      String(ticket.creator),
      ticket.number_id,
      messages,
    );

    return this.getTicketWithUserInfo(ticketId, user, jwt);
  }

  async createVaccineTicket(data: VaccineTicketDto, user: any, jwt: string) {
    const field = await this.utilService.getSpecialField(
      process.env.SPECIAL_FIELD_VACCINE,
    );

    data['creator'] = user.user_id;
    data['assignee'] = '-1';
    data['type'] = String(field.id);
    data['numberId'] = String(field.numberId);

    const locationIsActive = await this.activeLocationService.checkActiveCity(
      data.vaccineRegistraion.city,
    );

    if (!locationIsActive) {
      throw new BadRequest(
        'Tỉnh/ Thành phố chưa sử dụng dịch vụ của myLocal. Xin vui lòng chọn Tỉnh, Thành phố khác.',
      );
    }

    await this.utilService.valueCheckingVaccineTicket(data);

    let id = undefined;
    // NOTE(): sync with chat service
    {
      const requestData = serializer.ApiVaccineTicketSerialize(data);
      requestData['extra']['userPhone'] = user.phone_number;
      id = (await this.chatProxy.createGeneralTicket(requestData))?.data?.id;
    }

    if (id) {
      const insertData = serializer.InsertDbVaccineTicketSerialize(data);
      insertData.id = id;
      insertData.user_phone = user.phone_number;
      const result = await this.ticketRepository.save(insertData);

      const ticket = await this.getTicketWithUserInfo(result.id, user, jwt);

      {
        const messages = [
          {
            content: utils.CreateTicketMessage(ticket.bizName),
          },
          {
            link: `${process.env.WEB_URL}/ticket-public/${ticket.id}`,
          },
        ];
        this.chatProxy.sendMessage(
          String(ticket.creator),
          ticket.numberId,
          messages,
        );
      }

      return ticket;
    }
  }

  async getTicketWithUserInfo(id: string, user: any, jwt: string) {
    const record = await this.ticketRepository.findOne({ id: id });
    const extraData = await this.getTicketExtraInfo(record, user, jwt);

    let ticket = undefined;
    if (extraData) {
      switch (extraData['typeSpecial']) {
        case process.env.SPECIAL_FIELD_VACCINE:
          {
            ticket = serializer.DbVaccineTicketSerialize(record);
          }
          break;
        default:
          {
            ticket = serializer.DbTicketSerialize(record);
          }
          break;
      }
      ticket = { ...ticket, ...extraData };
    }
    return ticket;
  }

  async getPublicTicketWithUserInfo(id: string) {
    const record = await this.ticketRepository.findOne(id);
    const extraData = await this.getPublicTicketExtraInfo(record);

    let ticket = undefined;
    if (extraData) {
      switch (extraData['typeSpecial']) {
        case process.env.SPECIAL_FIELD_VACCINE:
          {
            ticket = serializer.DbVaccineTicketSerialize(record);
          }
          break;
        default: {
          ticket = serializer.DbTicketSerialize(record);
        }
      }
      ticket = { ...ticket, ...extraData };
    }
    delete ticket['userPhone'];
    return ticket;
  }

  async getTicketExtraInfo(ticket: Ticket, user: any, jwt: string) {
    const nullAsync = async () => null;

    const [creatorInfo, assigneeInfo, field, group] = await Promise.all([
      this.utilService.getUserInfoByPhone(user, ticket.user_phone, jwt),
      ticket.assignee !== -1
        ? this.utilService.getUserInfoByPhone(user, ticket.assignee_phone, jwt)
        : nullAsync,
      this.utilService.getType(String(ticket.field)),
      this.utilService.getGroupByNumberId(ticket.number_id),
    ]);

    const result = {};

    result['userInfo'] = creatorInfo;
    result['assigneeInfo'] = assigneeInfo;
    if (field) {
      result['typeName'] = field.name;
      result['typeSpecial'] = field.special;
    }
    if (group) result['bizName'] = group.group_name;
    return result;
  }

  async getPublicTicketExtraInfo(ticket: Ticket) {
    const nullAsync = async () => null;

    const [creatorInfo, assigneeInfo, field, group] = await Promise.all([
      this.utilService.getUserInfoByPhoneV2(ticket.user_phone),
      ticket.assignee !== -1
        ? this.utilService.getUserInfoByPhoneV2(ticket.assignee_phone)
        : nullAsync,
      this.utilService.getType(String(ticket.field)),
      this.utilService.getGroupByNumberId(ticket.number_id),
    ]);

    const result = {};

    result['userInfo'] = creatorInfo;
    result['assigneeInfo'] = assigneeInfo;
    if (field) {
      result['typeName'] = field.name;
      result['typeSpecial'] = field.special;
    }

    if (group) result['bizName'] = group.group_name;
    try {
      delete result['userInfo']['phone_number'];
      delete result['userInfo']['user_id'];
    } catch (error) {}
    try {
      delete result['assigneeInfo']['phone_number'];
      delete result['assigneeInfo']['user_id'];
      delete ticket['assigneePhone'];
    } catch (error) {}
    return result;
  }
}
