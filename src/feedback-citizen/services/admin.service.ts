import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

import * as xlsx from 'xlsx';
import * as styleXlsx from 'xlsx-style';
import * as path from 'path';

import { AuthProxy } from '@proxy/auth';
import { BadRequest, InternalServerError } from '@common/errors';
import { Role } from '@auth/enum';

import { AgentService } from './agent.service';
import { FieldConsultationService } from './field-consultation.service';
import { BaSettingService } from './ba-setting.service';
import { CreatePartnerUserDto, CreateAgentDto } from '../dto';
import { ImportAgent } from '../entities/import-agent.entity';
import { UtilService } from '@util/services';

@Injectable()
export class AdminService {
  private workSheetColumnNames = [
    'Tên Business Account',
    'Lĩnh vực',
    'Tỉnh / Thành phố',
    'Quận',
    'Phường',
    'Tên nhân viên',
    'Email',
    'Điện thoại di động',
    'Kết quả xử lý (sau khi gửi email)',
  ];
  private widthColumns = [
    { wch: 20 }, //Tên Business Account
    { wch: 20 }, //Lĩnh vực
    { wch: 20 }, //Tỉnh / Thành phố
    { wch: 20 }, //Quận
    { wch: 20 }, //Phường
    { wch: 20 }, //Tên nhân viên
    { wch: 25 }, //Email
    { wch: 20 }, //Điện thoại di động
    { wch: 40 }, //Kết quả xử lý (sau khi gửi email)
  ];

  private workSheetName = 'KẾT QUẢ';

  constructor(
    @InjectRepository(ImportAgent)
    private importAgentRepository: Repository<ImportAgent>,
    private readonly authProxy: AuthProxy,
    private readonly agentService: AgentService,
    private readonly utilService: UtilService,
    private readonly fieldConsultationService: FieldConsultationService,
    private readonly baSettingService: BaSettingService,
  ) {}

  async createPartnerUser(
    createPartnerUserDto: CreatePartnerUserDto,
    token: string,
    user: any,
  ) {
    const data = createPartnerUserDto.data;

    if (Array.isArray(data) && data.length == 0) {
      throw new BadRequest('Data empty');
    }

    const { result: dataConvert, check_error } = await this.validateData(
      data,
      user,
    );
    if (check_error) {
      const dataValidate = dataConvert.map((dt) => {
        return [
          dt.group_name,
          dt.field_name,
          dt.city_name,
          dt.district_name,
          dt.ward_name,
          dt.name,
          dt.email,
          dt.phone_number,
          dt.result,
        ];
      });
      const nameFile = this.getImportAgentName('OTT_validate-result');
      const path = `import-agent-validate/${nameFile}.xlsx`;
      await this.exportFile(
        dataValidate,
        this.workSheetColumnNames,
        this.workSheetName,
        path,
      );
      return {
        success: false,
        path: path,
      };
    }

    const listPromise: Array<any> = [];

    const importAgentDB = new ImportAgent();
    importAgentDB.name = this.getImportAgentName('OTT_import_result');
    importAgentDB.path = `import-agent-log/${importAgentDB.name}.xlsx`;
    const importAgent = await this.importAgentRepository.save(importAgentDB);

    for (const dt of dataConvert) {
      listPromise.push(this._processSavePartnerUser(dt, token, user));
    }

    let mailError = false;

    if (listPromise.length > 0) {
      try {
        const result = await Promise.all(listPromise);
        await this.exportFile(
          result,
          this.workSheetColumnNames,
          this.workSheetName,
          importAgent.path,
        );
        console.log('EXPORTED FILE:', result);
        mailError = true;
        await this.sendMailNoticeSuccessForAdmin(
          user.email,
          `Quá trình import đã được hoàn thành, hãy truy cập vào link ${process.env.API_URL}/${importAgent.path} để lấy kết quả import`,
          token,
        );
        mailError = false;
      } catch (error) {
        console.log(error);
        if (!mailError) {
          throw new InternalServerError(error);
        }
      }
      return { success: true, path_result_file: importAgentDB.path };
    }
    throw new BadRequest('Data empty');
  }

  async _processSavePartnerUser(dt: any, token: string, user) {
    let userInfo = undefined;
    try {
      userInfo = await this.utilService.getUserInfoByPhone(
        user,
        dt.phone_number,
        token,
      );
    } catch (error) {
      console.log('THIS IS MAY BE NOT AN ERROR: ', error);
    }
    console.log(userInfo);
    if (!userInfo) {
      userInfo = await this.authProxy.createPartnerUser(dt, token);
    }
    const data = [
      dt.group_name,
      dt.field_name,
      dt.city_name,
      dt.district_name,
      dt.ward_name,
      userInfo.name ? userInfo.name : dt.name,
      dt.email,
      dt.phone_number,
    ];

    try {
      if (!userInfo.user_id)
        throw new BadRequest('Can not create user in mylocal');
      console.log('user_id:' + userInfo.user_id);
      const agentDB = {
        name: userInfo.name ? userInfo.name : dt.name,
        user_id: userInfo.user_id,
        phone_number: dt['phone_number'],
        email: dt['email'],
        info: [
          {
            role: Role.AGENT,
            city: dt['city'],
            district: dt['district'],
            ward: dt['ward'],
            field: dt['field'],
            group_id: dt['group_id'],
          },
        ],
      };
      await this.agentService.createAgent(agentDB as CreateAgentDto);
      const infoSendMail = {
        name: dt.name,
        email: dt.email,
        group: dt.group_name,
        field: dt.field_name,
        location: `${dt.city_name} - ${dt.district_name} - ${dt.ward_name}`,
        phone_number: dt.phone_number,
        password: userInfo.password
          ? userInfo.password
          : 'User trước đó đã tồn tại, sử dụng lại password của user',
      };

      const resultSendMail = await this.authProxy.sendMailInfoAccount(
        infoSendMail,
        token,
      );

      if (resultSendMail.statusCode !== 200) {
        data.push('Thất bại');
      } else {
        data.push('Thành công');
      }

      return data;
    } catch (e) {
      console.log(e.toString());
      data.push('Thất bại');
      return data;
    }
  }

  _validatePhoneNumber(value: string) {
    if (typeof value !== 'string') return false;

    if (value) {
      return (
        (value.startsWith('84') && value.length === 11) ||
        (value.startsWith('0') && value.length === 10)
      );
    }
    return true;
  }

  async exportFile(data, workSheetColumnNames, workSheetName, pathFile) {
    const workBook = xlsx.utils.book_new();
    const workSheetData = [workSheetColumnNames, ...data];
    const workSheet = xlsx.utils.aoa_to_sheet(workSheetData);
    const logPath = `./public/${pathFile}`;
    xlsx.utils.book_append_sheet(workBook, workSheet, workSheetName);
    xlsx.writeFile(workBook, path.resolve(logPath));
    this.styleWorkBook(logPath);
  }

  async exportLogCreateAgent() {
    const importLog = await this.importAgentRepository.findOne({
      order: {
        id: 'DESC',
      },
    });
    if (!importLog) {
      return { path: '' };
    }
    return { path: importLog.path };
  }

  styleHeader(workBookStyle: any) {
    const sheetName = this.workSheetName;
    const style = {
      font: {
        sz: 12,
        bold: true,
      },
      fill: {
        fgColor: { rgb: 'FF9900' },
      },
      border: {
        top: { style: 'thin' },
        bottom: { style: 'thin' },
        left: { style: 'thin' },
        right: { style: 'thin' },
      },
    };
    workBookStyle.Sheets[sheetName].A1.s = style;
    workBookStyle.Sheets[sheetName].B1.s = style;
    workBookStyle.Sheets[sheetName].C1.s = style;
    workBookStyle.Sheets[sheetName].D1.s = style;
    workBookStyle.Sheets[sheetName].E1.s = style;
    workBookStyle.Sheets[sheetName].F1.s = style;
    workBookStyle.Sheets[sheetName].G1.s = style;
    workBookStyle.Sheets[sheetName].H1.s = style;
    workBookStyle.Sheets[sheetName].I1.s = style;
  }

  styleWorkBook(logPath: string) {
    const sheetName = this.workSheetName;
    const workBookStyle = styleXlsx.readFile(logPath);
    const col = workBookStyle.Sheets[sheetName]['!ref'];
    const countRows = Number(col.split(':')[1].replace('I', ''));
    const columns = 'ABCDEFGHI'.split('');
    const style = {
      border: {
        top: { style: 'thin' },
        bottom: { style: 'thin' },
        left: { style: 'thin' },
        right: { style: 'thin' },
      },
    };

    for (let i = 1; i <= countRows; i++) {
      for (let j = 0; j < columns.length; j++) {
        if (columns[j] == 'I') {
          const styleCustom = { border: style.border };
          if (
            workBookStyle.Sheets[sheetName][`${columns[j]}${i}`].v !==
              'Thành công' ||
            workBookStyle.Sheets[sheetName][`${columns[j]}${i}`].v == ''
          ) {
            styleCustom['font'] = {
              color: { rgb: 'FF0000' },
            };
          } else {
            styleCustom['font'] = {
              color: { rgb: '0033FF' },
            };
          }
          workBookStyle.Sheets[sheetName][`${columns[j]}${i}`].s = styleCustom;
        } else {
          workBookStyle.Sheets[sheetName][`${columns[j]}${i}`].s = style;
        }
      }
    }

    workBookStyle.Sheets[sheetName]['!cols'] = this.widthColumns;
    this.styleHeader(workBookStyle);

    styleXlsx.writeFile(workBookStyle, path.resolve(logPath));
  }

  getImportAgentName(name) {
    const today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0');
    const yyyy = today.getFullYear();
    const h = today.getHours();
    const m = today.getMinutes();
    const s = today.getSeconds();
    return `${name}_${dd}${mm}${yyyy}_${h}_${m}_${s}`;
  }

  async sendMailNoticeSuccessForAdmin(
    email: string,
    message: string,
    token: string,
  ) {
    await this.agentService.sendMailNoticeSuccessForAdmin(
      email,
      message,
      token,
    );
  }

  async validateData(data: any, user: any) {
    const listBizName = await this.baSettingService.listBiz();
    const listField = await this.fieldConsultationService.listField();
    const lisAgent = await this.agentService.listPhoneNumber();
    let checkError = false;
    const result = data.map((dt) => {
      const checkPhone = this._validatePhoneNumber(dt['phone_number']);
      let phone_validate: any = '';
      let city_validate: any = '';
      let district_validate: any = '';
      let ward_validate: any = '';
      let group_validate: any = '';
      let field_validate: any = '';
      let result = '';

      if (!checkPhone) {
        phone_validate = `Số điện thoại ${dt['phone_number']} không hợp lệ`;
        result += phone_validate + '\n';
        checkError = true;
      }

      const { city_code, district_code, ward_code } =
        this.utilService.mappingNameToCode(
          dt['city'],
          dt['district'],
          dt['ward'],
        );

      if (!city_code) {
        city_validate = 'Tỉnh/thành không chính xác';
        result += city_validate + '\n';
        checkError = true;
      } else {
        if (!district_code) {
          district_validate = 'Quận/huyện không chính xác';
          result += district_validate + '\n';
          checkError = true;
        } else {
          if (!ward_code) {
            ward_validate = 'Phường/xã không chính xác';
            result += ward_validate + '\n';
            checkError = true;
          }
        }
      }

      const authorized = this.utilService.permissionPowerChecking(
        user,
        {
          role: Role.AGENT,
          city: city_code,
          district: district_code,
          ward: ward_code,
        },
        // false,
      );

      if (!authorized) {
        result += `. User ${user.name} không có quyền để tạo user này\n`;
        checkError = true;
      }

      const biz = listBizName[dt['group_id']];

      let field: any = null;

      if (!biz) {
        group_validate = 'Tên Business Account không chính xác';
        result += group_validate + '\n';
        checkError = true;
      } else {
        field = listField.find(
          (f) => f.name == dt['field'] && f.group_id == biz.group_id,
        );

        if (!field) {
          field_validate = 'Tên lĩnh vực không chính xác';
          result += field_validate + '\n';
          checkError = true;
        }
      }

      if (
        biz &&
        field &&
        lisAgent.includes(
          `${dt['phone_number']}${biz.number_id}${field.id}${city_code}${district_code}${ward_code}`,
        )
      ) {
        phone_validate = `Số điện thoại ${dt['phone_number']} đã tồn tại`;
        result += phone_validate + '\n';
        checkError = true;
      }

      const phone = String(dt['phone_number']);
      const phoneUpdate = phone.startsWith('0') ? '84' + phone.slice(1) : phone;
      return {
        name: dt['name'],
        phone_number: phoneUpdate,
        email: dt['email'],
        city: city_code,
        city_name: dt['city'],
        district: district_code,
        district_name: dt['district'],
        ward: ward_code,
        ward_name: dt['ward'],
        group_id: biz ? biz.number_id : '',
        group_name: dt['group_id'],
        field: field ? field.id : '',
        field_name: dt['field'],
        result,
      };
    });
    return {
      check_error: checkError,
      result,
    };
  }
}
