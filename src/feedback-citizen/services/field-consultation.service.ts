import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BASetting, FieldConsultation } from '@feedback-citizen/entities';
import { Repository, IsNull, Not } from 'typeorm';

import { BadRequest } from '@common/errors';

import { CreateFieldConsultationDto, UpdateFieldConsultationDto } from '../dto';

@Injectable()
export class FieldConsultationService {
  constructor(
    @InjectRepository(BASetting)
    private baSettingRepository: Repository<BASetting>,
    @InjectRepository(FieldConsultation)
    private fieldConsultationRepository: Repository<FieldConsultation>,
  ) {}

  async createFields(data: CreateFieldConsultationDto) {
    const ba = await this.baSettingRepository.findOne({
      number_id: data.number_id,
    });
    if (!ba) {
      throw new BadRequest('Business account not exist');
    }
    const listField = data.list_field;
    const arrayDB: Array<any> = [];
    if (listField.length > 0) {
      for (const field of listField) {
        const dbField: Record<string, string> = {};
        dbField.name = field.name;
        dbField.special = field.special;
        dbField.group_id = ba.group_id;
        arrayDB.push(dbField);
      }
    }
    if (arrayDB.length > 0) {
      await this.fieldConsultationRepository.insert(arrayDB);
    }
    return { number_id: data.number_id, success: true };
  }

  async updateField(id: number, data: UpdateFieldConsultationDto) {
    const field = await this.fieldConsultationRepository.findOne({
      id: id,
    });
    field.name = data.name;
    return await this.fieldConsultationRepository.save(field);
  }

  async deleteField(id: number) {
    return await this.fieldConsultationRepository.delete(id);
  }

  async mappingNameToCode(groupName, fieldName) {
    const result = {
      group_id: '',
      field_id: '',
    };
    const ba = await this.baSettingRepository.findOne({
      group_name: groupName,
    });

    if (!ba) {
      return result;
    }
    const field = await this.fieldConsultationRepository.findOne({
      name: fieldName,
      group_id: ba.group_id,
    });

    if (!field) {
      return result;
    }

    return {
      group_id: ba.number_id,
      field_id: field.id,
    };
  }

  async listField() {
    const list = await this.fieldConsultationRepository.find({
      select: ['id', 'name', 'group_id'],
    });
    return list;
  }

  async resolveGroupIds(groupIds: string[]) {
    const query = groupIds.map((group_id) => {
      return { group_id };
    });
    const list = await this.baSettingRepository.find({
      where: query,
      select: ['group_id', 'number_id'],
    });
    const map = {};
    list.forEach((e) => {
      map[e.group_id] = e.number_id;
    });
    return map;
  }

  async resolveGroupId(groupId: string) {
    return (
      await this.baSettingRepository.findOne({
        where: { group_id: groupId },
        select: ['number_id'],
      })
    ).number_id;
  }

  async getSpecialList() {
    const list = await this.fieldConsultationRepository.find({
      where: {
        special: Not(IsNull()),
      },
      select: ['id', 'name', 'special', 'group_id'],
    });
    const groupSet = new Set<string>();
    list.forEach((e) => {
      groupSet.add(e.group_id);
    });
    const numberIdMap = await this.resolveGroupIds([...groupSet]);
    return list.map((e) => {
      return {
        id: e.id,
        name: e.name,
        special: e.special,
        group_id: numberIdMap[e.group_id],
      };
    });
  }

  async getSpecial(special: string) {
    const result = await this.fieldConsultationRepository.findOne({
      where: {
        special: special,
      },
      select: ['id', 'name', 'group_id'],
    });
    result.group_id = await this.resolveGroupId(result.group_id);
    return result;
  }
}
