import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getManager, LessThan } from 'typeorm';
import { Role, LOCK_NUMBER } from '@auth/enum';

import { AuthProxy } from '@proxy/auth';

import { UtilService } from '@util/services';

import { BadRequest, NotFound } from '@common/errors';
import { Config, SaveConfig } from '@common/config';

import { Ticket, ActiveLocation, Staff } from '../entities';
import { UpdateActiveLocationDto, ActiveLocationQuery } from '../dto';

@Injectable()
export class ActiveLocationService {
  private locationList = undefined;

  constructor(
    private readonly authProxy: AuthProxy,
    @InjectRepository(ActiveLocation)
    private activeLocationRepository: Repository<ActiveLocation>,
    @InjectRepository(Staff)
    private staffRepository: Repository<Staff>,
    private readonly utilService: UtilService,
    @InjectRepository(Ticket)
    private ticketRepository: Repository<Ticket>,
  ) {
    this.downloadLocationPatch();
  }

  async downloadLocationPatch() {
    try {
      const locationData = await this.authProxy.getLocation();
      SaveConfig('location', JSON.stringify(locationData));
    } catch (error) {
      console.log('Unable to download location-list');
    }

    this.locationList = Config('location');
  }

  async updateActiveLocation(data: UpdateActiveLocationDto) {
    const query = [
      {
        status: 'TODO',
        city: data.code,
      },
      {
        status: 'INPROGRESS',
        city: data.code,
      },
      {
        status: 'ASSIGNED',
        city: data.code,
      },
    ];

    const ticket = await this.ticketRepository.findOne({
      where: query,
      select: ['id'],
    });

    if (ticket) {
      throw new BadRequest(
        'Tỉnh/ Thành phố đang có yêu cầu hỗ trợ chưa xử lý xong, vui lòng xử lý hết yêu cầu hỗ trợ trước khi hủy kết nối',
      );
    }

    const activeLocation = await this.activeLocationRepository.findOne({
      code: data.code,
    });

    if (!activeLocation) {
      throw new NotFound('Could not find active location with this code!');
    }

    await getManager().transaction(async (entityManager) => {
      try {
        activeLocation.active = data.active;
        await entityManager.save(activeLocation);

        if (!data.active) {
          const staffs = await this.staffRepository.find({
            where: {
              city: data.code,
              role: LessThan(LOCK_NUMBER),
            },
          });
          if (staffs && staffs.length > 0) {
            for (const staff of staffs) {
              if (staff.role != Role.ADMIN) staff.role += LOCK_NUMBER;
            }
            await entityManager.save(staffs);
          }
        }
      } catch (error) {
        console.log('ERROR:', error);
      }
    });

    return true;
  }

  async getActiveCities(query: ActiveLocationQuery) {
    const dbQuery = {};
    if (String(query.active) == 'true') dbQuery['active'] = 1;
    if (String(query.active) == 'false') dbQuery['active'] = 0;

    const result = await this.activeLocationRepository.find(dbQuery);
    return result;
  }

  async getActiveLocationMap() {
    const lMap = await this.utilService.getLocationMap();
    if (lMap) {
      const cities = await this.activeLocationRepository.find({
        where: {
          active: true,
        },
        select: ['code'],
      });
      if (cities) {
        const result = {};
        for (const city of cities) {
          result[city.code] = lMap[city.code];
        }
        return result;
      }
    }
    return {};
  }

  async getActiveLocationList() {
    const list = this.locationList;
    if (list) {
      const cities = await this.activeLocationRepository.find({
        where: {
          active: true,
        },
        select: ['code'],
      });
      if (cities) {
        const result = [];
        const citySet = new Set<string>();

        for (const city of cities) {
          citySet.add(city.code);
        }

        for (const city of list) {
          if (citySet.has(city.code)) {
            result.push(city);
          }
        }
        return result;
      }
    }
    return [];
  }

  async checkActiveCity(code: string) {
    const city = await this.activeLocationRepository.findOne({
      code,
      active: true,
    });
    return !!city;
  }
}
