export * from './role.enum';

export class TicketQuery {
  city?: string;
  district?: string;
  ward?: string;
  number_id?: string;
  field?: number;
  status?: string;
  assignee?: number;
  creator?: number;
  created_at?: any;
  started_at?: any;
  completed_at?: any;
  title?: any;
}
