import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'ott_ticket_ticket' })
export class Ticket extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  title: string;

  @Column()
  city: string;

  @Column()
  district: string;

  @Column()
  ward: string;

  @Column()
  field: number;

  @Column()
  status: string;

  @Column()
  number_id: string;

  @Column()
  assignee: number;

  @Column()
  creator: number;

  @Column()
  assignee_phone: string;

  @Column()
  user_phone: string;

  @Column({ type: 'json' })
  extra: Record<string, string>;

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  created_at: Date;

  @Column()
  assigned_at: Date;

  @Column()
  started_at: Date;

  @Column()
  completed_at: Date;
}
