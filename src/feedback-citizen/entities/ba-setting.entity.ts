import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'ott_ticket_ba_setting' })
export class BASetting extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  number: string;

  @Column()
  number_id: string;

  @Column()
  queue_id: string;

  @Column()
  routing_id: string;

  @Column()
  group_id: string;

  @Column()
  group_name: string;
}
