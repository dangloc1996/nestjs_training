import {
  BaseEntity,
  Column,
  UpdateDateColumn,
  Entity,
  PrimaryColumn,
} from 'typeorm';

@Entity({ name: 'ott_ticket_active_location' })
export class ActiveLocation extends BaseEntity {
  @PrimaryColumn()
  code: string;

  @Column({ default: false })
  active: boolean;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updated_at: Date;
}
