import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Role } from '@auth/enum';

@Entity({
  name: 'ott_ticket_staff',
  orderBy: {
    created_at: 'DESC',
  },
})
export class Staff extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  user_id: string;

  @Column()
  agent_id: string;

  @Column({ default: Role.AGENT })
  role: number;

  @Column()
  email: string;

  @Column()
  phone_number: string;

  @Column()
  city: string;

  @Column()
  district: string;

  @Column()
  ward: string;

  @Column()
  field: string;

  @Column()
  group_id: string;

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  created_at: Date;
}
