import { Controller, Get, Param, Query } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';

import { Permission } from '@auth/decorators';
import { Permission as P } from '@auth/enum';
import { CommunicationService } from '../services';
import { CallQueryDTO } from '../dto';
import { CallEventTransformer } from '../transformers';

@ApiTags('Communication')
@Controller('communication')
export class CommunicationController {
  constructor(private readonly communicationService: CommunicationService) {}

  @Permission(P.ADMIN | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD)
  @ApiOkResponse({ type: CallEventTransformer, isArray: true })
  @Get('call-history/:id')
  getCallHistory(@Param('id') numberId: string) {
    return this.communicationService.getCallHistory(numberId);
  }

  @Permission(P.ADMIN | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD)
  @ApiOkResponse({ type: CallEventTransformer, isArray: true })
  @Get('call-history-v2/:id')
  getCallHistoryV2(
    @Param('id') numberId: string,
    @Query() query: CallQueryDTO,
  ) {
    return this.communicationService.getCallHistoryV2(numberId, query);
  }
}
