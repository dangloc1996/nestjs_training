import {
  Body,
  Controller,
  Get,
  Headers,
  Post,
  Res,
  Param,
} from '@nestjs/common';
import { join } from 'path';
import { AdminService } from '@feedback-citizen/services/admin.service';
import { ApiTags } from '@nestjs/swagger';
import { CreatePartnerUserDto } from '@feedback-citizen/dto/create-partner-user.dto';
import { Permission, User } from '@auth/decorators';
import { Permission as P } from '@auth/enum';

@ApiTags('Admin')
@Controller('admin')
export class AdminController {
  constructor(private readonly adminService: AdminService) {}

  @Permission(P.ADMIN | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD)
  @Post('partner/create-user')
  async createPartnerUser(
    @Body() createPartnerUserDto: CreatePartnerUserDto,
    @Headers() header,
    @User() user,
  ) {
    const token = header['authorization'];

    return await this.adminService.createPartnerUser(
      createPartnerUserDto,
      token,
      user,
    );
  }

  @Permission(P.ADMIN | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD)
  @Get('partner/download-result/:file')
  async downloadResult(@Res() res, @Param('file') file: string) {
    return res.download(
      join(process.cwd(), `public/import-agent-validate/${file}`),
    );
  }

  @Permission(P.ADMIN | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD)
  @Get('partner/template-import-agent')
  async exportLog(@Res() res) {
    return res.download(
      join(process.cwd(), 'public/import-template/Template-1022.xlsx'),
    );
  }
}
