import {
  Controller,
  Post,
  Put,
  Res,
  Get,
  Body,
  Delete,
  Param,
  Headers,
  Query,
  UploadedFiles,
  UseInterceptors,
} from '@nestjs/common';
import { FilesInterceptor } from '@nestjs/platform-express';
import {
  ApiOkResponse,
  ApiTags,
  ApiConsumes,
  ApiQuery,
  ApiParam,
} from '@nestjs/swagger';
import { Express } from 'express';

import { User } from '@auth/decorators';
import {
  ClientTicketDto as TicketDto,
  TicketQueryDto,
  UpdateTicketDto,
  VaccineTicketDto,
  AnswerTicketDto,
  VaccineReplyTicketDto,
  CloseTicketDto,
  MultipleAssignDto,
  MyTicketQueryDto,
} from '../dto';
import { TicketService } from '../services';
import { TicketIdTransformer } from '../transformers';

import { Permission } from '@auth/decorators';
import { Permission as P } from '@auth/enum';

@ApiTags('Ticket')
@Controller('ticket')
export class TicketController {
  constructor(private readonly ticketService: TicketService) {}

  @Permission(P.ALL)
  @ApiOkResponse({ type: TicketDto })
  @ApiConsumes('multipart/form-data')
  @Post('create-ticket')
  @UseInterceptors(FilesInterceptor('images'))
  async createTicket(
    @UploadedFiles() images: Array<Express.Multer.File>,
    @Body() data: TicketDto,
    @User() user,
    @Headers() headers,
  ) {
    const jwt = headers.authorization;
    return this.ticketService.createTicket(images, data, user, jwt);
  }

  @Permission(P.ALL)
  @ApiOkResponse({ type: VaccineTicketDto })
  @Post('create-vaccine-ticket')
  async createVaccineTicket(
    @Body() data: VaccineTicketDto,
    @User() user,
    @Headers() headers,
  ) {
    const jwt = headers.authorization;
    return this.ticketService.createVaccineTicket(data, user, jwt);
  }

  @Permission(P.USER | P.ADMIN)
  @ApiOkResponse({ type: TicketIdTransformer })
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FilesInterceptor('images'))
  @Put('update-ticket/:id')
  async updateTicket(
    @UploadedFiles() images: Array<Express.Multer.File>,
    @Param('id') id: string,
    @Body() data: UpdateTicketDto,
    @User() user,
  ) {
    return this.ticketService.updateTicket(images, id, data, user);
  }

  @Permission(P.AGENT)
  @ApiOkResponse({ type: TicketDto })
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FilesInterceptor('images'))
  @Put('answer-ticket/:id')
  async answerTicket(
    @UploadedFiles() images: Array<Express.Multer.File>,
    @Param('id') id: string,
    @User() user,
    @Body() data: AnswerTicketDto,
    @Headers() headers,
  ) {
    const jwt = headers.authorization;
    return this.ticketService.replyTicket(images, id, user, data, jwt);
  }

  @Permission(P.AGENT)
  @ApiOkResponse({ type: TicketDto })
  @Put('answer-vaccine-ticket/:id')
  async answerVaccineTicket(
    @Param('id') id: string,
    @User() user,
    @Body() data: VaccineReplyTicketDto,
    @Headers() headers,
  ) {
    const jwt = headers.authorization;
    return this.ticketService.replyVaccineTicket(id, user, data, jwt);
  }

  @Permission(
    P.AGENT | P.ADMIN | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD,
  )
  @ApiOkResponse({ type: TicketIdTransformer })
  @Put('close-ticket/:id')
  @ApiParam({ name: 'id' })
  async closeTicket(
    @Param('id') id,
    @User() user,
    @Body() data: CloseTicketDto,
  ) {
    return this.ticketService.closeTicket(id, user, data);
  }

  @ApiQuery({ name: 'ticketId' })
  @ApiQuery({ name: 'agentId' })
  @Permission(P.ADMIN | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD)
  @ApiOkResponse({ type: TicketIdTransformer })
  @Put('assign-ticket/')
  assignTicket(
    @Query('ticketId') ticketId: string,
    @Query('agentId') agentId: string,
    @User() user: any,
  ) {
    return this.ticketService.assignTicket(ticketId, agentId, user);
  }

  @Permission(P.ADMIN | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD)
  // @ApiOkResponse({ type: TicketIdTransformer })
  @Put('assign-multi-ticket/')
  assignMultiTicket(@Body() data: MultipleAssignDto, @User() user: any) {
    return this.ticketService.assignMultiTicket(data, user);
  }

  @Permission(P.ADMIN | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD)
  // @ApiOkResponse({ type: TicketIdTransformer })
  @Put('assign-multi-ticket-assignee-list/')
  assignMultiTicketAssignableList(
    @Body() data: MultipleAssignDto,
    @User() user: any,
  ) {
    return this.ticketService.assignMultiTicketAssignableList(data, user);
  }

  @Permission(
    P.AGENT | P.ADMIN | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD,
  )
  @ApiOkResponse({ type: TicketIdTransformer })
  @Put('self-assign-ticket/:id')
  @ApiParam({ name: 'id' })
  selfAssignTicket(@Param('id') id, @User() user, @Headers() headers) {
    const jwt = headers.authorization;
    return this.ticketService.selfAssignTicket(id, user, jwt);
  }

  @Permission(P.ALL)
  @ApiOkResponse({ type: TicketDto })
  @Get('get-ticket/:id')
  getTicket(@Param('id') id: string, @User() user, @Headers() headers) {
    const jwt = headers.authorization;
    return this.ticketService.getTicketWithUserInfo(id, user, jwt);
  }

  @Permission(
    P.AGENT | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD | P.ADMIN,
  )
  @Put('process-ticket/:id')
  processTicket(@Param('id') id: string, @User() user, @Headers() headers) {
    const jwt = headers.authorization;
    return this.ticketService.processTicket(id, user, jwt);
  }

  @ApiOkResponse({ type: TicketDto })
  @Get('get-public-ticket/:id')
  async getPublicTicket(@Param('id') id: string) {
    return this.ticketService.getPublicTicketWithUserInfo(id);
  }

  @Permission(
    P.AGENT | P.ADMIN | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD,
  )
  @ApiOkResponse({ type: TicketDto, isArray: true })
  @Get('get-ticket-list')
  async getTicketList(
    @Query() query: TicketQueryDto,
    @User() user,
    @Headers() headers,
  ) {
    const jwt = headers.authorization;
    return this.ticketService.getTicketList(query, user, jwt);
  }

  @Permission(P.ALL)
  @ApiOkResponse({ type: TicketDto, isArray: true })
  @Get('get-my-ticket-list')
  async getMyTicketList(
    @Query() query: MyTicketQueryDto,
    @User() user,
    @Headers() headers,
  ) {
    const jwt = headers.authorization;
    return this.ticketService.getMyTicketList(query, user, jwt);
  }

  @Get('get-image')
  async downloadImage(@Query('key') key: string, @Res() response) {
    const result = await this.ticketService.downloadImage(key);
    return result.pipe(response);
  }

  @Permission(P.ADMIN)
  @ApiOkResponse({ type: TicketIdTransformer })
  @Delete('delete-ticket/:id')
  async deleteTicket(@Param('id') id: string, @User() user) {
    return this.ticketService.deleteTicket(id, user);
  }
}
