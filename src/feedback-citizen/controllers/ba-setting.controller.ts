import { Body, Controller, Post } from '@nestjs/common';
import { BaSettingService } from '@feedback-citizen/services/ba-setting.service';
import { ApiTags } from '@nestjs/swagger';
import { CreateSettingBaDto } from '@feedback-citizen/dto';

@ApiTags('Setting')
@Controller('ba-setting')
export class BaSettingController {
  constructor(private readonly baSettingService: BaSettingService) {}
  @Post('init')
  async initSettingBA() {
    return await this.baSettingService.initSettingBa();
  }

  @Post('create')
  async createBa(@Body() ba: CreateSettingBaDto) {
    return await this.baSettingService.createSettingBa(ba);
  }
}
