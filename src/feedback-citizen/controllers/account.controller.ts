import { Controller, Put, Body, Headers } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import { Auth, User } from '@auth/decorators';
import { AccountService } from '../services';
import { ChangePasswordDto } from '../dto';

@ApiTags('Account Utils')
@Controller('account')
export class AccountController {
  constructor(private readonly accountService: AccountService) {}

  @Auth()
  @Put('change-password')
  async changePassword(
    @Body() data: ChangePasswordDto,
    @User() user,
    @Headers('authorization') jwt: string,
  ) {
    return this.accountService.changePassword(user, data, jwt);
  }
}
