import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Headers,
  Query,
  Put,
} from '@nestjs/common';
import { ApiOkResponse, ApiTags, ApiQuery } from '@nestjs/swagger';

import { AgentService } from '../services';

import {
  CreateAgentDto,
  GetTokenDto,
  UpdateAgentDto,
  ListAgentQueryDto,
  GetListFilterDto,
  UpdateRoleDto,
  DeleteAgentQueryDTO,
} from '../dto';

import { Permission, User } from '@auth/decorators';
import { Permission as P } from '@auth/enum';
import {
  AuthTransformer,
  GetInfoUserToPhoneTransformers,
  ListLocationTransformer,
  ListFieldTransformer,
  ListAgentTransformer,
  ListGroupTransformer,
  DetailAgentTransformer,
  GetListFilterTransformer,
  GetRoleTransformer,
} from '../transformers';

@ApiTags('Agent')
@Controller('agent')
export class AgentController {
  constructor(private readonly agentService: AgentService) {}

  @ApiOkResponse({ type: AuthTransformer })
  @Post('auth')
  async getToken(@Body() getTokenDto: GetTokenDto) {
    return await this.agentService.getToken(getTokenDto);
  }

  @ApiOkResponse({ type: ListAgentTransformer })
  @Permission(P.ADMIN | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD)
  @Get('get-list')
  async listAgent(@Query() query: ListAgentQueryDto, @User() user) {
    console.log(query);
    return await this.agentService.listAgent(query, user);
  }

  @Permission(P.ADMIN | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD)
  @Post('create-agent')
  async createAgent(@Body() createAgent: CreateAgentDto, @User() user) {
    return this.agentService.createAgent(createAgent, user);
  }

  @Permission(P.ADMIN | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD)
  @ApiOkResponse({ type: GetInfoUserToPhoneTransformers })
  @Get('find-user-by-phone/:phone')
  async findUserByPhone(@Param('phone') phone: string, @Headers() headers) {
    const accessToken = headers.authorization;
    return this.agentService.findUserByPhone(phone, accessToken);
  }

  @ApiOkResponse({ type: ListLocationTransformer })
  @Get('location/get-list')
  async getLocation() {
    return this.agentService.getLocation();
  }

  @Permission(P.ADMIN | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD)
  @Delete('delete-agent/:id')
  async deleteAgent(@Param('id') id: number, @User() user) {
    return await this.agentService.deleteAgent(id, user);
  }

  @Permission(P.ADMIN | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD)
  @ApiQuery({ name: 'numberId' })
  @Delete('delete-agent-with-userid/:user_id')
  async deleteAgentWithUserId(
    @Query() query: DeleteAgentQueryDTO,
    @Param('user_id') id: number,
    @User() user,
  ) {
    return await this.agentService.deleteAgentWithUserId(id, query, user);
  }

  @ApiOkResponse({ type: ListFieldTransformer, isArray: true })
  @Get('field/get-list/:group_id')
  async listField(@Param('group_id') id: string) {
    return await this.agentService.fieldList(id);
  }

  @Permission(P.ADMIN | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD)
  @Put('update-agent/:user_id')
  async updateAgent(
    @Param('user_id') user_id: number,
    @Body() data: UpdateAgentDto,
    @User() user,
  ) {
    return await this.agentService.updateAgent(user_id, data, user);
  }

  @Permission(
    P.AGENT | P.ADMIN | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD,
  )
  @ApiOkResponse({ type: ListGroupTransformer, isArray: true })
  @Get('group/get-list')
  async getListGroup(@User() user: any) {
    return await this.agentService.getListGroup(user);
  }

  @Permission(P.ALL)
  @Get('get-role')
  @ApiOkResponse({ type: GetRoleTransformer })
  async getRole(@User() user) {
    return await this.agentService.getRole(user);
  }

  @Permission(
    P.AGENT | P.ADMIN | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD,
  )
  @Get('detail-agent/:id')
  @ApiOkResponse({ type: DetailAgentTransformer })
  async detailAgent(@Param('id') id: number, @User() user) {
    return await this.agentService.detailAgent(user, id);
  }

  @Permission(P.ALL)
  @Get('get-list-filter')
  @ApiOkResponse({ type: GetListFilterTransformer })
  async getListFilter(@Query() query: GetListFilterDto) {
    return await this.agentService.getListFilter(query.biz);
  }

  @Permission(P.ADMIN | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD)
  @Put('update-role/:staff_id')
  async updateRole(
    @Body() data: UpdateRoleDto,
    @Param('staff_id') staff_id: number,
    @User() user,
  ) {
    return await this.agentService.updateRole(data.role, staff_id, user);
  }

  @Permission(P.ADMIN | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD)
  @Put('lock-staff/:staff_id')
  async lockUser(@Param('staff_id') id: number, @User() user) {
    return await this.agentService.lockStaff(id, user);
  }

  @Permission(P.ADMIN | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD)
  @Put('unlock-staff/:staff_id')
  async unlockUser(@Param('staff_id') id: number, @User() user) {
    return await this.agentService.unlockStaff(id, user);
  }
}
