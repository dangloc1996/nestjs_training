import { Controller, Get, Param, Headers, Query } from '@nestjs/common';

import { ApiTags, ApiParam, ApiQuery } from '@nestjs/swagger';
import { User } from '@auth/decorators';
import { Permission } from '@auth/decorators';
import { Permission as P } from '@auth/enum';

import { UtilService } from '@util/services';

@ApiTags('Util')
@Controller('util')
export class UtilController {
  constructor(private readonly utilService: UtilService) {}

  @Permission(P.ADMIN | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD)
  @Get('get-user-info-by-phone/:phone')
  @ApiParam({ name: 'phone' })
  async getUserInfoByPhone(
    @Param('phone') phoneNumber,
    @User() user,
    @Headers() headers,
  ) {
    const jwt = headers.authorization;
    return this.utilService.getUserInfoByPhone(phoneNumber, user, jwt);
  }

  @Get('resolve-type-list/')
  @ApiQuery({ name: 'types' })
  async resolveTypes(@Query('types') typeIds: string) {
    const ids = new Set<string>();
    typeIds.split(',').forEach((p) => {
      ids.add(p.trim());
    });
    return this.utilService.getTypes(ids);
  }

  @Permission(P.ADMIN | P.ADMIN_CITY | P.ADMIN_DISTRICT | P.ADMIN_WARD)
  @Get('download-location/')
  async downloadLocationPatch() {
    return this.utilService.downloadLocationPatch();
  }

  @Get('resolve-location/')
  @ApiQuery({ name: 'cityCode' })
  @ApiQuery({ name: 'districtCode' })
  @ApiQuery({ name: 'wardCode' })
  async getLocationName(
    @Query('cityCode') city: string,
    @Query('districtCode') district: string,
    @Query('wardCode') ward: string,
  ) {
    return this.utilService.getLocationName(city, district, ward);
  }

  @Get('location-map/')
  async getLocationMap() {
    return this.utilService.getLocationMap();
  }

  @Get('staff-location/')
  async getStaffsLocation() {
    return this.utilService.getStaffsLocation();
  }

  @Get('staff-cities/')
  async getStaffsCities() {
    return this.utilService.getStaffsCities();
  }

  @Get('vaccine-field/')
  async getVaccineField() {
    return (
      await this.utilService.getSpecialField(process.env.SPECIAL_FIELD_VACCINE)
    ).id;
  }

  @Get('ba-list/')
  async getBaList() {
    return this.utilService.getBaList();
  }

  @Get('master/')
  getMaster() {
    return this.utilService.getMaster();
  }

  @Get('vaccine-map/')
  getVaccineMap() {
    return this.utilService.getVaccineMap();
  }

  @Get('map/:lat,:long')
  getMap(@Param('lat') lat: number, @Param('long') long: number) {
    return this.utilService.getMap(lat, long);
  }
}
