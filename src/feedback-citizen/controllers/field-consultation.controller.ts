import {
  Body,
  Controller,
  Post,
  Put,
  Param,
  Delete,
  Get,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import { CreateFieldConsultationDto, UpdateFieldConsultationDto } from '../dto';
import { FieldConsultationService } from '../services';

import { Permission } from '@auth/decorators';
import { Permission as P } from '@auth/enum';

@ApiTags('field-consultation')
@Controller('field-consultation')
export class FieldConsultationController {
  constructor(
    private readonly fieldConsultationService: FieldConsultationService,
  ) {}

  @Permission(P.ADMIN)
  @Post('create-fields')
  async createFieldsConsultation(@Body() data: CreateFieldConsultationDto) {
    return this.fieldConsultationService.createFields(data);
  }

  @Permission(P.ADMIN)
  @Put('update/:id')
  async updateFieldConsultation(
    @Param('id') id: number,
    @Body() data: UpdateFieldConsultationDto,
  ) {
    return this.fieldConsultationService.updateField(id, data);
  }

  @Permission(P.ADMIN)
  @Delete('delete/:id')
  async deleteFieldConsultation(@Param('id') id: number) {
    return this.fieldConsultationService.deleteField(id);
  }

  @Get('special-list')
  async getSpecialList() {
    return this.fieldConsultationService.getSpecialList();
  }

  @Get('special/:special')
  async getSpecial(@Param('special') special: string) {
    return this.fieldConsultationService.getSpecial(special);
  }
}
