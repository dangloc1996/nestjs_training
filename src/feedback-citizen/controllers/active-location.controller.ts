import { Body, Controller, Get, Put, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import { ActiveLocationService } from '../services/active-location.service';

import { UpdateActiveLocationDto, ActiveLocationQuery } from '../dto';
import { Permission } from '@auth/decorators';
import { Permission as P } from '@auth/enum';

@ApiTags('Active-Location')
@Controller('active-location')
export class ActiveLocationController {
  constructor(private readonly activeLocationService: ActiveLocationService) {}

  @Permission(P.ADMIN)
  @Put('active-update')
  updateActiveLocation(@Body() data: UpdateActiveLocationDto) {
    return this.activeLocationService.updateActiveLocation(data);
  }

  //  @Permission(P.ALL)
  @Get('cities')
  getActiveCities(@Query() query: ActiveLocationQuery) {
    return this.activeLocationService.getActiveCities(query);
  }

  //  @Permission(P.ALL)
  @Get('map')
  getActiveLocationMap() {
    return this.activeLocationService.getActiveLocationMap();
  }

  //  @Permission(P.ALL)
  @Get('list')
  getActiveLocationList() {
    return this.activeLocationService.getActiveLocationList();
  }
}
