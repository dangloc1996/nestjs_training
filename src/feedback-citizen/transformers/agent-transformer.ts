import { ApiProperty } from '@nestjs/swagger';

export class AgentTransformer {
  @ApiProperty()
  readonly id: number;

  @ApiProperty()
  readonly name: string;

  @ApiProperty()
  readonly user_id: string;

  @ApiProperty()
  readonly agent_id: string;

  @ApiProperty()
  readonly role: number;

  @ApiProperty()
  readonly email: string;

  @ApiProperty()
  phone_number: string;

  @ApiProperty()
  city: string;

  @ApiProperty()
  district: string;

  @ApiProperty()
  ward: string;

  @ApiProperty()
  field: string;

  @ApiProperty()
  group_id: string;

  @ApiProperty()
  created_at: string;
}
