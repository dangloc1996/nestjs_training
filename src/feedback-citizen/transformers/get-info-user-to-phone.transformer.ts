import { ApiProperty } from '@nestjs/swagger';

export class GetInfoUserToPhoneTransformers {
  @ApiProperty()
  readonly user_id: number;

  @ApiProperty()
  readonly name: string;

  @ApiProperty()
  readonly phone_number: string;

  @ApiProperty()
  readonly email: string;

  @ApiProperty()
  readonly avatar_url: string;
}
