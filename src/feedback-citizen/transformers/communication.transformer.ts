import { ApiProperty } from '@nestjs/swagger';

export class CallEventTransformer {
  @ApiProperty()
  id: string;

  @ApiProperty()
  callId?: string;

  @ApiProperty()
  callStatus?: string;

  @ApiProperty()
  projectId?: number;

  @ApiProperty()
  requestFromUserId?: string;

  @ApiProperty()
  accountSid?: string;

  @ApiProperty()
  timestampMs?: number;

  @ApiProperty()
  fromNumber?: string;

  @ApiProperty()
  fromAlias?: string;

  @ApiProperty()
  fromIsOnline?: boolean;

  @ApiProperty()
  fromType?: string;

  @ApiProperty()
  toNumber?: string;

  @ApiProperty()
  toAlias?: string;

  @ApiProperty()
  toIsOnline?: boolean;

  @ApiProperty()
  toType?: string;

  @ApiProperty()
  clientCustomData?: string;

  @ApiProperty()
  type?: string;

  @ApiProperty()
  callCreatedReason?: string;

  @ApiProperty()
  endCallCause?: string;

  @ApiProperty()
  endedBy?: string;

  @ApiProperty()
  callType?: string;

  @ApiProperty()
  duration?: number;

  @ApiProperty()
  answerDuration?: number;
}
