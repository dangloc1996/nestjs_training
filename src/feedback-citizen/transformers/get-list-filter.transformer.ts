import { ApiProperty } from '@nestjs/swagger';

export class GetListFilterTransformer {
  @ApiProperty()
  readonly user_id: number;

  @ApiProperty()
  readonly name: string;
}
