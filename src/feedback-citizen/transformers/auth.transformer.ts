import { ApiProperty } from '@nestjs/swagger';
import { AgentTransformer } from '@feedback-citizen/transformers/agent-transformer';

export class AuthTransformer {
  @ApiProperty()
  readonly user: AgentTransformer;

  @ApiProperty()
  readonly jwt_token: string;
}
