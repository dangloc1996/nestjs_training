import { ApiProperty } from '@nestjs/swagger';

class StaffExtra {
  @ApiProperty()
  id: string;

  @ApiProperty()
  role: string;

  @ApiProperty()
  field: string;

  @ApiProperty()
  city: string;

  @ApiProperty()
  district: string;

  @ApiProperty()
  ward: string;

  @ApiProperty()
  group_id: string;

  @ApiProperty()
  fieldName: string;

  @ApiProperty()
  groupName: string;

  @ApiProperty()
  require_done: string;

  @ApiProperty()
  require_processing: string;
}

export class ListAgentTransformer {
  @ApiProperty({
    description: 'Name of user',
  })
  name: string;

  @ApiProperty({
    description: 'id of user',
  })
  user_id: string;

  @ApiProperty({
    description: 'phone of user',
  })
  phone_number: string;

  @ApiProperty({
    description: 'email of user',
  })
  email: string;

  @ApiProperty({
    description: 'ADMIN: 1, AGENT: 2',
  })
  role: number;

  @ApiProperty({ type: [StaffExtra] })
  info: StaffExtra[];
}
