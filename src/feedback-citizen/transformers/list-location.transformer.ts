import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class ListLocationTransformer {
  @ApiProperty()
  @IsNotEmpty()
  readonly id: number;

  @ApiProperty()
  @IsNotEmpty()
  readonly code: string;

  @ApiProperty()
  @IsNotEmpty()
  readonly name: string;

  @ApiProperty()
  @IsNotEmpty()
  readonly level: number;

  @ApiProperty()
  readonly parent_id?: number;

  @ApiProperty()
  @IsNotEmpty()
  readonly lower_name: string;

  @ApiProperty()
  readonly childs?: Record<string, unknown>[];

  readonly parent?: Record<string, unknown>[];
}
