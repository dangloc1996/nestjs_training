import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class ListGroupTransformer {
  @ApiProperty()
  @IsNotEmpty()
  readonly group_id: number;

  @ApiProperty()
  @IsNotEmpty()
  readonly group_name: string;

  @ApiProperty()
  @IsNotEmpty()
  readonly number: string;
}
