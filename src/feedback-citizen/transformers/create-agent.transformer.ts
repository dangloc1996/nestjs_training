import { ApiProperty } from '@nestjs/swagger';

export class AuthTransformer {
  @ApiProperty()
  readonly agent_id: string;
}
