import { ApiProperty } from '@nestjs/swagger';

export class TicketIdTransformer {
  @ApiProperty({ default: 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx' })
  readonly id: string;
}
