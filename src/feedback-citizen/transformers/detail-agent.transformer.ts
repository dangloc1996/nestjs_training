import { ApiProperty } from '@nestjs/swagger';

export class DetailAgentTransformer {
  @ApiProperty()
  readonly id: number;

  @ApiProperty()
  readonly name: string;

  @ApiProperty()
  readonly user_id: string;

  @ApiProperty()
  readonly agent_id: string;

  @ApiProperty()
  readonly role: string;

  @ApiProperty()
  readonly email: string;

  @ApiProperty()
  readonly phone_number: string;

  @ApiProperty({
    description:
      '[group_id: NUX3TOOI, field: 5, city: HCM, district: 010, ward: 0012]',
  })
  readonly info: [];

  @ApiProperty()
  readonly created_at: string;
}
