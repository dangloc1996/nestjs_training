import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class UserTransformer {
  @ApiProperty()
  @IsNotEmpty()
  readonly id: number;

  @ApiProperty()
  @IsNotEmpty()
  readonly name: string;

  @ApiProperty()
  @IsNotEmpty()
  readonly avatar_url: string;

  @ApiProperty()
  @IsNotEmpty()
  readonly dob: Date;

  @ApiProperty()
  @IsNotEmpty()
  readonly phone_number: string;

  @ApiProperty()
  @IsNotEmpty()
  readonly gender: number;

  @ApiProperty()
  @IsNotEmpty()
  readonly user_id: number;
}
