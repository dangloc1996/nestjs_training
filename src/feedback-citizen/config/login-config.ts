export interface ILoginHeader {
  'partner-id': string;
  'partner-name': string;
  'partner-secret': string;
}

export function loginHeader(): ILoginHeader {
  return {
    'partner-id': 'partnerId',
    'partner-name': 'partnerName',
    'partner-secret': 'partnerSecret',
  };
}
