import { Express } from 'express';
const uuid = require('uuid-random');

/// File utils
export function GetExtension(filename) {
  const extension = filename.split('.').pop();
  return extension;
}

export function GenImagePath(id: number) {
  return `ott-ticket/${id}/${uuid()}`;
}

export function CheckValidKey(key: string) {
  return key.startsWith('ott-ticket/');
}

export function GetImagePath(path: string) {
  const parts = path.split('/');
  parts.pop();
  return parts.join('/');
}

export function IsImage(file: Express.Multer.File) {
  const mimetype = file.mimetype;
  return mimetype.startsWith('image/');
}

export function AllIsImage(files: Array<Express.Multer.File>) {
  let result = true;
  files.forEach((file) => {
    result = result && IsImage(file);
  });
  return result;
}

/// Location utils
export function CheckLocation(userLocation, ticketLocation) {
  return (
    userLocation.city == ticketLocation.city &&
    userLocation.district == ticketLocation.district &&
    userLocation.ward == ticketLocation.ward
  );
}

// Other utils
export function Trim(obj: any) {
  for (const key in obj) {
    if (typeof obj[key] == 'string') {
      obj[key] = obj[key].trim();
    }
  }
  return obj;
}

export function getDateFromVietnameseFormat(date) {
  const parts = date.split('/');
  const tmp = parts[0];
  parts[0] = parts[1];
  parts[1] = tmp;
  return new Date(parts.join('/'));
}

export function CreateTicketMessage(groupName: string) {
  return `Cảm ơn bạn đã gửi yêu cầu hỗ trợ đến ${groupName}. Chúng tôi sẽ chuyển thông tin đến bộ phận chuyên môn và phản hồi sớm nhất đến bạn. Theo dõi yêu cầu hỗ trợ của bạn theo đường link bên dưới:`;
}
