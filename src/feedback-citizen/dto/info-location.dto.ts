import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class InfoLocationDto {
  @ApiProperty()
  @IsNotEmpty()
  readonly city: string;

  @ApiProperty()
  @IsNotEmpty()
  readonly district: string;

  @ApiProperty()
  @IsNotEmpty()
  readonly ward: string;

  @ApiProperty()
  @IsNotEmpty()
  readonly group_id: string;

  @ApiProperty()
  @IsNotEmpty()
  readonly field: string;
}
