import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreatePartnerUserDto {
  @ApiProperty({
    description: 'Array info partner user | note: group_id is number_id',
    isArray: true,
    default: [
      {
        name: 'test',
        phone_number: '84374380020',
        email: 'test@gmail.com',
        city: 'Thành Phố Hồ Chí Minh',
        district: 'Quận Tân Bình',
        ward: 'Phường 01',
        group_id: 'Tổng đài 1022',
        field: 'Viễn thông',
      },
    ],
  })
  @IsNotEmpty()
  readonly data: [];
}
