import { ApiProperty } from '@nestjs/swagger';
import { Allow } from 'class-validator';

export class GetListFilterDto {
  @ApiProperty({ required: false })
  @Allow()
  biz?: string;
}
