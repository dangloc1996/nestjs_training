import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, Validate, IsString } from 'class-validator';
import { IsPhoneNumberRule } from '../validations/is-phone-number.validation';

export class GetTokenDto {
  @ApiProperty()
  @IsNotEmpty()
  @Validate(IsPhoneNumberRule)
  readonly phone_number: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  readonly password: string;
}
