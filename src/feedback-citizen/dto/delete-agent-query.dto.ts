import { ApiProperty } from '@nestjs/swagger';
import { Allow } from 'class-validator';

export class DeleteAgentQueryDTO {
  @ApiProperty({ required: false })
  @Allow()
  numberId?: string;
}
