import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class UpdateRoleDto {
  @ApiProperty({ description: '1: admin, 2: agent' })
  @IsNotEmpty()
  readonly role: number;
}
