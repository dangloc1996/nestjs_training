import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, Allow } from 'class-validator';

export class StaffExtraDto {
  @ApiProperty()
  @IsNotEmpty()
  role?: any;

  @ApiProperty()
  @Allow()
  field?: number;

  @ApiProperty()
  @IsNotEmpty()
  city?: string;

  @ApiProperty()
  @Allow()
  district?: string;

  @ApiProperty()
  @Allow()
  ward?: string;

  @ApiProperty()
  @Allow()
  group_id?: string;
}
