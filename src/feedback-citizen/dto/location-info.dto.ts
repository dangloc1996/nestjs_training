export class LocationInfoDto {
  city?: string;
  district?: string;
  ward?: string;
}
