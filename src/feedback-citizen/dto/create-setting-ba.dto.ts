import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateSettingBaDto {
  @ApiProperty({ description: 'Id of number => provide by chat module' })
  @IsNotEmpty()
  readonly number_id: string;

  @ApiProperty({
    description: 'The hotline number => provide by chat module queue',
  })
  @IsNotEmpty()
  readonly number: string;

  @ApiProperty()
  @IsNotEmpty()
  readonly group_name: string;
}
