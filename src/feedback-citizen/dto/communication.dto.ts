import { ApiProperty } from '@nestjs/swagger';
import { Allow } from 'class-validator';

export class CallQueryDTO {
  @ApiProperty({ required: false, description: 'default 0' })
  @Allow()
  page?: string;

  @ApiProperty({ required: false, description: 'default = max = 100' })
  @Allow()
  limit?: string;

  @ApiProperty({
    required: false,
    description:
      'Default: sort_by=start_time; (id,from_number,to_number, start_time, answer_time, stop_time, amount, created)',
  })
  @Allow()
  sort_by?: string;

  @ApiProperty({
    required: false,
    description: 'Default: order_by=desc; (asc, desc)',
  })
  @Allow()
  order_by?: string;

  @ApiProperty({
    required: false,
    description: 'from_user_id and to_user_id are empty => call to hotline',
  })
  @Allow()
  from_user_id?: string;

  @ApiProperty({
    required: false,
    description: '"from_user_id and to_user_id are empty => call to hotline"',
  })
  @Allow()
  to_user_id?: string;
}
