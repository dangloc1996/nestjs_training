import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, Allow } from 'class-validator';

export class FieldConsultationDto {
  @ApiProperty()
  @IsNotEmpty()
  name: string;

  @ApiProperty()
  @Allow()
  special?: string;
}

export class CreateFieldConsultationDto {
  @ApiProperty()
  @IsNotEmpty()
  readonly number_id: string;

  @ApiProperty()
  @IsNotEmpty()
  readonly list_field: FieldConsultationDto[];
}

export class UpdateFieldConsultationDto {
  @ApiProperty()
  @IsNotEmpty()
  readonly name: string;
}
