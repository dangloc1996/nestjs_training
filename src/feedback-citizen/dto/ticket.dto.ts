import { ApiProperty } from '@nestjs/swagger';
import { Allow, IsNotEmpty, IsEmpty } from 'class-validator';
import { Express } from 'express';

export class ClientTicketDto {
  @ApiProperty()
  @IsNotEmpty()
  title: string;

  @ApiProperty()
  @IsNotEmpty()
  description: string;

  @ApiProperty()
  @IsNotEmpty()
  type: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  biz: string;

  @ApiProperty({ required: true })
  @Allow()
  city: string;

  @ApiProperty({ required: true })
  @Allow()
  district: string;

  @ApiProperty({ required: true })
  @Allow()
  ward: string;

  @ApiProperty({ required: false })
  @Allow()
  assignee: string;

  @ApiProperty({ required: false })
  @Allow()
  creator: string;

  @ApiProperty()
  @IsNotEmpty()
  hasImages: string;

  @ApiProperty({ required: false, format: 'binary' })
  @Allow()
  images: Array<Express.Multer.File> | Array<string>;

  @ApiProperty({ required: false, description: 'address' })
  @Allow()
  x_address: string;
}

export class UpdateTicketDto {
  @ApiProperty({ required: false })
  @Allow()
  title: string;

  @ApiProperty({ required: false })
  @Allow()
  description: string;

  @ApiProperty({ required: false })
  @Allow()
  type: string;

  @ApiProperty({ required: false })
  @Allow()
  biz: string;

  @ApiProperty({ required: false })
  @Allow()
  city: string;

  @ApiProperty({ required: false })
  @Allow()
  district: string;

  @ApiProperty({ required: false })
  @Allow()
  ward: string;

  @ApiProperty({ required: false })
  @Allow()
  assignee: string;

  @ApiProperty({ required: false })
  @Allow()
  creator: string;

  @ApiProperty()
  @IsNotEmpty()
  hasImages: string;

  @ApiProperty({ required: false, format: 'binary' })
  @Allow()
  images: Array<Express.Multer.File> | Array<string>;

  @ApiProperty({ required: false, description: 'address' })
  @Allow()
  x_address: string;
}

export class TicketQueryDto {
  @ApiProperty({ required: false })
  @Allow()
  status?: string;

  @ApiProperty({ required: false })
  @Allow()
  assignee?: string;

  @ApiProperty({ required: false })
  @Allow()
  creator?: string;

  @ApiProperty({ required: false })
  @Allow()
  city?: string;

  @ApiProperty({ required: false })
  @Allow()
  district?: string;

  @ApiProperty({ required: false })
  @Allow()
  ward?: string;

  @ApiProperty({ required: false })
  @Allow()
  biz?: string;

  @ApiProperty({ required: false })
  @Allow()
  type?: string;

  @ApiProperty({ required: false, description: 'default 0' })
  @Allow()
  page?: number;

  @ApiProperty({ required: false, description: 'default 10, max 100' })
  @Allow()
  limit?: number;

  @ApiProperty({ required: false, description: 'Ngày tạo từ (dd-mm-yyyy)' })
  @Allow()
  createdFrom?: string;

  @ApiProperty({ required: false, description: 'Ngày tạo đến (dd-mm-yyyy)' })
  @Allow()
  createdTo?: string;

  @ApiProperty({ required: false, description: 'Bắt đầu nhận từ (dd-mm-yyyy)' })
  @Allow()
  startedFrom?: string;

  @ApiProperty({
    required: false,
    description: 'Bắt đầu nhận đến (dd-mm-yyyy)',
  })
  @Allow()
  startedTo?: string;

  @ApiProperty({ required: false, description: 'Kết thúc từ (dd-mm-yyyy)' })
  @Allow()
  completedFrom?: string;

  @ApiProperty({ required: false, description: 'Kết thúc đến (dd-mm-yyyy)' })
  @Allow()
  completedTo?: string;

  @ApiProperty({ required: false, description: 'Kết thúc đến (dd-mm-yyyy)' })
  @Allow()
  search?: string;
}

export class MyTicketQueryDto {
  @ApiProperty({ required: false })
  @Allow()
  status?: string;

  @ApiProperty({ required: false })
  @Allow()
  assignee?: string;

  @ApiProperty({ required: false })
  @Allow()
  city?: string;

  @ApiProperty({ required: false })
  @Allow()
  district?: string;

  @ApiProperty({ required: false })
  @Allow()
  ward?: string;

  @ApiProperty({ required: false })
  @Allow()
  biz?: string;

  @ApiProperty({ required: false })
  @Allow()
  type?: string;

  @ApiProperty({ required: false, description: 'default 0' })
  @Allow()
  page?: number;

  @ApiProperty({ required: false, description: 'default 10, max 100' })
  @Allow()
  limit?: number;

  @ApiProperty({ required: false, description: 'Ngày tạo từ (dd-mm-yyyy)' })
  @Allow()
  createdFrom?: string;

  @ApiProperty({ required: false, description: 'Ngày tạo đến (dd-mm-yyyy)' })
  @Allow()
  createdTo?: string;

  @ApiProperty({ required: false, description: 'Bắt đầu nhận từ (dd-mm-yyyy)' })
  @Allow()
  startedFrom?: string;

  @ApiProperty({
    required: false,
    description: 'Bắt đầu nhận đến (dd-mm-yyyy)',
  })
  @Allow()
  startedTo?: string;

  @ApiProperty({ required: false, description: 'Kết thúc từ (dd-mm-yyyy)' })
  @Allow()
  completedFrom?: string;

  @ApiProperty({ required: false, description: 'Kết thúc đến (dd-mm-yyyy)' })
  @Allow()
  completedTo?: string;
}

export class ApiTicketDto {
  title: string;
  description: string;
  type: string;
  status: string;
  priority: number;
  assignee: string;
  extra: Record<string, any>;
  creator: string;
  numberId: string;
}

export class AnswerTicketDto {
  @ApiProperty()
  @IsNotEmpty()
  answer: string;

  @ApiProperty()
  @IsNotEmpty()
  hasImages: string;

  @ApiProperty({ required: false, format: 'binary' })
  @Allow()
  images: Array<Express.Multer.File> | Array<string>;
}

export class VaccineReplyTicketDto {
  @ApiProperty()
  @IsNotEmpty()
  accept: boolean;

  @ApiProperty()
  @IsNotEmpty()
  answer: string;
}

export class VaccineRegistationDto {
  @ApiProperty()
  @IsNotEmpty()
  name: string;

  @ApiProperty()
  @IsNotEmpty()
  dob: string;

  @ApiProperty({ description: '0: Name, 1: Nữ' })
  @IsNotEmpty()
  gender: string;

  @ApiProperty({ description: 'Số CMNN' })
  @IsNotEmpty()
  idCard: string;

  @ApiProperty({ description: 'Dân tộc' })
  @IsNotEmpty()
  ethnic: string;

  @ApiProperty()
  @IsNotEmpty()
  nationality: string;

  @ApiProperty({ description: 'Số bảo hiểm y tế' })
  @IsNotEmpty()
  insuranceNumber: string;

  @ApiProperty()
  @IsNotEmpty()
  city: string;

  @ApiProperty()
  @IsNotEmpty()
  district: string;

  @ApiProperty()
  @IsNotEmpty()
  ward: string;

  @ApiProperty()
  @IsNotEmpty()
  adress: string;

  @ApiProperty()
  @IsNotEmpty()
  job: string;

  @ApiProperty({ description: 'Đối tượng' })
  @IsNotEmpty()
  subject: string;

  @ApiProperty({ description: 'Tiêm mũi thứ (1 | 2)' })
  @IsNotEmpty()
  doseNumber: string;

  @ApiProperty({ description: 'Loại vaccine' })
  @IsNotEmpty()
  doseType: string;

  @ApiProperty({ description: 'Ngày tiêm vaccine, (format: dd/mm/yyyy)' })
  @IsNotEmpty()
  vaccineDate: string;

  @ApiProperty({
    description: '10 option trong trang 2 [giá trị: (0 | 1 | 2)]',
  })
  @IsNotEmpty()
  p2options: string[];

  @ApiProperty({ description: 'Phản ứng sau tiêm nếu có', required: false })
  @Allow()
  p2description: string;
}

export class VaccineTicketDto {
  @ApiProperty({ required: false })
  @Allow()
  assignee?: string;

  @ApiProperty({ required: false })
  @IsEmpty()
  creator?: string;

  @ApiProperty({ required: false })
  @IsEmpty()
  biz?: string;

  @ApiProperty({ required: false })
  @IsEmpty()
  type?: string;

  @ApiProperty({ required: true })
  @Allow()
  vaccineRegistraion: VaccineRegistationDto;
}

export class CloseTicketDto {
  @ApiProperty({ required: false })
  @Allow()
  reason?: string;
}

export class MultipleAssignDto {
  @ApiProperty({ required: true })
  @IsNotEmpty()
  ticketIds: string[];

  @ApiProperty({ required: false })
  @Allow()
  userId: number;
}
