import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class QueueDetailDto {
  @ApiProperty({ description: 'The queue name' })
  @IsNotEmpty()
  readonly name: string;

  @ApiProperty({ default: true, description: 'Record calls?' })
  readonly record_calls: boolean;

  @ApiProperty({
    default: true,
    description: 'Enabling wrap-up gives the agent time to finish',
  })
  readonly agent_wrap_up_after_calls: boolean;

  @ApiProperty({ description: 'Wrap-up time limit (seconds)' })
  readonly wrap_up_time_limit: number;

  @ApiProperty({ description: '1=Always route calls; 2=Using business hours' })
  readonly schedule: number;

  @ApiProperty({ description: 'Wait agent answer timeout (seconds); min=3' })
  readonly wait_agent_answer_timeout: number;

  @ApiProperty({
    default: null,
    description:
      "The from phone number when callout to agent's phone number => hotline number",
  })
  readonly from_number_callout_to_agent: string;

  @ApiProperty({
    default: 1,
    description:
      '1: Route the call to the agent only if the agent is online on Web/App (or offline but has push notification);\n' +
      '2: Route the call to the agent when the agent is online on Web/App or SIP phone (or offline but has push notification)',
  })
  readonly cond_routing: number;
}
