import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class GroupDetailDto {
  @ApiProperty({ description: 'The group name' })
  @IsNotEmpty()
  readonly name: string;
}
