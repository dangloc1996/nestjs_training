import { ApiProperty } from '@nestjs/swagger';
import { Allow } from 'class-validator';

export class ListAgentQueryDto {
  @ApiProperty({ required: false })
  @Allow()
  page?: number;

  @ApiProperty({ required: false })
  @Allow()
  page_size?: number;

  @ApiProperty({ required: false })
  @Allow()
  city?: string;

  @ApiProperty({ required: false })
  @Allow()
  district?: string;

  @ApiProperty({ required: false })
  @Allow()
  ward?: string;

  @ApiProperty({ required: false })
  @Allow()
  type?: string;

  @ApiProperty({ required: false })
  @Allow()
  biz?: string;
}
