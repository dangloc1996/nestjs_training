import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, Validate } from 'class-validator';

import { IsPhoneNumberRule } from '../validations/is-phone-number.validation';
import { StaffExtraDto } from './staff-extra.dto';

export class CreateAgentDto {
  @ApiProperty({
    description: 'Name of user',
  })
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    description: 'id of user',
  })
  @IsNotEmpty()
  user_id: string;

  @ApiProperty({
    description: 'phone of user',
  })
  @IsNotEmpty()
  @Validate(IsPhoneNumberRule)
  phone_number: string;

  @ApiProperty({
    description: 'email of user',
  })
  @IsNotEmpty()
  email: string;

  @ApiProperty({ type: [StaffExtraDto] })
  @IsNotEmpty()
  info: StaffExtraDto[];
}
