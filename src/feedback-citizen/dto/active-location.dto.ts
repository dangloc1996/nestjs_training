import { ApiProperty } from '@nestjs/swagger';
import { Allow, IsNotEmpty } from 'class-validator';

export class ActiveLocationQuery {
  @ApiProperty({ required: false })
  @Allow()
  active: boolean;
}

export class UpdateActiveLocationDto {
  @ApiProperty({ required: true })
  @IsNotEmpty()
  code: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  active: boolean;
}
