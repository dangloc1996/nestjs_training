import {
  ClientTicketDto,
  ApiTicketDto,
  VaccineTicketDto,
  VaccineRegistationDto,
} from '../dto';
import { Ticket } from '../entities';
import * as utils from '../utils';

export function ApiTicketSerialize(data: ClientTicketDto): ApiTicketDto {
  const images = data.images;
  const extra = {};
  if (data.hasImages == 'true') {
    for (let i = 0; i < images.length; ++i) {
      extra['images_' + String(i)] = data.images[i];
    }
  }
  for (const key in data) {
    if (key.startsWith('x_')) {
      extra[key] = data[key];
    }
  }
  extra['city'] = data.city;
  extra['district'] = data.district;
  extra['ward'] = data.ward;
  const result = new ApiTicketDto();
  result.title = data.title;
  result.description = data.description;
  result.type = String(data.type);
  result.status = data['status'] || 'TODO';
  result.numberId = data['numberId'];
  result.priority = 0;
  result.assignee = data.assignee ? String(data.assignee) : '-1';
  result.extra = utils.Trim(extra);
  result.creator = String(data.creator);
  return utils.Trim(result);
}

export function ApiVaccineTicketSerialize(
  data: VaccineTicketDto,
): ApiTicketDto {
  const extra = {};

  const rData = { ...data.vaccineRegistraion };

  rData.p2options.forEach((option, index) => {
    extra[`p2option_${index}`] = option;
  });
  delete rData.p2options;

  for (const key in rData) {
    if (key) extra[key] = rData[key];
  }

  const result = new ApiTicketDto();

  result.title = 'Đăng ký tiêm vaccine covid19';
  result.description = 'Đăng ký tiêm vaccine covid19';
  result.type = data.type;
  result.status = data['status'] || 'TODO';
  result.numberId = data['numberId'];
  result.priority = 0;
  result.assignee = data.assignee ? String(data.assignee) : '-1';
  result.extra = utils.Trim(extra);
  result.creator = String(data.creator);
  return utils.Trim(result);
}

export function ClientTicketSerialize(data: ApiTicketDto): ClientTicketDto {
  const extra = data.extra;
  const result = {};
  result['images'] = [];
  for (const key in extra) {
    switch (true) {
      case key.startsWith('images_'):
        {
          result['images'].push(extra[key]);
        }
        break;
      case key.startsWith('answer_images'):
        {
          result['answer_images'] = extra[key].split(',');
        }
        break;
      default:
        {
          result[key] = extra[key];
        }
        break;
    }
  }
  result['hasImages'] = result['images'].length > 0;
  result['title'] = data.title;
  result['description'] = data.description;
  result['type'] = data.type;
  result['assignee'] = data.assignee;
  result['creator'] = data.creator;
  result['status'] = data.status;
  result['numberId'] = data.numberId;

  return result as ClientTicketDto;
}

export function ClientVaccineTicketSerialize(
  data: ApiTicketDto,
): VaccineTicketDto {
  const extra = data.extra;
  const rData = new VaccineRegistationDto();
  rData['p2options'] = [];
  for (const key in extra) {
    if (key.startsWith('p2option_')) {
      rData['p2options'].push(extra[key]);
    } else {
      rData[key] = extra[key];
    }
  }

  const result = {};
  result['title'] = data.title;
  result['description'] = data.description;
  result['type'] = data.type;
  result['assignee'] = data.assignee;
  result['creator'] = data.creator;
  result['status'] = data.status;
  result['numberId'] = data.numberId;
  result['city'] = rData.city;
  result['district'] = rData.district;
  result['ward'] = rData.ward;

  result['vaccineRegistraion'] = rData;

  return result as VaccineTicketDto;
}

export function DbTicketSerialize(data: Ticket): ClientTicketDto {
  const extra = data.extra;
  const result = {};
  result['images'] = [];
  for (const key in extra) {
    switch (true) {
      case key.startsWith('images_'):
        {
          result['images'].push(extra[key]);
        }
        break;
      case key.startsWith('answer_images'):
        {
          result['answer_images'] = extra[key].split(',');
        }
        break;
      default:
        {
          result[key] = extra[key];
        }
        break;
    }
  }
  result['id'] = data.id;
  result['hasImages'] = result['images'].length > 0;
  result['title'] = data.title;
  result['description'] = data.title;
  result['type'] = String(data.field);
  result['assignee'] = String(data.assignee);
  result['creator'] = String(data.creator);
  result['status'] = data.status;
  result['numberId'] = data.number_id;

  result['city'] = data.city;
  result['district'] = data.district;
  result['ward'] = data.ward;

  result['assigneePhone'] = data.assignee_phone;
  result['userPhone'] = data.user_phone;
  result['createdAt'] = data.created_at;

  return result as ClientTicketDto;
}

export function InsertDbTicketSerialize(data: ClientTicketDto): Ticket {
  const images = data.images;
  const extra = {};
  if (data.hasImages == 'true') {
    for (let i = 0; i < images.length; ++i) {
      extra['images_' + String(i)] = data.images[i];
    }
  }
  for (const key in data) {
    if (key.startsWith('x_')) {
      extra[key] = data[key];
    }
  }
  const result = new Ticket();
  result.title = data.title;
  result.field = +data.type;
  result.status = data['status'] || 'TODO';
  result.number_id = data['numberId'];
  result.assignee = data.assignee ? +data.assignee : -1;
  result.extra = utils.Trim(extra);
  result.creator = +data.creator;
  result.city = data.city;
  result.district = data.district;
  result.ward = data.ward;

  return utils.Trim(result);
}

export function DbVaccineTicketSerialize(data: Ticket): VaccineTicketDto {
  const extra = data.extra;
  const rData = new VaccineRegistationDto();
  rData['p2options'] = [];
  for (const key in extra) {
    if (key.startsWith('p2option_')) {
      rData['p2options'].push(extra[key]);
    } else {
      rData[key] = extra[key];
    }
  }

  const result = {};
  result['id'] = data.id;
  result['title'] = data.title;
  result['description'] = data.title;
  result['assignee'] = String(data.assignee);
  result['creator'] = String(data.creator);
  result['status'] = data.status;
  result['numberId'] = data.number_id;
  result['city'] = rData.city;
  result['district'] = rData.district;
  result['ward'] = rData.ward;
  result['assigneePhone'] = data.assignee_phone;
  result['userPhone'] = data.user_phone;
  result['createdAt'] = data.created_at;

  result['vaccineRegistraion'] = rData;

  return result as VaccineTicketDto;
}

export function InsertDbVaccineTicketSerialize(data: VaccineTicketDto): Ticket {
  const extra = {};

  const rData = { ...data.vaccineRegistraion };

  rData.p2options.forEach((option, index) => {
    extra[`p2option_${index}`] = option;
  });
  delete rData.p2options;

  for (const key in rData) {
    if (key) extra[key] = rData[key];
  }

  const result = new Ticket();

  result.title = 'Đăng ký tiêm vaccine covid19';

  result.field = +data.type;
  result.status = data['status'] || 'TODO';
  result.number_id = data['numberId'];
  result.assignee = data.assignee ? +data.assignee : -1;
  result.city = rData.city;
  result.district = rData.district;
  result.ward = rData.ward;
  result.user_phone = data['userPhone'];
  result.assignee_phone = data['assigneePhone'];
  result.extra = utils.Trim(extra);
  result.creator = +data.creator;

  return utils.Trim(result);
}
