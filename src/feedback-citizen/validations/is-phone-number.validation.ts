import { Injectable } from '@nestjs/common';
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';

@ValidatorConstraint({ name: 'IsPhoneNumber', async: true })
@Injectable()
export class IsPhoneNumberRule implements ValidatorConstraintInterface {
  async validate(value: string) {
    if (typeof value !== 'string') return false;

    if (value && value.length > 0) {
      if (!value.match(/^[0-9]+$/)) {
        return false;
      }
      return (
        (value.startsWith('84') && value.length === 11) ||
        (value.startsWith('0') && value.length === 10)
      );
    }
    return false;
  }

  defaultMessage(args: ValidationArguments) {
    return `The number phone ${args.value} is invalid.`;
  }
}
