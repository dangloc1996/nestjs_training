export const jwtConstants = {
  secret:
    process.env.JWT_SECRET_KEY ||
    'U2FsdGVkX19c9akTmddKp5hgFKqIZxBdllzNvwfV6zc=',
  expiresIn: '6h',
  expireTime: 6 * 60 * 60,
};
