import { Module, ValidationPipe } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { APP_PIPE } from '@nestjs/core';

import { AuthModule } from '@auth/auth.module';
import { ProxyModule } from '@proxy/proxy.module';
import { UtilModule } from '@util/util.module';

import {
  AgentService,
  TicketService,
  BaSettingService,
  AccountService,
  FieldConsultationService,
  CommunicationService,
  ActiveLocationService,
} from './services';
import {
  AgentController,
  TicketController,
  AdminController,
  BaSettingController,
  UtilController,
  AccountController,
  FieldConsultationController,
  CommunicationController,
  ActiveLocationController,
} from './controllers';
import {
  BASetting,
  Staff,
  FieldConsultation,
  ActiveLocation,
  Ticket,
} from './entities';
import { AdminService } from '@feedback-citizen/services/admin.service';
import { ImportAgent } from '@feedback-citizen/entities/import-agent.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      BASetting,
      Staff,
      FieldConsultation,
      ImportAgent,
      ActiveLocation,
      Ticket,
    ]),
    ProxyModule,
    AuthModule,
    UtilModule,
  ],
  controllers: [
    AgentController,
    TicketController,
    AdminController,
    BaSettingController,
    UtilController,
    FieldConsultationController,
    AccountController,
    CommunicationController,
    ActiveLocationController,
  ],
  providers: [
    AgentService,
    TicketService,
    BaSettingService,
    AdminService,
    {
      provide: APP_PIPE,
      useValue: new ValidationPipe({ whitelist: true, transform: true }),
    },
    FieldConsultationService,
    AccountService,
    CommunicationService,
    ActiveLocationService,
  ],
  exports: [
    AgentService,
    BaSettingService,
    FieldConsultationService,
    ActiveLocationService,
    TicketService,
  ],
})
export class FeedbackCitizenModule {}
