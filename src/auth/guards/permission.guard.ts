import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

import { Permission } from '../enum';

@Injectable()
export class PermissionGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const permission = this.reflector.get<number>(
      'permission',
      context.getHandler(),
    );

    if (permission == Permission.ALL) {
      return true;
    }

    const request = context.switchToHttp().getRequest();
    const user = request.user;

    for (let i = 0; i < user.info.length; ++i) {
      const role = user.info[i].role;
      const hasPermission = (1 << role) & permission;
      if (hasPermission > 0) {
        return true;
      }
    }

    return false;
  }
}
