import { ExtractJwt, Strategy } from 'passport-jwt';

import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';

import { Config } from '@common/config';
import { UtilService } from '@util/services';

const JwtConfig = Config('jwt');

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly utilService: UtilService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: JwtConfig.secret,
    });
  }

  async validate(payload: any) {
    const user = payload;

    try {
      const staff = await this.utilService.mergedStaffDetail(user.id);
      staff['avatar_url'] = user.avatar_url;
      return staff;
    } catch (error) {
      console.log(error);
      return {
        ...user,
        user_id: user.id,
        info: [
          {
            role: 0,
          },
        ],
      };
    }
  }
}
