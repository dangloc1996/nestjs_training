import { applyDecorators, UseGuards, SetMetadata } from '@nestjs/common';
import {
  ApiForbiddenResponse,
  ApiUnauthorizedResponse,
  ApiBearerAuth,
} from '@nestjs/swagger';

import { PermissionGuard, JwtAuthGuard } from '../guards';
import { Permission as P } from '../enum';

function RolesResolve(permission) {
  const roles = [];
  if (P.ALL & permission) roles.push('ALL');
  if (P.USER & permission) roles.push('USER');
  if (P.ADMIN & permission) roles.push('ADMIN');
  if (P.AGENT & permission) roles.push('AGENT');
  if (P.ADMIN_CITY & permission) roles.push('CITY ADMIN');
  if (P.ADMIN_DISTRICT & permission) roles.push('DISTICT ADMIN');
  if (P.ADMIN_WARD & permission) roles.push('WARD ADMIN');

  if (roles.length == 0) {
    return 'ALL';
  }

  return roles.join(', ');
}

export function Permission(permission: number) {
  const desc =
    `Fobbiden exception, unless user has role ` +
    `∈ { ${RolesResolve(permission)} }`;
  return applyDecorators(
    SetMetadata('permission', permission),
    UseGuards(JwtAuthGuard, PermissionGuard),
    ApiBearerAuth(),
    ApiUnauthorizedResponse({ description: 'Unauthorized' }),
    ApiForbiddenResponse({ description: desc }),
  );
}
