import { applyDecorators, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiUnauthorizedResponse } from '@nestjs/swagger';

import { JwtAuthGuardAdmin } from '@auth/guards/jwt-auth-admin.guard';

export function IsAdmin() {
  // (...roles: Role[]) {
  return applyDecorators(
    // SetMetadata('roles', roles),
    // UseGuards(AuthGuard, RolesGuard),
    UseGuards(JwtAuthGuardAdmin),
    ApiBearerAuth(),
    ApiUnauthorizedResponse({ description: 'Unauthorized' }),
  );
}
