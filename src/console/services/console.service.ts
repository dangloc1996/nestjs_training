import { Injectable } from '@nestjs/common';
import { ConsoleService as NestConsoleService } from 'nestjs-console';
import { getManager } from 'typeorm';

import {
  Staff,
  BASetting,
  FieldConsultation,
  Ticket,
} from '@feedback-citizen/entities';
import {
  AgentService,
  BaSettingService,
  FieldConsultationService,
  ActiveLocationService,
  TicketService,
} from '@feedback-citizen/services';
import { UtilService } from '@util/services';
import { AuthProxy } from '@proxy/auth';
import { ChatProxy } from '@proxy/chat';
import { SqlQuery } from '@common/mysql';
import { Config, SaveConfig } from '@common/config';

import { Tests } from '../tests';

let HackAuthProxy = undefined;
let HackChatProxy = undefined;
let HackAgentService = undefined;
let HackBaSettingService = undefined;
let HackFieldService = undefined;
let HackActiveLocationService = undefined;
let HackUtilService = undefined;
let HackTicketService = undefined;

const Log = console.log;
console.log = () => {};

async function GetUser(phone: string, password: string) {
  const user = await HackAuthProxy.getUserNoStaff(phone, password);
  return user;
}

async function CreateAgent(data: any) {
  const result = await HackAgentService.createAgent(data);
  return result;
}

async function CreateBaSetting(data: any) {
  try {
    return await HackBaSettingService.createSettingBa(data);
  } catch (error) {
    Log('ERROR:', error.message);
  }
}

async function InitBaSetting() {
  try {
    const result = await HackBaSettingService.initSettingBa();
    Log(result);
  } catch (error) {
    Log('ERROR:', error.message);
  }
}

async function CreateFields(data: any) {
  try {
    const result = await HackFieldService.createFields(data);
    Log(result);
  } catch (error) {
    Log('ERROR:', error.message);
  }
}

async function InitActiveLocation() {
  try {
    const cities = await HackUtilService.getStaffsCities();
    for (const city of cities) {
      const result = await HackActiveLocationService.updateActiveLocation({
        code: city.code,
        active: true,
      });
      Log('UPDATED:', city, result ? 'activated' : 'deactivated');
    }
  } catch (error) {
    Log('ERROR:', error);
  }
}

async function ImportTicketsFromChatModule() {
  try {
    const tickets = (await HackChatProxy.getGeneralTicketList({})).tickets;
    for (const t of tickets) {
      const ticket = (await HackChatProxy.getGeneralTicket(t.id)).data;

      const fields = {
        id: ticket.id,
        title: ticket.title,
        field: ticket.type,
        status: ticket.status,
        number_id: ticket.numberId,
        assignee: ticket.assignee,
        creator: ticket.creator,
        city: ticket.extra.city,
        district: ticket.extra.district,
        ward: ticket.extra.ward,
        user_phone: ticket.extra.userPhone,
        assignee_phone: ticket.extra.assigneePhone,
        created_at: new Date(ticket.createdAt),
        extra: {},
      };

      try {
        delete ticket.extra.city;
      } catch (error) {}
      try {
        delete ticket.extra.district;
      } catch (error) {}
      try {
        delete ticket.extra.ward;
      } catch (error) {}
      try {
        delete ticket.extra.userPhone;
      } catch (error) {}
      try {
        delete ticket.extra.assigneePhone;
      } catch (error) {}

      fields.extra = ticket.extra;

      try {
        const insertData = new Ticket();
        for (const key in fields) {
          insertData[key] = fields[key];
        }
        await getManager().transaction(async (entityManager) => {
          try {
            await entityManager.save(insertData);
            Log('IMPORTED:', insertData.id);
          } catch (error) {
            Log('ERROR:', error);
          }
        });
      } catch (error) {
        Log('ERROR:', error);
      }
    }
  } catch (error) {
    Log('ERROR:', error);
  }
}

@Injectable()
export class ConsoleService {
  constructor(
    private readonly consoleService: NestConsoleService,
    private readonly authProxy: AuthProxy,
    private readonly chatProxy: ChatProxy,
    private readonly agentService: AgentService,
    private readonly baSettingService: BaSettingService,
    private readonly fieldService: FieldConsultationService,
    private readonly activeLocationService: ActiveLocationService,
    private readonly ticketService: TicketService,
    private readonly utilService: UtilService,
  ) {
    const cli = this.consoleService.getCli();
    HackChatProxy = chatProxy;
    HackAuthProxy = authProxy;
    HackAgentService = agentService;
    HackBaSettingService = baSettingService;
    HackFieldService = fieldService;
    HackActiveLocationService = activeLocationService;
    HackUtilService = utilService;
    HackTicketService = ticketService;

    this.consoleService.createCommand(
      {
        command: 'create-superuser',
        description: "Create ott's superuser base on exist user on mylocal",
        options: [
          {
            flags: '-n, --phonenumber <value>',
            description: `Superuser's phonenumber; [required]`,
            required: true,
          },
          {
            flags: '-p, --password <value>',
            description: `Superuser's password; [required]`,
            required: true,
          },
          {
            flags: '-e, --email <value>',
            description: `Superuser's email; [required]`,
            required: true,
          },
          {
            flags: '-c, --city <value>',
            description: `Superuser's city; [required]`,
            required: true,
          },
          {
            flags: '-d, --district <value>',
            description: `Superuser's district; [required]`,
            required: true,
          },
          {
            flags: '-w, --ward <value>',
            description: `Superuser's ward; [required]`,
            required: true,
          },
          {
            flags: '-f, --field <value>',
            description: `Superuser's field; [required]`,
            required: true,
          },
          {
            flags: '-g, --group <value>',
            description: `Superuser's group id; [required]`,
            required: true,
          },
        ],
      },
      this.createSuperUser,
      cli,
    );
    this.consoleService.createCommand(
      {
        command: 'create-tables',
        description: 'Create tables in database for this app',
      },
      this.createTables,
      cli,
    );
    this.consoleService.createCommand(
      {
        command: 'init-ba',
        description: "Init business account's settings for this app",
      },
      this.initBaSetting,
      cli,
    );
    this.consoleService.createCommand(
      {
        command: 'init-fields',
        description: 'Init fields in db',
      },
      this.initFields,
      cli,
    );
    this.consoleService.createCommand(
      {
        command: 'init-active-locations',
        description: `Init active location in db`,
      },
      this.initActiveLocations,
      cli,
    );
    this.consoleService.createCommand(
      {
        command: 'import-tickets',
        description: `Import tickets from chat module`,
      },
      this.importTicketsFromChatModule,
      cli,
    );
    this.consoleService.createCommand(
      {
        command: 'backup',
        description: `Clone current fields, groups, staffs from db`,
        options: [
          {
            flags: '-n --number <value>',
            description: `Backup number; [required]`,
            required: true,
          },
        ],
      },
      this.backupData,
      cli,
    );
    this.consoleService.createCommand(
      {
        command: 'load-backup',
        description: `Load fields, groups, staffs from backup`,
        options: [
          {
            flags: '-n --number <value>',
            description: `Backup number; [required]`,
            required: true,
          },
        ],
      },
      this.loadData,
      cli,
    );
    this.consoleService.createCommand(
      {
        command: 'run-tests',
        description: `Run unit tests`,
      },
      this.runTests,
      cli,
    );
  }

  async createSuperUser(options: any) {
    const args = options;
    const user = await GetUser(args.phonenumber, args.password);
    const data = {
      name: user.name,
      user_id: String(user.user_id),
      phone_number: String(args.phonenumber),
      email: args.email,
      info: [
        {
          role: 1,
          city: args.city,
          district: args.district,
          ward: args.ward,
          field: args.field,
          group_id: args.group,
        },
      ],
    };
    const result = await CreateAgent(data);
    Log(result);
  }

  async createTables() {
    try {
      await getManager().query(SqlQuery('create-ott_ticket_ba_setting'));
    } catch (error) {
      Log('ERROR:', error.message);
    }
    try {
      await getManager().query(
        SqlQuery('create-ott_ticket_field_consultation'),
      );
    } catch (error) {
      Log('ERROR:', error.message);
    }
    try {
      await getManager().query(SqlQuery('create-ott_ticket_staff'));
    } catch (error) {
      Log('ERROR:', error.message);
    }

    try {
      await getManager().query(SqlQuery('create-ott_ticket_import_agent'));
    } catch (error) {
      Log('ERROR:', error.message);
    }

    try {
      await getManager().query(SqlQuery('create-ott_ticket_active_location'));
    } catch (error) {
      Log('ERROR:', error.message);
    }

    try {
      await getManager().query(SqlQuery('create-ott_ticket_ticket'));
    } catch (error) {
      Log('ERROR:', error.message);
    }
  }

  async initBaSetting() {
    const baConfig = Config('ba');
    for (let i = 0; i < baConfig.length; ++i) {
      await CreateBaSetting(baConfig[i]);
    }
    await InitBaSetting();
  }

  async initFields() {
    const fieldConfig = Config('field');
    for (const key in fieldConfig) {
      const data = {
        number_id: key,
        list_field: fieldConfig[key],
      };
      await CreateFields(data);
    }
  }

  async initActiveLocations() {
    await InitActiveLocation();
  }

  async importTicketsFromChatModule() {
    await ImportTicketsFromChatModule();
  }

  async backupData(options: any) {
    const n = options['number'];

    const getStaff = getManager().query(`select * from ott_ticket_staff`);
    const getField = getManager().query(
      `select * from ott_ticket_field_consultation`,
    );
    const getGroup = getManager().query(`select * from ott_ticket_ba_setting`);

    const staffs = await getStaff;
    const fields = await getField;
    const groups = await getGroup;

    const backupData = {
      staffs,
      fields,
      groups,
    };
    SaveConfig(`backup-${n}`, JSON.stringify(backupData));
  }

  async loadData(options: any) {
    const n = options['number'];

    const data = Config(`backup-${n}`);

    const staffs = data.staffs;
    const fields = data.fields;
    const groups = data.groups;

    try {
      await getManager().transaction(async (entityManager) => {
        const createEntityList = <T>(data: any, Type: { new (): T }) => {
          const entities: T[] = [];
          data.forEach((map: any) => {
            const e = new Type();
            for (const key in map) {
              e[key] = map[key];
            }
            entities.push(e);
          });
          return entities;
        };
        const staffEntities = createEntityList<Staff>(staffs, Staff);
        const fieldEntities = createEntityList<FieldConsultation>(
          fields,
          FieldConsultation,
        );
        const groupEntities = createEntityList<BASetting>(groups, BASetting);

        const staffSave = entityManager.save(staffEntities);
        const fieldSave = entityManager.save(fieldEntities);
        const groupSave = entityManager.save(groupEntities);

        await staffSave;
        await fieldSave;
        await groupSave;
      });
    } catch (error) {
      Log(error);
    }
  }

  async runTests() {
    const tests = new Tests(
      HackAgentService,
      HackBaSettingService,
      HackFieldService,
      HackActiveLocationService,
      HackTicketService,
      HackUtilService,
    );
    await tests.testUtilService();
    await tests.testAgentService();
    await tests.TestTicketService();
  }
}
