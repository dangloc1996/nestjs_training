import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ConsoleModule as NestConsoleModule } from 'nestjs-console';
import { ConsoleService } from './services/console.service';
import { ProxyModule } from '@proxy/proxy.module';
import { FeedbackCitizenModule } from '@feedback-citizen/feedback-citizen.module';
import { UtilModule } from '@util/util.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot(),
    NestConsoleModule,
    ProxyModule,
    FeedbackCitizenModule,
    UtilModule,
  ],
  providers: [ConsoleService],
  exports: [],
})
export class ConsoleModule {}
