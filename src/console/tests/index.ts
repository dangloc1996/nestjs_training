import {
  AgentService,
  BaSettingService,
  FieldConsultationService,
  ActiveLocationService,
  TicketService,
} from '@feedback-citizen/services';
import { UtilService } from '@util/services';
import { Role } from '@auth/enum';

const Log = console.log;
console.log = () => {};

const Color = {
  Reset: '\x1b[0m',
  Bright: '\x1b[1m',
  Dim: '\x1b[2m',
  Underscore: '\x1b[4m',
  Blink: '\x1b[5m',
  Reverse: '\x1b[7m',
  Hidden: '\x1b[8m',

  FgBlack: '\x1b[30m',
  FgRed: '\x1b[31m',
  FgGreen: '\x1b[32m',
  FgYellow: '\x1b[33m',
  FgBlue: '\x1b[34m',
  FgMagenta: '\x1b[35m',
  FgCyan: '\x1b[36m',
  FgWhite: '\x1b[37m',

  BgBlack: '\x1b[40m',
  BgRed: '\x1b[41m',
  BgGreen: '\x1b[42m',
  BgYellow: '\x1b[43m',
  BgBlue: '\x1b[44m',
  BgMagenta: '\x1b[45m',
  BgCyan: '\x1b[46m',
  BgWhite: '\x1b[47m',
};

function Expect(ok) {
  if (!ok) {
    throw new Error(Color.FgRed + 'Test fail!' + Color.Reset);
  }
  Log(Color.FgGreen + 'OK!' + Color.Reset);
}

async function TestAsync(name, asyncfn) {
  Log(Color.FgCyan + name + Color.Reset);
  await asyncfn();
  Log(Color.FgCyan + 'DONE!' + Color.Reset);
  Log('-----------');
}

const TestsConfig = {
  users: {
    admin: {
      phone_number: '84891099260',
      password: '123456',
    },
    agents: [
      {
        phone_number: '84392040909',
        password: '123456',
      },
    ],
  },
};

export class Tests {
  constructor(
    private readonly agentService: AgentService,
    private readonly baSettingService: BaSettingService,
    private readonly fieldService: FieldConsultationService,
    private readonly activeLocationService: ActiveLocationService,
    private readonly ticketService: TicketService,
    private readonly utilService: UtilService,
  ) {}

  async testUtilService() {
    await TestAsync('Test get user info by phone', async () => {
      const utilService = this.utilService;
      const agentService = this.agentService;

      const admin = TestsConfig.users.admin;
      const loginResult = await agentService.getToken({
        phone_number: admin.phone_number,
        password: admin.password,
      });
      Expect(loginResult);
      const token = loginResult.jwt_token;
      const user = loginResult.user;
      Expect(token && user);

      const infoResult = await utilService.getUserInfoByPhone(
        user,
        '84392040909',
        'Bearer ' + token,
      );
      Expect(
        infoResult &&
          infoResult != 'user_not_found' &&
          infoResult.name &&
          infoResult.phone_number &&
          infoResult.user_id,
      );

      const infoResultV2 = await utilService.getUserInfoByPhoneV2(
        '84392040909',
      );
      Expect(
        infoResultV2 &&
          infoResultV2.name &&
          infoResultV2.phone_number &&
          infoResult.user_id,
      );
    });
  }

  async testAgentService() {
    const agentService = this.agentService;
    const utilService = this.utilService;
    const activeLocationService = this.activeLocationService;

    await TestAsync('Test Create and Update Agent', async () => {
      const agent = TestsConfig.users.agents[0];
      const user = await utilService.getUserInfoByPhoneV2(agent.phone_number);
      const activeCities = await activeLocationService.getActiveCities({
        active: true,
      });
      const city = activeCities[0];

      Log('Create Agent');
      {
        const info = (
          await utilService.autoFillInfo({
            info: [
              {
                role: Role.ADMIN,
                city: city.code,
              },
              {
                role: Role.ADMIN,
                city: city.code,
              },
            ],
          })
        ).info;

        for (const a of info) {
          a.role = Role.AGENT;
        }

        const result = await agentService.createAgent({
          name: user.name,
          phone_number: user.phone_number,
          user_id: user.user_id,
          email: 'test@asim.vn',
          info,
        });
        Expect(result);
      }

      Log('Update Agent');
      {
        const info = (
          await utilService.autoFillInfo({
            info: [
              {
                role: Role.ADMIN,
                city: city.code,
              },
              {
                role: Role.ADMIN,
                city: city.code,
              },
            ],
          })
        ).info;

        const result = await agentService.updateAgent(user.user_id, {
          name: user.name,
          phone_number: user.phone_number,
          user_id: user.user_id,
          email: 'test@asim.vn',
          info,
        });
        Expect(result);
      }
    });

    await TestAsync('Test Delete Agent', async () => {
      const agent = TestsConfig.users.agents[0];
      const user = await utilService.getUserInfoByPhoneV2(agent.phone_number);
      const result = await agentService.deleteAgentWithUserId(user.user_id, {});
      Expect(result);
    });
  }

  async TestTicketService() {
    const agentService = this.agentService;
    const ticketService = this.ticketService;
    const activeLocationService = this.activeLocationService;
    const utilService = this.utilService;

    await TestAsync('Test create ticket', async () => {
      const activeCities = await activeLocationService.getActiveCities({
        active: true,
      });
      const city = activeCities[0];
      const info = (
        await utilService.autoFillInfo({
          info: [
            {
              role: Role.ADMIN,
              city: city.code,
            },
          ],
        })
      ).info[0];

      const admin = TestsConfig.users.admin;
      const loginResult = await agentService.getToken({
        phone_number: admin.phone_number,
        password: admin.password,
      });
      const user = loginResult?.user;
      const token = loginResult?.jwt_token;
      Expect(user && token);

      const createTicketResult = await ticketService.createTicket(
        null,
        {
          title: 'Script testing',
          description: 'Script testing',
          type: String(info.field),
          biz: info.group_id,
          city: info.city,
          district: info.district,
          ward: info.ward,
          hasImages: 'false',
          assignee: null,
          creator: null,
          images: null,
          x_address: null,
        },
        user,
        'Bearer ' + token,
      );
      Expect(createTicketResult);

      const createVaccineTicketResult = await ticketService.createVaccineTicket(
        {
          vaccineRegistraion: {
            name: user.name,
            dob: '01/02/1987',
            gender: '1',
            idCard: '123456789',
            ethnic: 'Kinh',
            nationality: 'VN',
            insuranceNumber: '123456789',
            city: info.city,
            district: info.district,
            ward: info.ward,
            adress: 'To Huu, Thanh Xuan, Ha Noi',
            job: 'Lap trinh vien',
            subject:
              'Người làm việc trong các cơ sở y tế, ngành y tế (công lập và tư nhân)',
            doseNumber: '2',
            doseType: 'Astra Zecera',
            vaccineDate: '1/1/2022',
            p2options: ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0'],
            p2description: 'Khong co van de gi ca',
          },
        },
        user,
        'Bearer ' + token,
      );
      Expect(createVaccineTicketResult);
    });

    await TestAsync('Test get ticket list', async () => {
      const admin = TestsConfig.users.admin;
      const loginResult = await agentService.getToken({
        phone_number: admin.phone_number,
        password: admin.password,
      });
      const user = loginResult?.user;
      const token = loginResult?.jwt_token;
      Expect(user && token);

      Log('Ticket list');
      const tickets = await ticketService.getTicketList({}, user, token);
      Expect(tickets.listTotal && tickets.list);

      Log('My ticket list');
      const mytickets = await ticketService.getMyTicketList({}, user, token);
      Expect(mytickets.listTotal && mytickets.list);
      {
        let check = true;
        for (const ticket of mytickets.list) {
          check = check && ticket.creator == user.user_id;
        }
        Expect(check);
      }
    });

    await TestAsync("Test get ticket's status", async () => {});

    await TestAsync('Test assign ticket status', async () => {
      const admin = TestsConfig.users.admin;
      const loginResult = await agentService.getToken({
        phone_number: admin.phone_number,
        password: admin.password,
      });
      const user = loginResult?.user;
      const token = loginResult?.jwt_token;
      Expect(user && token);

      Log('Searching for assignable tickets');

      let page = 0;
      let count = 0;
      let tickets = undefined;
      while (count < 100) {
        const ticketListResult = await ticketService.getTicketList(
          {
            status: 'TODO',
            limit: 2,
            page,
          },
          user,
          token,
        );
        if (tickets.listTotal > 0) {
          tickets = tickets;
          break;
        } else {
          page += 1;
        }
        count += 1;
      }

      if (!tickets) {
        const ticketListResult = await ticketService.getTicketList(
          {
            status: 'TODO',
            limit: 2,
            page,
          },
          user,
          token,
        );
      }

      const assignList = await ticketService.assignMultiTicketAssignableList(
        {
          ticketIds: tickets.list.map((ticket) => ticket.id),
          userId: undefined,
        },
        user,
      );
    });
  }
}
