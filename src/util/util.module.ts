import { Module } from '@nestjs/common';

import { TypeOrmModule } from '@nestjs/typeorm';

import { UtilService } from './services/util.service';

import { ProxyModule } from '@proxy/proxy.module';
import {
  BASetting,
  Staff,
  FieldConsultation,
} from '@feedback-citizen/entities';

@Module({
  imports: [
    TypeOrmModule.forFeature([BASetting, Staff, FieldConsultation]),
    ProxyModule,
  ],
  providers: [UtilService],
  exports: [UtilService],
})
export class UtilModule {}
