import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AuthProxy } from '@proxy/auth';
import { ChatProxy } from '@proxy/chat';

import { Role } from '@auth/enum';
import { Config, SaveConfig } from '@common/config';
import { RunUtil } from '@common/util';
import { BadRequest, Forbidden } from '@common/errors';
import { Sleep } from '@common/util';

import {
  Staff,
  FieldConsultation,
  BASetting,
} from '@feedback-citizen/entities';
import {
  VaccineTicketDto,
  LocationInfoDto,
  StaffExtraDto,
} from '@feedback-citizen/dto';
import * as utils from '@feedback-citizen/utils';

// TODO(): Resovle city, district, ward seperately
function ResolveName(name: string) {
  const prefixList = [
    // city
    'T.',
    'Tỉnh ',
    'Thành Phố ',
    'TP.',
    'TP ',
    // district
    'P.',
    'Quận ',
    'Huyện ',
    'H.',
    // ward
    'Thị Trấn',
    'T.Trấn ',
    'TT. ',
    'Xã ',
    'Thị Xã ',
    'TX.',
    'Phường ',
    'P. ',
  ];
  for (let i = 0; i < prefixList.length; ++i) {
    const prefix = prefixList[i];
    const pos = name.indexOf(prefix);
    if (pos >= 0) {
      const result = name.substring(pos + prefix.length);
      return result;
    }
  }
  return name;
}

function LocationConfigMap(locationConfig) {
  const codeMap = {};
  const nameMap = {};

  locationConfig.forEach((city) => {
    const code = city.code;
    let name = city.name;
    const childs = city.childs;
    {
      codeMap[code] = { name, childs };
      const cityMap = codeMap[code];
      cityMap.childs.forEach((district) => {
        const code = district.code;
        const name = district.name;
        const childs = district.childs;

        cityMap[code] = { name, childs };

        const districtMap = cityMap[district.code];
        districtMap.childs.forEach((ward) => {
          districtMap[ward.code] = {};
          districtMap[ward.code] = {
            name: ward.name,
          };
        });
        delete districtMap.childs;
      });
      delete cityMap.childs;
    }
    {
      name = ResolveName(name);
      nameMap[name] = { code, childs };
      const cityMap = nameMap[name];
      cityMap.childs.forEach((district) => {
        const code = district.code;
        const name = ResolveName(district.name);
        const childs = district.childs;

        cityMap[name] = { code, childs };
        const districtMap = cityMap[name];

        districtMap.childs.forEach((ward) => {
          const name = ResolveName(ward.name);
          const code = ward.code;
          districtMap[name] = {};
          districtMap[name] = { code };
        });
        delete districtMap.childs;
      });
      delete cityMap.childs;
    }
  });
  return { codeMap, nameMap };
}

@Injectable()
export class UtilService {
  locationCodeMap = undefined;
  locationNameMap = undefined;
  vaccineConfig = undefined;
  covidNumberId = undefined;

  constructor(
    @InjectRepository(Staff)
    private staffRepository: Repository<Staff>,
    @InjectRepository(FieldConsultation)
    private fieldConsultationRepository: Repository<FieldConsultation>,
    @InjectRepository(BASetting)
    private baSettingRepository: Repository<BASetting>,
    private readonly authProxy: AuthProxy,
    private readonly chatProxy: ChatProxy,
  ) {
    this.downloadLocationPatch();
    this.covidNumberId = process.env.COVID_NUMBER_ID;
    this.vaccineConfig = Config('vaccine');
  }

  async getUserInfoByPhone(currentUser, phoneNumber: string, jwt) {
    try {
      if (currentUser.phone_number === phoneNumber) {
        return {
          name: currentUser.name,
          phone_number: currentUser.phone_number,
          avatar_url: currentUser.avatar_url,
          email: currentUser.email,
          user_id: currentUser.user_id,
        };
      }

      return await this.authProxy.getUserByPhone(phoneNumber, jwt);
    } catch (error) {
      console.log(error);
      return 'user_not_found';
    }
  }

  async getUserInfoByPhoneV2(phoneNumber: string) {
    try {
      return this.authProxy.getUserByPhoneV2(phoneNumber);
    } catch (error) {
      console.log(error);
      return 'user_not_found';
    }
  }

  async getStaffPhoneNumberById(userId: string) {
    const result = await this.staffRepository.find({
      where: {
        user_id: +userId,
      },
      select: ['phone_number'],
      take: 1,
    });
    if (result && result.length > 0) {
      return result[0].phone_number;
    }

    {
      const message = `this staff with user_id not found: ${userId}`;
      throw new HttpException(message, HttpStatus.NOT_FOUND);
    }
  }

  async downloadLocationPatch() {
    try {
      const locationData = await this.authProxy.getLocation();
      SaveConfig('location', JSON.stringify(locationData));
    } catch (error) {
      console.log('Unable to download location-list');
    }

    const locationMap = LocationConfigMap(Config('location'));

    this.locationCodeMap = locationMap.codeMap;
    this.locationNameMap = locationMap.nameMap;
    return true;
  }

  async getLocationMap() {
    return this.locationCodeMap;
  }

  async getStaffsLocation() {
    const staffs = await this.staffRepository.find({
      select: ['city', 'district', 'ward'],
    });

    const locationMap = {};

    for (const staff of staffs) {
      const city = staff.city;
      const district = staff.district;
      const ward = staff.ward;

      if (!locationMap[city]) {
        locationMap[city] = {};
      }
      if (!locationMap[city][district]) {
        locationMap[city][district] = {};
      }
      if (!locationMap[city][district][ward]) {
        locationMap[city][district][ward] = 1;
      }
    }

    return locationMap;
  }

  async getStaffsCities() {
    await this.waitLocationLoad();
    const staffs = await this.staffRepository.find({
      select: ['city'],
    });

    const locationSet = new Set<string>();
    for (const staff of staffs) {
      const city = staff.city;
      if (!locationSet.has[city]) {
        locationSet.add(city);
      }
    }
    const result = [];
    locationSet.forEach((city) => {
      result.push({
        code: city,
        name: this.locationCodeMap[city].name,
      });
    });
    return result;
  }

  async waitLocationLoad() {
    {
      let failedCount = 0;
      while (!this.locationCodeMap && failedCount < 20) {
        Sleep(100);
        failedCount += 1;
      }
      if (failedCount >= 20) {
        console.log('Cannot load locationCodeMap');
        await this.downloadLocationPatch();
      }
    }
  }

  async getLocationName(
    cityCode: string,
    districtCode?: string,
    wardCode?: string,
    errorThrows?: boolean,
  ) {
    await this.waitLocationLoad();
    if (!errorThrows) {
      let city: any = null,
        district: any = null,
        ward: any = null;
      city = this.locationCodeMap[cityCode];
      if (districtCode) {
        district = city[districtCode];
        if (wardCode) {
          ward = district[wardCode];
        }
      }
      return {
        city: city ? city.name : undefined,
        district: district ? district.name : undefined,
        ward: ward ? ward.name : undefined,
      };
    }
    let city: any = null,
      district: any = null,
      ward: any = null;
    const cityErrorMsg = 'city code not valid';
    const districtErrorMsg = 'district code not valid';
    const wardErrorMsg = 'ward code not valid';

    city = this.locationCodeMap[cityCode];
    if (!city) throw new BadRequest(cityErrorMsg);
    if (districtCode) {
      district = city[districtCode];
      if (!district) throw new BadRequest(districtErrorMsg);
      if (wardCode) {
        ward = district[wardCode];
        if (!ward) throw new BadRequest(wardErrorMsg);
      } else {
        throw new BadRequest(wardErrorMsg);
      }
    } else {
      throw new BadRequest(districtErrorMsg);
    }
    return {
      city: city.name,
      district: district.name,
      ward: ward.name,
    };
  }

  async valueCheckingLocation(
    cityCode: string,
    districtCode: string,
    wardCode: string,
  ) {
    await this.getLocationName(cityCode, districtCode, wardCode, true);
  }

  async locationHasStaffChecking(
    cityCode: string,
    districtCode: string,
    wardCode: string,
    type: string,
  ) {
    let result = undefined;

    try {
      result = await this.staffRepository.findOne({
        where: {
          city: cityCode,
          district: districtCode,
          ward: wardCode,
          field: type,
          role: Role.AGENT,
        },
        select: ['id'],
      });
    } catch (error) {}

    if (!result)
      try {
        result = await this.staffRepository.findOne({
          where: {
            city: cityCode,
            district: districtCode,
            ward: wardCode,
            role: Role.ADMIN_WARD,
          },
          select: ['id'],
        });
      } catch (error) {}

    if (!result)
      try {
        result = await this.staffRepository.findOne({
          where: {
            city: cityCode,
            district: districtCode,
            role: Role.ADMIN_DISTRICT,
          },
          select: ['id'],
        });
      } catch (error) {}

    if (!result)
      try {
        result = await this.staffRepository.findOne({
          where: {
            city: cityCode,
            role: Role.ADMIN_CITY,
          },
          select: ['id'],
        });
      } catch (error) {}

    if (!result) {
      throw new BadRequest('There are 0 staffs in this location');
    }
  }

  async valueCheckingFieldWithNumberId(fieldId: number, numberId: string) {
    const groupId = await this.getGroupIdByGroupNumber(numberId);
    if (!groupId) {
      throw new BadRequest('numberId not valid');
    }
    const field = await this.fieldConsultationRepository.findOne({
      where: {
        id: fieldId,
      },
      select: ['group_id'],
    });
    if (!field) {
      throw new BadRequest('fieldId not valid');
    }
    if (!(groupId == field.group_id)) {
      throw new BadRequest('this fieldId is not associated with this groudId');
    }
  }

  async getGroupIdByGroupNumber(numberId: string) {
    const group = await this.baSettingRepository.findOne({
      where: {
        number_id: numberId,
      },
      select: ['group_id'],
    });
    if (group) return group.group_id;
  }

  async getType(id: string) {
    const result = await this.fieldConsultationRepository.findOne({
      where: { id: id },
      select: ['name', 'special'],
    });
    if (result) return result;
  }

  async getTypes(ids: Set<string>) {
    const fields = await this.fieldConsultationRepository.find({
      select: ['name', 'id'],
    });

    const map = {};
    fields.forEach((field) => {
      const id = field.id;
      if (ids.has(String(id))) {
        map[id] = field.name;
      }
    });

    return map;
  }

  async getGroupByNumberId(numberId: string) {
    const group = await this.baSettingRepository.findOne({
      where: {
        number_id: numberId,
      },
      select: ['number_id', 'group_name', 'group_id'],
    });

    return group;
  }

  async getGroupsByNumberIds(numberIds: string[]) {
    const query = numberIds.map((id) => {
      return {
        number_id: id,
      };
    });
    const groups = await this.baSettingRepository.find({
      where: query,
      select: ['number_id', 'group_name', 'group_id'],
    });

    const map = {};

    groups.forEach((group) => {
      map[group.number_id] = group.group_name;
    });

    map['groupNameList'] = groups.map((group) => group.group_name);

    return map;
  }

  async getStaffNameByUserId(userId: number) {
    const staff = await this.staffRepository.findOne({
      where: { user_id: userId },
      select: ['user_id', 'name'],
    });
    return staff;
  }

  async getStaffByQuery(query: any) {
    const staffs = await this.staffRepository.find({
      where: query,
      select: ['city', 'district', 'ward', 'field'],
    });
    if (staffs && staffs.length > 0) {
      return {
        city: staffs.map((e) => e.city).toString(),
        district: staffs.map((e) => e.district).toString(),
        ward: staffs.map((e) => e.ward).toString(),
        field: staffs.map((e) => e.field).toString(),
      };
    }
  }

  async getGroupIdFromNumberId(numberId: string) {
    const result = await this.baSettingRepository.findOne({
      where: { number_id: numberId },
      select: ['group_id'],
    });
    if (!result) {
      throw new Forbidden('Number ID not exist!');
    }
    return result.group_id;
  }

  async getNumberIdMapFromGroupId() {
    const settings = await this.baSettingRepository.find({
      select: ['group_id', 'number_id'],
    });
    const map = {};
    settings.forEach((setting) => {
      map[setting.group_id] = setting.number_id;
    });
    return map;
  }

  // NOTE(): Update staff info in db, so number_id -> group_id for finding staff

  async getSpecialField(special: string) {
    const res: any = await this.fieldConsultationRepository
      .createQueryBuilder('field')
      .where('field.special = :special', { special })
      .leftJoinAndSelect(
        'ott_ticket_ba_setting',
        'group',
        'group.group_id = field.group_id',
      )
      .getRawOne();
    const result = {
      id: res.field_id,
      numberId: res.group_number_id,
      special: special,
    };
    return result;
  }

  async valueCheckingVaccineTicket(data: VaccineTicketDto) {
    const rData = data.vaccineRegistraion;

    await this.valueCheckingLocation(rData.city, rData.district, rData.ward);

    // TODO(): valid dob
    // HACK(): valid gender
    if (rData.gender != '0' && rData.gender != '1') {
      throw new BadRequest('gender not valid');
    }
    // TODO(): valid idCard
    // TODO(): valid nationality
    // TODO(): valid insuranceNumber
    // TODO(): valid subject
    // HACK(): valid doseNumber
    if (rData.doseNumber != '1' && rData.doseNumber != '2') {
      throw new BadRequest('doseNumber not valid');
    }
    // TODO(): valid doseType
    // TODO(): valid date
    utils.getDateFromVietnameseFormat(rData.vaccineDate);

    // HACK(): valid p2options
    {
      if (rData.p2options.length != 10) {
        throw new BadRequest(
          `bad p2options size, need 10, have ${rData.p2options.length}`,
        );
      }
      rData.p2options.forEach((option) => {
        const optionNum = +option;
        if (optionNum != 0 && optionNum != 1 && optionNum != 2) {
          throw new BadRequest('bad option value');
        }
      });
    }
  }

  mappingNameToCode(cityName: string, districtName: string, wardName: string) {
    const result = {
      city_code: '',
      district_code: '',
      ward_code: '',
    };

    const data = Config('location');
    const infoCity = data.find((city: any) => city.name == cityName.trim());
    if (!infoCity) return result;
    result['city_code'] = infoCity.code;

    const listDistrict = infoCity.childs;
    const infoDistrict = listDistrict.find(
      (district: any) => district.name == districtName.trim(),
    );
    if (!infoDistrict) return result;
    result['district_code'] = infoDistrict.code;

    const listWard = infoDistrict.childs;
    const infoWard = listWard.find((ward: any) => ward.name == wardName.trim());
    if (!infoWard) return result;
    result['ward_code'] = infoWard.code;

    return result;
  }

  async getBaList() {
    const result = await this.baSettingRepository.find({
      order: {
        id: 'ASC',
      },
      select: ['number_id', 'group_name'],
    });
    return result.map((e) => {
      return {
        group_id: e.number_id,
        group_name: e.group_name,
      };
    });
  }

  getMaster() {
    return {
      location: this.locationCodeMap,
      vaccine: this.vaccineConfig,
    };
  }

  getVaccineMap() {
    return this.vaccineConfig;
  }

  async getMap(lat: number, long: number) {
    const utilResult = await RunUtil('find_ward', `./find_ward ${long} ${lat}`);
    const parts = utilResult.split(',');

    let city = undefined;
    let district = undefined;
    let ward = undefined;

    try {
      city = this.locationNameMap[parts[0]];
    } catch (error) {
      const message = `Not found ${parts[0]}`;
      throw new HttpException(message, HttpStatus.NOT_FOUND);
    }
    try {
      district = city[parts[1]];
    } catch (error) {
      const message = `Not found ${parts[1]}`;
      throw new HttpException(message, HttpStatus.NOT_FOUND);
    }
    try {
      ward = district[parts[2]];
    } catch (error) {
      const message = `Not found ${parts[2]}`;
      throw new HttpException(message, HttpStatus.NOT_FOUND);
    }

    return {
      city: city.code,
      district: district.code,
      ward: ward.code,
    };
  }

  async mergedStaffDetail(
    user_id: number,
    qData?: StaffExtraDto | StaffExtraDto[],
  ) {
    const query = this.staffRepository
      .createQueryBuilder('staff')
      .select([
        'group_concat(id)',
        'max(name)',
        'user_id',
        'max(phone_number)',
        'max(email)',
        'group_concat(agent_id)',
        'group_concat(group_id)',
        'group_concat(role)',
        'group_concat(field)',
        'group_concat(city)',
        'group_concat(district)',
        'group_concat(ward)',
      ])
      .where('staff.user_id = :user_id', { user_id })
      .groupBy('user_id');

    if (qData) {
      const [qs, qp] = this.makeQueryR('staff', '', qData);
      query.where(qs, qp);
    }

    const res = await query.getRawOne();

    if (!res) {
      throw new Forbidden('Can not get staff detail!');
    }

    const roles = res['group_concat(role)'].split(',');
    const agentIds = res['group_concat(agent_id)'].split(',');
    const groupIds = res['group_concat(group_id)'].split(',');
    const fields = res['group_concat(field)'].split(',');
    const cities = res['group_concat(city)'].split(',');
    const districts = res['group_concat(district)'].split(',');
    const wards = res['group_concat(ward)'].split(',');
    const numberIdMap = await this.getNumberIdMapFromGroupId();
    return {
      name: res['max(name)'],
      user_id: res.user_id,
      phone_number: res['max(phone_number)'],
      email: res['max(email)'],
      info: res['group_concat(id)'].split(',').map((id, index) => {
        return {
          id,
          role: roles[index],
          agent_id: agentIds[index],
          group_id: numberIdMap[groupIds[index]],
          field: fields[index],
          city: cities[index],
          district: districts[index],
          ward: wards[index],
        };
      }),
    };
  }

  async mergedStaffDetailWithPermission(
    user,
    user_id: number,
    qData?: StaffExtraDto | StaffExtraDto[],
  ) {
    const staff = await this.mergedStaffDetail(user_id, qData);

    const info = [];
    for (const i of staff.info) {
      if (this.permissionPowerChecking(user, i)) {
        info.push(i);
      }
    }
    staff.info = info;
    return staff;
  }

  roleLessPowerResovle(role: Role) {
    let result = [];
    switch (+role) {
      case Role.ADMIN:
        {
          result = [
            Role.ADMIN,
            Role.ADMIN_CITY,
            Role.ADMIN_DISTRICT,
            Role.ADMIN_WARD,
            Role.LOCK_ADMIN,
            Role.LOCK_AGENT,
            Role.LOCK_ADMIN_CITY,
            Role.LOCK_ADMIN_DISTRICT,
            Role.AGENT,
          ];
        }
        break;
      case Role.ADMIN_CITY:
        {
          result = [
            Role.ADMIN_DISTRICT,
            Role.ADMIN_WARD,
            Role.AGENT,
            Role.LOCK_ADMIN_DISTRICT,
            Role.LOCK_ADMIN_WARD,
            Role.LOCK_AGENT,
          ];
        }
        break;
      case Role.ADMIN_DISTRICT:
        {
          result = [
            Role.ADMIN_WARD,
            Role.AGENT,
            Role.LOCK_ADMIN_WARD,
            Role.LOCK_AGENT,
          ];
        }
        break;
      case Role.ADMIN_WARD:
        {
          result = [Role.AGENT, Role.LOCK_AGENT];
        }
        break;
    }
    return result;
  }

  makeQueryR(column: string, key: string, data: any) {
    if (data) {
      if (data.constructor === Array) {
        const sList = [];
        let params = {};
        for (let i = 0; i < data.length; ++i) {
          const query = this.makeQueryR(column, `${column}_${i}`, data[i]);
          if (!query) {
            continue;
          }
          let [s, p] = query;
          sList.push(s);
          for (const j in p) {
            params[j] = p[j];
          }
        }
        const qString = '(' + sList.join(' or ') + ')';
        return [qString, params];
      } else if (data.constructor === Object) {
        const sList = [];
        let params = {};
        for (const k in data) {
          const query = this.makeQueryR(
            `${column}.${k}`,
            `${k}_${key}`,
            data[k],
          );
          if (!query) {
            continue;
          }
          let [s, p] = query;
          sList.push(s);
          for (const j in p) {
            params[j] = p[j];
          }
        }
        const qString = '(' + sList.join(' and ') + ')';
        return [qString, params];
      } else {
        const qString = `${column} = :${key}`;
        const params = {};
        params[key] = data;
        return [qString, params];
      }
    }
    return undefined;
  }

  async mergedStaffList(qData: StaffExtraDto[], take: number, skip: number) {
    const query = this.staffRepository
      .createQueryBuilder('staff')
      .select([
        'group_concat(id)',
        'max(name)',
        'user_id',
        'max(phone_number)',
        'max(email)',
        'group_concat(agent_id)',
        'group_concat(group_id)',
        'group_concat(role)',
        'group_concat(field)',
        'group_concat(city)',
        'group_concat(district)',
        'group_concat(ward)',
      ])
      .groupBy('user_id')
      .take(take)
      .skip(skip);

    const staffQuery = this.makeQueryR('staff', '', qData);
    const [queryString, queryParam] = staffQuery;

    query.where(queryString, queryParam);

    const [res, total] = await Promise.all([
      query.getRawMany(),
      query.getCount(),
    ]);

    if (!res) {
      throw new Forbidden('Something went wrong!');
    }

    const numberIdMap = await this.getNumberIdMapFromGroupId();
    const data = res.map((r) => {
      const roles = r['group_concat(role)'].split(',');
      const agentIds = r['group_concat(agent_id)'].split(',');
      const groupIds = r['group_concat(group_id)'].split(',');
      const fields = r['group_concat(field)'].split(',');
      const cities = r['group_concat(city)'].split(',');
      const districts = r['group_concat(district)'].split(',');
      const wards = r['group_concat(ward)'].split(',');

      return {
        name: r['max(name)'],
        user_id: r.user_id,
        phone_number: r['max(phone_number)'],
        email: r['max(email)'],
        info: r['group_concat(id)'].split(',').map((id, index) => {
          return {
            id,
            role: roles[index],
            agent_id: agentIds[index],
            group_id: numberIdMap[groupIds[index]],
            field: fields[index],
            city: cities[index],
            district: districts[index],
            ward: wards[index],
          };
        }),
      };
    });
    return {
      total,
      data,
    };
  }

  locationPermissionChecking(
    user,
    locationInfo: LocationInfoDto,
    willThrow?: boolean,
  ) {
    const isAdmin = !!user.info.find((info) => info.role == Role.ADMIN);
    if (isAdmin) {
      return true;
    }

    const { city, district, ward } = locationInfo;

    for (const info of user.info) {
      switch (+info.role) {
        case Role.ADMIN_CITY:
          {
            if (info.city == city) {
              return true;
            }
          }
          break;
        case Role.ADMIN_DISTRICT:
          {
            if (info.city == city && info.district == district) {
              return true;
            }
          }
          break;
        case Role.ADMIN_WARD:
          {
            if (
              info.city == city &&
              info.district == district &&
              info.ward == ward
            ) {
              return true;
            }
          }
          break;
      }
    }

    if (willThrow)
      throw new Forbidden("You do not have the location's permission!");

    return false;
  }

  rolePowerChecking(
    user,
    targetId: number,
    targetRole: Role,
    ignoreAdmin?: boolean,
  ) {
    if (!ignoreAdmin) {
      const isAdmin = !!user.info.find((info) => info.role == Role.ADMIN);
      if (isAdmin) {
        return true;
      } else if (targetRole == Role.ADMIN) {
        return false;
      }
    }

    if (targetId == +user.id) {
      return false;
    }

    const agentRoleCount = user.info.filter(
      (info) => info.role == Role.AGENT,
    ).length;
    // NOTE(): if user is agent
    if (agentRoleCount == user.info.length) {
      return false;
    }

    if (targetRole == Role.AGENT) {
      return true;
    }
    let authorized = false;
    for (const info of user.info) {
      if (info.role < targetRole) {
        return true;
      }
    }

    return authorized;
  }

  permissionPowerChecking(
    user,
    info: StaffExtraDto,
    agentPermissionForAdmin = true,
  ): boolean {
    const isAdmin = !!user.info.find((info) => info.role == Role.ADMIN);
    if (!agentPermissionForAdmin) {
      if (isAdmin && info.role != Role.AGENT) {
        return true;
      }
    } else if (isAdmin) {
      return true;
    }

    const { city, district, ward, role } = info;

    switch (+role) {
      case Role.AGENT:
        {
          for (const extra of user.info) {
            if (extra.role == Role.ADMIN_CITY && extra.city == city) {
              return true;
            } else if (
              extra.role == Role.ADMIN_DISTRICT &&
              extra.city == city &&
              extra.district == district
            ) {
              return true;
            } else if (
              extra.role == Role.ADMIN_WARD &&
              extra.city == city &&
              extra.district == district &&
              extra.ward == ward
            ) {
              return true;
            }
          }
        }
        break;
      case Role.ADMIN_WARD:
        {
          for (const extra of user.info) {
            if (extra.role == Role.ADMIN_CITY && extra.city == city) {
              return true;
            } else if (
              extra.role == Role.ADMIN_DISTRICT &&
              extra.city == city &&
              extra.district == district
            ) {
              return true;
            }
          }
        }
        break;
      case Role.ADMIN_DISTRICT:
        {
          for (const extra of user.info) {
            if (extra.role == Role.ADMIN_CITY && extra.city == city) {
              return true;
            }
          }
        }
        break;
      case Role.ADMIN_CITY: {
        return false;
      }
    }
    return false;
  }

  async createAgentMessage(
    cityCode: string,
    districtCode: string,
    wardCode: string,
    role: number,
  ) {
    const location = await this.getLocationName(
      cityCode,
      districtCode,
      wardCode,
    );
    let locationName = undefined;
    let roleName = undefined;
    if (role == Role.AGENT) {
      roleName = 'Nhân viên xử lý';
      locationName = `thành phố/tỉnh ${location.city}, quận/huyện ${location.district}, phường/xã ${location.ward}`;
    } else if (role == Role.ADMIN) {
      roleName = 'Quản trị tổng';
      locationName = '';
    } else if (role == Role.ADMIN_CITY) {
      roleName = 'Quản trị thành phố/tỉnh';
      locationName = `thành phố/tỉnh ${location.city}`;
    } else if (role == Role.ADMIN_DISTRICT) {
      roleName = 'Quản trị quận/huyện';
      locationName = `thành phố/tỉnh ${location.city}, quận/huyện ${location.district}`;
    } else if (role == Role.ADMIN_WARD) {
      roleName = 'Quản trị phường/xã';
      locationName = `thành phố/tỉnh ${location.city}, quận/huyện ${location.district}, phường/xã ${location.ward}`;
    }
    return `Quý khách đã được đăng kí thành công vào Tổng đài 1022 của ${locationName} với vai trò ${roleName}. Quý khách đăng nhập vào trang web ${process.env.WEB_URL} để kiểm tra.`;
  }

  async autoFillInfo(data: any) {
    const infos = data.info;

    for (const info of infos)
      if (info.role != Role.AGENT) {
        const city = info.city;
        if (!info.district) {
          info.district = Object.keys(this.locationCodeMap[city])[1];
        }
        const district = info.district;
        if (!info.ward) {
          info.ward = Object.keys(this.locationCodeMap[city][district])[1];
        }

        let group = undefined;
        if (!info.group_id) {
          const ba = await this.baSettingRepository.findOne({});
          info.group_id = ba.number_id;
          group = ba.group_id;
        } else {
          group = (
            await this.baSettingRepository.findOne({
              number_id: info.group_id,
            })
          ).group_id;
        }
        if (!info.field) {
          const field = await this.fieldConsultationRepository.findOne({
            group_id: group,
          });
          info.field = field.id;
        }
      }

    return data;
  }
}
