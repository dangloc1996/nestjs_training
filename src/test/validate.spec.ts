import { IsPhoneNumberRule } from '../feedback-citizen/validations';

describe('Testing Validation Functions', () => {
  describe('Validate PhoneNumber', () => {
    const phoneValidationRule = new IsPhoneNumberRule();

    function testPhoneNumber(phoneNumber: string, expectResult: boolean) {
      it(`${phoneNumber} -> should be ${expectResult}`, async () => {
        expect(await phoneValidationRule.validate(phoneNumber)).toBe(
          expectResult,
        );
      });
    }

    testPhoneNumber('123', false);
    testPhoneNumber('0987654321', true);
    testPhoneNumber('84876543211', true);
    testPhoneNumber('8487654321123', false);
    testPhoneNumber('abcsefghjk', false);
    testPhoneNumber('848765432jk', false);
    testPhoneNumber('09876543211', false);
  });

  describe('Validate location', () => {});
});
