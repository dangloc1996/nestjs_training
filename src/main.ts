import { NestFactory } from '@nestjs/core';
import { INestApplication } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

import { Config } from '@common/config';

import { MyLogger } from '@common/logger';

import { AppModule } from './app.module';

const SwaggerConfig = Config('swagger');
const Host = process.env.HOST || 'localhost';
const Port = process.env.PORT || 3000;
const Local = process.env.LOCAL;

function disableConsoleLog() {
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  console.log = () => {};
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  console.debug = () => {};
}

function ConfigLog() {
  console.log(SwaggerConfig);
}

function buildApiDoc(app: INestApplication, urlPath = SwaggerConfig.path) {
  const path = Local
    ? SwaggerConfig.apiPath.local
    : SwaggerConfig.apiPath.cloud;

  const docConfig = new DocumentBuilder()
    .setTitle(SwaggerConfig.title)
    .setDescription(SwaggerConfig.description)
    .setVersion(SwaggerConfig.version)
    .addServer(path)
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, docConfig);
  SwaggerModule.setup(urlPath, app, document);
  console.log('Create Swagger Docs with path:', urlPath);
}

async function bootstrap() {
  const logger = new MyLogger();
  const app = await NestFactory.create(AppModule, {
    logger: logger,
  });

  // app.use('/public', express.static(join(process.cwd(), '../public')));

  if (process.env.ENABLE_DEBUG === 'false') {
    console.log('disableConsoleLog');
    disableConsoleLog();
  }
  if (process.env.NODE_ENV !== 'production') {
    buildApiDoc(app);
    app.enableCors();
  }

  logger.log(`Application start at port: ${Port}`);
  await app.listen(Port, Host);
}

ConfigLog();
bootstrap();
