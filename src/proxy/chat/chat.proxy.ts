import { BadRequestException, Injectable } from '@nestjs/common';
import { CustomHttpService as HttpService } from '../http.service';

import { Config } from '@common/config';
import { MyLogger } from '@common/logger';

import {
  ApiTicketDto as TicketDto,
  TicketQueryDto,
} from '@feedback-citizen/dto';

@Injectable()
export class ChatProxy {
  private readonly logger = new MyLogger('ChatProxy');
  private config = Config('chat');

  constructor(private httpService: HttpService) {}

  private getHeaders() {
    return this.config['headers'];
  }

  private getUrl(endpointKey: string) {
    const domain = this.config['domain'];
    const endpoint = this.config['endpoints'][endpointKey];
    return domain + endpoint;
  }

  public async createGeneralTicket(data: TicketDto) {
    const url = this.getUrl('createGeneralTicket');
    const options = { headers: this.getHeaders() };
    const result = await this.httpService.post(url, data, options);
    return result.data;
  }

  public async updateGeneralTicket(id: string, data: TicketDto) {
    let url = this.getUrl('updateGeneralTicket') + `/${id}`;
    url += '?forceUpdate=true';
    const options = { headers: this.getHeaders() };
    const result = await this.httpService.put(url, data, options);
    return result.data;
  }

  public async deleteGeneralTicket(id: string) {
    const url = this.getUrl('deleteGeneralTicket') + `/${id}`;
    const options = { headers: this.getHeaders() };
    const result = await this.httpService.delete(url, options);
    return result.data;
  }

  public async getGeneralTicket(id: string) {
    const url = this.getUrl('getGeneralTicket') + `?ticket=${id}`;
    const options = { headers: this.getHeaders() };
    const result = await this.httpService.get(url, options);

    return result.data;
  }

  public async getGeneralTicketList(query: TicketQueryDto) {
    const url = this.getUrl('getGeneralTicketList');
    const options = { headers: this.getHeaders(), params: query };
    const result = await this.httpService.get(url, options);
    return result.data.data;
  }

  public async getUsersTicketStatus(userIds: string[]) {
    const url = this.getUrl('getUsersTicketStatus');
    const options = { headers: this.getHeaders() };
    const data = {
      user_ids: userIds,
    };
    const result = await this.httpService.post(url, data, options);
    return result.data.data;
  }

  public async createSettingBA(ba: any) {
    const dataInput: any = {
      number: ba.number,
      number_id: ba.number_id,
      queue: {
        name: ba.group_name,
        record_calls: true,
        wrap_up_time_limit: 10,
        schedule: 1,
        wait_agent_answer_timeout: 15,
        from_number_callout_to_agent: null,
        cond_routing: 1,
      },
      group: {
        name: ba.group_name,
      },
      agentIds: null,
    };
    const url = this.getUrl('createSettingBA');
    const options = { headers: this.getHeaders() };
    const result = await this.httpService.post(url, dataInput, options);
    return result.data;
  }

  public async registerAgent(createAgent: any) {
    const data = {
      name: createAgent.name,
      stringee_user_id: String(createAgent.user_id),
      manual_status: 'AVAILABLE',
      phone_number: createAgent.phone_number,
    };
    const url = this.getUrl('registerAgent');
    const result = await this.httpService.post(url, data, {
      headers: this.getHeaders(),
    });
    if (
      Object.keys(result.data.data).length === 0 &&
      result.data.data.constructor === Object
    )
      throw new BadRequestException('Agent is exist');
    return result.data;
  }

  public async deleteGroupAgent(groupId: string, agentId: string) {
    const url = this.getUrl('deleteGroupAgent');
    const result = await this.httpService.delete(url, {
      headers: this.getHeaders(),
      params: {
        group: groupId,
        agent: agentId,
      },
    });
    return result;
  }

  public async addGroup(agentId: string, groupId: string) {
    const data = {
      group_id: groupId,
      agent_id: agentId,
    };
    const url = this.getUrl('addGroupAgent');
    const result = await this.httpService.post(url, data, {
      headers: this.getHeaders(),
    });
    return result;
  }

  public async sendMessage(userId: string, numberId: string, messages: any) {
    try {
      const data = {
        userId: userId,
        numberId: numberId,
        messages: messages,
      };
      const url = this.getUrl('sendMessage');
      const result = await this.httpService.post(url, data, {
        headers: this.getHeaders(),
      });
      return result;
    } catch (error) {
      this.logger.error('Send message error');
    }
  }

  public async getStringeeEvents(baId: string) {
    const url = `${this.getUrl('stringeeEvents')}/${baId}`;
    const result = await this.httpService.get(url, {
      headers: this.getHeaders(),
    });
    return result.data.data;
  }

  public async getStringeeCalls(baId: string, query: any) {
    const url = `${this.getUrl('stringeeCalls')}/${baId}`;
    const result = await this.httpService.get(url, {
      headers: this.getHeaders(),
      params: query,
    });
    return result.data.data;
  }
}
