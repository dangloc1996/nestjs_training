import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';

const now = require('performance-now');

@Injectable()
export class CustomHttpService {
  constructor(private axiosService: HttpService) {}

  async request(method: string, url: string, data?: any, options?: any) {
    try {
      const opt = {
        ...options,
        url: url,
        method: method,
        data: data,
      };
      const startTime = now();
      const result = await this.axiosService.request(opt).toPromise();
      const endTime = now();
      const elapsedTime = `${(endTime - startTime).toFixed(6)}ms`;
      Logger.log(`Request ${method} to ${url} taken ${elapsedTime}`);
      return result;
    } catch (error) {
      if (error.response) {
        console.log(error.response.status);
        console.log(error.response.data);
        console.log(error.request._header);
        console.log(data);
        const message = {
          proxy_error: error.response.data,
        };
        throw new HttpException(
          message,
          error.response.status || HttpStatus.INTERNAL_SERVER_ERROR,
        );
      } else {
        throw new Error(error);
      }
    }
  }

  async post(url: string, data?: any, options?: any) {
    return this.request('POST', url, data, options);
  }

  async put(url: string, data?: any, options?: any) {
    return this.request('PUT', url, data, options);
  }

  async get(url: string, options?: any) {
    return this.request('GET', url, undefined, options);
  }

  async delete(url: string, options?: any) {
    return this.request('DELETE', url, undefined, options);
  }
}
