import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { S3 } from 'aws-sdk';

import { Config } from '@common/config';
import { MyLogger } from '@common/logger';

@Injectable()
export class S3Service {
  private readonly config = Config('aws-s3');
  private readonly logger = new MyLogger('AWS-S3');
  private readonly s3Client = new S3(this.config);

  getResourceUrl(key: string): string {
    const bucket = this.config['bucket'];
    const region = this.config['region'];
    return `https://${bucket}.s3.${region}.amazonaws.com/${key}`;
  }

  getKey(resourceUrl: string): string {
    const index = resourceUrl.search('amazonaws.com');
    return resourceUrl.substring(index + 14);
  }

  async uploadBytesData(
    key: string,
    buffer: S3.Body,
    temp?: boolean,
    acl?: string,
  ): Promise<boolean> {
    try {
      const data = {
        Bucket: this.config.bucket,
        Key: key,
        Body: buffer,
        ACL: acl,
      };
      const response = await this.s3Client.putObject(data).promise();
      if (response.$response.error) {
        const message = `response.$response.error.message}`;
        throw new InternalServerErrorException(message);
      }
      return true;
    } catch (error) {
      throw new InternalServerErrorException(error.message);
    }
  }

  async publicUpload(key: string, buffer: S3.Body): Promise<boolean> {
    return await this.uploadBytesData(key, buffer, false);
  }

  async getObject(key: string) {
    return this.s3Client
      .getObject({
        Key: key,
        Bucket: this.config['bucket'],
      })
      .createReadStream()
      .on('error', (error) => {
        this.logger.error(error.message);
      });
  }

  async deleteObject(key: string) {
    this.s3Client.deleteObject(
      {
        Key: key,
        Bucket: this.config['bucket'],
      },
      (error) => {
        if (error) {
          this.logger.error(error.message);
        }
      },
    );
  }
}
