import { BadRequestException, Injectable } from '@nestjs/common';
import { CustomHttpService as HttpService } from '../http.service';

import { LOCK_NUMBER } from '@auth/enum';

import { Config } from '@common/config';
import { MyLogger } from '@common/logger';
import { Staff } from '@feedback-citizen/entities';

@Injectable()
export class AuthProxy {
  private config = Config('auth');
  private readonly logger = new MyLogger('AuthProxy');
  private keyGateway = this.config['gateway'];

  constructor(private httpService: HttpService) {}

  private getUrl(endpointKey: string) {
    const domain = this.config['domain'];
    const endpoint = this.config['endpoints'][endpointKey];
    return domain + endpoint;
  }

  private getHeaders(token?) {
    if (token)
      return {
        'Content-Type': 'application/json',
        'partner-id': 'partnerId',
        'partner-name': 'partnerName',
        'partner-secret': 'partnerSecret',
        Authorization: token,
        apikey: this.keyGateway,
      };
    return {
      'Content-Type': 'application/json',
      'partner-id': 'partnerId',
      'partner-name': 'partnerName',
      'partner-secret': 'partnerSecret',
      apikey: this.keyGateway,
    };
  }

  async getUserNoStaff(phoneNumber: string, password: string) {
    const url = this.getUrl('getToken');
    const data = {
      phone_number: phoneNumber,
      password: password,
    };
    const result = await this.httpService.post(url, data, {
      headers: this.getHeaders(),
    });

    return result.data.data.user;
  }

  async getStaff(user_id: number) {
    const query = Staff.createQueryBuilder('staff')
      .select([
        'group_concat(id)',
        'max(name)',
        'user_id',
        'max(phone_number)',
        'max(email)',
        'group_concat(agent_id)',
        'group_concat(group_id)',
        'group_concat(role)',
        'group_concat(field)',
        'group_concat(city)',
        'group_concat(district)',
        'group_concat(ward)',
      ])
      .where('staff.user_id = :user_id', { user_id })
      .groupBy('user_id');

    const res = await query.getRawOne();

    if (!res) {
      throw new BadRequestException('Staff not found!');
    }

    const roles = res['group_concat(role)'].split(',');
    const fields = res['group_concat(field)'].split(',');
    const cities = res['group_concat(city)'].split(',');
    const districts = res['group_concat(district)'].split(',');
    const wards = res['group_concat(ward)'].split(',');

    return {
      name: res['max(name)'],
      user_id: res.user_id,
      phone_number: res['max(phone_number)'],
      email: res['max(email)'],
      info: res['group_concat(id)'].split(',').map((id, index) => {
        return {
          id,
          role: roles[index],
          field: fields[index],
          city: cities[index],
          district: districts[index],
          ward: wards[index],
        };
      }),
    };
  }

  public async getToken(phoneNumber: string, password: string) {
    const url = this.getUrl('getToken');
    const data = {
      phone_number: phoneNumber,
      password: password,
    };
    const result = await this.httpService.post(url, data, {
      headers: this.getHeaders(),
    });

    const isFirstLogin = result.data.data.user.is_first_login;

    const user = await this.getStaff(result.data.data.user.id);

    if (user.info && user.info.length > 0) {
      let check = false;
      for (const i of user.info) {
        if (i.role < LOCK_NUMBER) {
          check = true;
          break;
        }
      }
      if (!check) {
        throw new BadRequestException('Tài khoản đã bị khóa!');
      }
    }

    return {
      user: { ...user, avatar_url: result.data.data.user.avatar_url },
      jwt_token: result.data.data.jwt_token,
      is_first_login: isFirstLogin,
    };
  }

  public async getUserByPhone(phone: string, accessToken: string) {
    const url = this.getUrl('getUserByPhone');
    const data = {
      phone: [phone],
    };
    const result = await this.httpService.post(url, data, {
      headers: this.getHeaders(accessToken),
    });
    if (result.data.data.length == 0) return null;
    return result.data.data[0];
  }

  public async getUserByPhoneV2(phone: string) {
    const url = this.getUrl('getUserByPhoneV2') + `?phone_number=${phone}`;
    const result = await this.httpService.get(url, {
      headers: this.getHeaders(),
    });
    return result.data.data;
  }

  public async getLocation() {
    const url = this.getUrl('getLocation');
    const result = await this.httpService.get(url, {
      headers: this.getHeaders(),
    });
    if (result.data.statusCode !== 200)
      throw new BadRequestException('Call service error');
    return result.data.data;
  }

  public async changePassword(newPassword: string, jwt: string) {
    const url = this.getUrl('changePassword');
    const data = {
      newPassword: newPassword,
    };
    return this.httpService.post(url, data, {
      headers: this.getHeaders(jwt),
    });
  }

  public async createPartnerUser(createAgentDto: any, token: string) {
    const url = this.getUrl('createPartnerUser');
    const data = {
      name: createAgentDto.name,
      email: createAgentDto.email,
      phone_number: createAgentDto.phone_number,
    };
    const result = await this.httpService.post(url, data, {
      headers: this.getHeaders(token),
    });
    return result.data.data;
  }

  public async sendMailInfoAccount(accountInfo: any, token) {
    const url = this.getUrl('sendAccountInfoMail');
    const result = await this.httpService.post(url, accountInfo, {
      headers: this.getHeaders(token),
    });
    return result.data;
  }

  public async sendMailNoticeSuccessForAdmin(
    email: string,
    message: string,
    token: string,
  ) {
    const url = this.getUrl('sendAgentImportNotice');
    const result = await this.httpService.post(
      url,
      { email: email, message: message },
      {
        headers: this.getHeaders(token),
      },
    );
    return result.data;
  }
}
