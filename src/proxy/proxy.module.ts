import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';

import { CustomHttpService as HttpService } from './http.service';
import { ChatProxy } from './chat';
import { AuthProxy } from './auth';
import { S3Service } from './aws';

@Module({
  imports: [HttpModule],
  providers: [HttpService, ChatProxy, AuthProxy, S3Service],
  exports: [ChatProxy, AuthProxy, S3Service],
})
export class ProxyModule {}
