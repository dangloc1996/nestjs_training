import * as path from 'path';

const util = require('util');
const exec = util.promisify(require('child_process').exec);

const UTIL_DIR = 'util';

function UtilFilePath(filePath: string): string {
  const projectDir = path.resolve('./');
  return path.join(projectDir, `${UTIL_DIR}/${filePath}`);
}

export async function RunUtil(dir: string, command: string) {
  const directory = UtilFilePath(dir);
  const { stdout } = await exec(`cd ${directory} && ${command}`);
  return stdout;
}

export async function Sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
