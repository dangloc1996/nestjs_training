import { Injectable, Scope } from '@nestjs/common';
import { ConsoleLogger } from '@nestjs/common';

@Injectable({ scope: Scope.TRANSIENT })
export class MyLogger extends ConsoleLogger {
  throwError(error: Error): void {
    this.error(error.message, error.stack);
  }
}
