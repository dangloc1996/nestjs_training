export * from './exceptions';

export enum ErrorCode {
  CALL_SERVICE_FAILED = 503,
  AUTHORIZE = 401,
}
