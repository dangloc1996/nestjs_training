import { HttpException, HttpStatus } from '@nestjs/common';
import { ErrorCode } from '.';

export const handleError = (
  message: string,
  statusCode: ErrorCode,
  httpStatus?: number,
) => {
  throw new HttpException(
    {
      message,
      statusCode,
    },
    httpStatus || HttpStatus.BAD_REQUEST,
  );
};
