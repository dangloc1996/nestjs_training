import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const now = require('performance-now');

export interface Response<T> {
  data: T;
}

function ResponseSerialize(response: any): any {
  if (response.listTotal != undefined)
    return {
      count: response.listTotal,
      data: response.list,
    };
  return {
    data: response,
  };
}

@Injectable()
export class TransformInterceptor<T>
  implements NestInterceptor<T, Response<T>>
{
  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<Response<T>> {
    const startTime = now();

    return next.handle().pipe(
      map((response) => {
        if (response) {
          const endTime = now();
          const elapsedTime = `${(endTime - startTime).toFixed(6)}ms`;
          let rs = ResponseSerialize(response);
          if (process.env.DEBUG)
            rs = {
              ...rs,
              internal_elapsed: `${elapsedTime}`,
            };
          return rs;
        }
      }),
    );
  }
}
