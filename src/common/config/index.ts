import { Logger } from '@nestjs/common';

import * as fs from 'fs';
import * as path from 'path';

const CONFIG_DIR = '.config';

function ConfigFilePath(configName: string): string {
  const projectDir = path.resolve('./');
  return path.join(projectDir, `${CONFIG_DIR}/${configName}.json`);
}

export function Config(configName: string) {
  const configFile = ConfigFilePath(configName);
  const jsonString: string = fs.readFileSync(configFile, 'utf8');
  Logger.log(`===> LOADED CONFIG FILE ${configFile}`);
  return JSON.parse(jsonString);
}

export function SaveConfig(configName: string, content: string) {
  const configFile = ConfigFilePath(configName);
  fs.writeFileSync(configFile, content);
  Logger.log(`===> SAVED CONFIG FILE ${configFile}`);
}
