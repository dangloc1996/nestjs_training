import { Logger } from '@nestjs/common';

import * as fs from 'fs';
import * as path from 'path';

const SQL_DIR = 'sql';

function SqlFilePath(sqlName: string): string {
  const projectDir = path.resolve('./');
  return path.join(projectDir, `${SQL_DIR}/${sqlName}.sql`);
}

export function SqlQuery(sqlName: string) {
  const sqlFile = SqlFilePath(sqlName);
  const sqlString: string = fs.readFileSync(sqlFile, 'utf8');
  Logger.log(`===> LOADED SQL FILE ${sqlFile}`);
  return sqlString;
}

export function SqlSave(sqlName: string, content: string) {
  const configFile = SqlFilePath(sqlName);
  fs.writeFileSync(configFile, content);
  Logger.log(`===> SAVED SQL FILE ${configFile}`);
}
